import * as Auth          from '../auth'
import * as Common        from '../common'
import * as MoteResources from '../api/mote_resources'

import { MoteCommError }  from '../../mote_comm/comm'
import { loggers }        from '../../logger.js'
import { DbOpError }      from '../../db/common'
import { QueryExecError } from '../api/resources'

const logger = loggers.web


export const unique = async (request, response) => {
    if (request.body.mote_id == null) {
        logger.verbose('@api.mote no mote id indicated')
        response.status(400)
                .send('No mote id name was indicated')
        return
    }
    if (request.body.operation == null) {
        logger.verbose('@api.mote no operation indicated')
        response.status(400)
                .send('No operation name was indicated')
        return
    }
    if (request.body.data == null) {
        logger.verbose('@api.mote no data indicated')
        response.status(400)
                .send('No operation data name was indicated')
        return
    }

    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.Write])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.mote failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.mote failed to execute mote operation with unknown error:`)
            console.log(e)
            response.status(500).send(`failed to execute mote operation`)
        }
        return
    }

    try {

        let mote_id  : number = request.body.mote_id
        let operation: string = request.body.operation
        let data     : object = request.body.data

        await MoteResources.execute(mote_id, operation, data, client_info)
        response.status(200).send('{"status": "ok"}')

        await Common.log_info(
            client_info,
            Common.ClientOperation.MoteInstruction,
            `instrução "${operation}" enviada com sucesso`,
            request.body
        )
        return

    } catch (e) {
        if (e instanceof QueryExecError || e instanceof DbOpError) {
            logger.verbose(`@api.mote failed to execute mote operation: ${e.message}`)
            response.status(400).send(e.message)
        }
        else if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.mote failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                    .set('WWW-Authenticate', e.to_string())
                    .send()
        } else if (e instanceof MoteCommError) {
            response.status(400).send(e.message)
        }
        else {
            logger.warn(
                `@api.mote failed to execute mote operation with unknown error: ${e}`
            )
            response.status(500).send(`failed to execute mote operation`)
        }
    }

    await Common.log_err(
        client_info,
        Common.ClientOperation.MoteInstruction,
        `execução falhada`,
        request.body
    )
}