import * as Common      from '../common'
import * as Control     from '../api/control'
import * as Auth        from '../auth'
import * as EPResources from '../api/ext_pack_resources'

import { ExtensionPack  } from '../../db/models'
import { loggers        } from '../../logger.js'
import { DbOpError      } from '../../db/common'
import { QueryExecError } from '../api/resources'

const logger = loggers.web


const do_sub_unsub_to_motes = async (request, response, sub=true) => {
    if (request.body.motes === undefined) {
        logger.verbose('@api.extension_pack no motes list supplied')
        response.status(400)
            .send('No motes list supplied')
        return
    }


    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.Compute])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.extension_pack failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.extension_pack failed to sub/unsub to motes with unknown error:`)
            console.log(e)
            response.status(500).send(`failed to sub/unsub to motes`)
        }
        return
    }

    // get extension pack information from server
    let extension_pack: ExtensionPack
    try {

        extension_pack = await ExtensionPack.findOne({ group_id: client_info.group_id })
        if (extension_pack === undefined)
            throw `Extension pack not found`
        // else if (!extension_pack.active)
        //     throw `Extension pack is not active`

    } catch (exception) {
        logger.warn(`@api.extension_pack client does not have an extension pack`)
        console.log(exception)
        response.status(500).send(`no registered extension pack was found`)
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'subscrição falhada - sem extension pack',
            request.body
        )
        return
    }


    // subcribe
    try {
        
        if (sub)
            await EPResources.subscribe_to_events(request.body.motes, extension_pack)
        else 
            await EPResources.unsubscribe_to_events(request.body.motes, extension_pack)
            
        response.status(200).send('{"status":"ok"}')

        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'subscrição executada com sucesso',
            request.body
        )
        return

    } catch (exception) {
        logger.warn(`@api.extension_pack sub/unsub request failed`)
        console.log(exception)
        if (exception instanceof EPResources.ExtPackError) {
            response.status(400).send(exception.message)
        } else {
            response.status(500).send(`subscribe/unsubscribe request failed`)
        }
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'subscrição falhada',
            request.body
        )
        return
    }

}


export async function subscribe_to_mote_events (request, response) {
    await do_sub_unsub_to_motes(request, response, true)
}

export async function unsubscribe_to_mote_events (request, response) {
    await do_sub_unsub_to_motes(request, response, false)
}


export async function send_client_request (request, response) {
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.Compute])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.extension_pack.client_request failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.extension_pack failed to get client's credentials with error:`)
            console.log(e)
            response.status(500).send(`failed to send client request `)
        }
        return
    }

    // get extension pack information from server
    let extension_pack: ExtensionPack
    try {

        extension_pack = await ExtensionPack.findOne({ group_id: client_info.group_id })
        if (extension_pack === undefined)
            throw `Extension pack not found`
        else if (!extension_pack.active) {
            response.status(400).send(`extension pack is stopped`)
            logger.warn(`@api.extension_pack.client_request inactive extension pack`)
            await Common.log_err(
                client_info,
                Common.ClientOperation.Compute,
                'envio de pedido falhada - extension pack inativa',
                request.body
            )
            return
        }

    } catch (exception) {
        logger.warn(`@api.extension_pack.client_request client does not have an extension pack`)
        console.log(exception)
        response.status(500).send(`no registered extension pack was found`)
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'envio de pedido falhada - sem extension pack',
            request.body
        )
        return
    }

    try {

        let res = await EPResources.send_client_request(request.body, extension_pack)
        response.status(200).send(res)

        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'pedido enviado com sucesso',
            request.body
        )
        return

    } catch (exception) {
        logger.warn(`@api.extension_pack.client_request send client request failed`)
        console.log(exception)
        if (exception instanceof EPResources.ExtPackError) {
            response.status(400).send(exception.message)
        } else {
            response.status(500).send(`failed to send request`)
        }
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'envio de pedido falhou',
            request.body
        )
        return
    }
}



export async function create_extension_pack (request, response) {
    if (request.body.language === undefined) {
        logger.verbose('@api.extension_pack language field not supplied')
        response.status(400)
                .send('Language was not specified. Expected field "language"')
        return
    }
    if (request.body.repository_url === undefined) {
        logger.verbose('@api.extension_pack repository url field not supplied')
        response.status(400)
                .send('Repository url was not specified. Expected field "repository_url"')
        return
    }
    
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.All])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.extension_pack.create failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.extension_pack.create failed to get client's credentials with error:`)
            console.log(e)
            response.status(500).send(`failed to send client request `)
        }
        return
    }

    try {

        await EPResources.create_extension_pack(
            client_info, request.body.repository_url, request.body.language
        )
        response.status(200).send('{"status": "ok"}')

        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'extension pack criada com sucesso',
            request.body
        )
        return

    } catch (exception) {
        logger.warn(`@api.extension_pack.create failed`)
        console.log(exception)
        if (exception instanceof EPResources.ExtPackError) {
            response.status(400).send(exception.message)
        } else {
            response.status(500).send(`extension pack creation failed`)
        }
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'não foi possível criar extension pack',
            request.body
        )
        return
    }

}


export async function delete_extension_pack (request, response) {
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.All])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.extension_pack.delete failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.extension_pack.delete failed to get client's credentials with error:`)
            console.log(e)
            response.status(500).send(`failed to delete extension pack`)
        }
        return
    }

    try {

        await EPResources.delete_extension_pack(client_info)
        response.status(200).send('{"status": "ok"}')

        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'extension pack removida com sucesso',
            request.body
        )
        return

    } catch (exception) {
        logger.warn(`@api.extension_pack.delete failed`)
        console.log(exception)
        if (exception instanceof EPResources.ExtPackError) {
            response.status(400).send(exception.message)
        } else {
            response.status(500).send(`failed to delete extension pack`)
        }
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'não foi possível remover extension pack',
            request.body
        )
        return
    }
}


export async function start_extension_pack (request, response) {
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.All])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.extension_pack.start failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.extension_pack.start failed to get client's credentials with error:`)
            console.log(e)
            response.status(500).send(`failed to start extension pack`)
        }
        return
    }

    try {

        await EPResources.start_extension_pack(client_info)
        response.status(200).send('{"status": "ok"}')

        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'extension pack iniciada com sucesso',
            request.body
        )
        return

    } catch (exception) {
        logger.warn(`@api.extension_pack.start failed`)
        console.log(exception)
        if (exception instanceof EPResources.ExtPackError) {
            response.status(400).send(exception.message)
        } else {
            response.status(500).send(`failed to start extension pack`)
        }
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'não foi possível iniciar extension pack',
            request.body
        )
        return
    }
}


export async function stop_extension_pack(request, response) {
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.All])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.extension_pack.stop failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.extension_pack.stop failed to get client's credentials with error:`)
            console.log(e)
            response.status(500).send(`failed to stop extension pack`)
        }
        return
    }

    try {

        await EPResources.stop_extension_pack(client_info)
        response.status(200).send('{"status": "ok"}')

        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'extension pack parada com sucesso',
            request.body
        )
        return

    } catch (exception) {
        logger.warn(`@api.extension_pack.stop failed`)
        console.log(exception)
        if (exception instanceof EPResources.ExtPackError) {
            response.status(400).send(exception.message)
        } else {
            response.status(500).send(`failed to stop extension pack`)
        }
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'não foi possível para extension pack',
            request.body
        )
        return
    }
}


export async function update_extension_pack (request, response) {
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.All])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.extension_pack.update failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.extension_pack.update failed to get client's credentials with error:`)
            console.log(e)
            response.status(500).send(`failed to update extension pack`)
        }
        return
    }

    try {

        await EPResources.update_extension_pack(client_info, request.body.language, request.body.repository_url)
        response.status(200).send('{"status": "ok"}')

        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'extension pack atualizada com sucesso',
            request.body
        )
        return

    } catch (exception) {
        logger.warn(`@api.extension_pack.update failed`)
        console.log(exception)
        if (exception instanceof EPResources.ExtPackError) {
            response.status(400).send(exception.message)
        } else {
            response.status(500).send(`failed to update extension pack`)
        }
        await Common.log_err(
            client_info,
            Common.ClientOperation.Compute,
            'não foi possível atualizar extension pack',
            request.body
        )
        return
    }
}