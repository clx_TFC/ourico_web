import * as Common        from '../common'
import * as Control       from '../api/control'
import * as Auth          from '../auth'


import { loggers }        from '../../logger.js'
import { DbOpError }      from '../../db/common'
import { QueryExecError } from '../api/resources'


const logger = loggers.web


export const new_admin = async (request, response) => {
    if (request.body.data === undefined) {
        logger.verbose('@api.control no admin data supplied')
        response.status(400)
                .send('No admin data supplied')
        return
    }

    try {

        let new_token = await Control.new_admin(request.body.data)
        response.status(200).send(new_token)

    } catch (e) {
        if (e instanceof Control.ControlOperationError) {
            logger.verbose(`@api.control failed create new admin account: ${e.message}`)
            response.status(400).send(e.message)
        }
        else {
            logger.warn(`@api.control failed to create admin with error: ${e}`)
            response.status(500).send(`failed to execute control operation`)
        }
    }
}


export const delete_client = async (request, response) => {
    if (request.body.data === undefined) {
        logger.verbose('@api.control no client data supplied')
        response.status(400)
                .send('No admin data supplied')
        return
    }


    let admin_info: Auth.ClientAuthInfo
    let token     : string

    try {
        token = await Auth.extract_token(request.get('Authorization'))
        admin_info = await Common.get_auth_info(token, [Auth.Capabilities.All])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.control failed to delete client with error: ${e}`)
            response.status(500).send(`failed to execute control operation`)
        }
        return
    }

    try {

        console.log('..... -> admin info', admin_info)
        await Control.delete_client(admin_info, token, request.body.data)
        response.status(200).send('{"status": "ok"}')

        console.log('..... -> admin info', admin_info)
        await Common.log_info(
            admin_info,
            Common.ClientOperation.Control,
            'cliente eliminado',
            request.body
        )
        return

    } catch (e) {
        if (e instanceof Control.ControlOperationError) {
            logger.verbose(`@api.control failed delete client account: ${e.message}`)
            response.status(400).send(`failed delete client account: ${e.message}`)
        }
        else if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                    .set('WWW-Authenticate', e.to_string())
                    .send()
        }
        else {
            logger.warn(`@api.control failed to delete client with error:`)
            console.log(e)
            response.status(500).send(`failed to execute control operation`)
        }
    }


    await Common.log_err(
        admin_info,
        Common.ClientOperation.Control,
        'falha a eliminar cliente',
        request.body
    )

}


export const update_client = async (request, response) => {
    if (request.body.data === undefined) {
        logger.verbose('@api.control no new data supplied')
        response.status(400)
                .send('No new data supplied')
        return
    }

    let admin_info: Auth.ClientAuthInfo
    let token: string

    try {
        token = await Auth.extract_token(request.get('Authorization'))
        admin_info = await Common.get_auth_info(token, [Auth.Capabilities.All])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.control failed to delete client with error:`)
            console.log(e)
            response.status(500).send(`failed to execute control operation`)
        }
        return
    }


    try {

        let new_token =
            await Control.update_client_info(admin_info, token, request.body.data)
        response.status(200).send(JSON.stringify({ new_token: new_token }))

        await Common.log_info(
            admin_info,
            Common.ClientOperation.Control,
            'cliente atualizado',
            '{}'
        )
        return

    } catch (e) {
        if (e instanceof Control.ControlOperationError) {
            logger.verbose(`@api.control failed update client account: ${e.message}`)
            response.status(400).send(`failed update client account: ${e.message}`)
        }
        else if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                    .set('WWW-Authenticate', e.to_string())
                    .send()
        }
        else {
            logger.warn(`@api.control failed to update client with error:`)
            console.log(e)
            response.status(500).send(`failed to execute control operation`)
        }
    }

    await Common.log_err(
        admin_info,
        Common.ClientOperation.Control,
        'falha a atualizar cliente',
        request.body
    )
}


export const update_token = async (request, response) => {

    let client_info: Auth.ClientAuthInfo
    let token: string

    try {
        token = await Auth.extract_token(request.get('Authorization'))
        // get auth info without checking expiration
        client_info = await Common.get_auth_info(token, [], false)
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.control failed to delete client with error:`)
            console.log(e)
            response.status(500).send(`failed to execute control operation`)
        }
        return
    }

    try {

        let new_token = await Control.update_token(token, client_info)
        response.status(200).send(JSON.stringify({ new_token: new_token }))

        await Common.log_info(
            client_info,
            Common.ClientOperation.Control,
            'token atualizado',
            request.body
        )
        return

    } catch (e) {
        if (e instanceof Control.ControlOperationError) {
            logger.verbose(`@api.control failed update token: ${e.message}`)
            response.status(400).send(`failed update token: ${e.message}`)
        }
        else if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                    .set('WWW-Authenticate', e.to_string())
                    .send()
        }
        else {
            logger.warn(`@api.control failed to update token:`)
            console.log(e)
            response.status(500).send(`failed to execute control operation`)
        }
    }

    await Common.log_err(
        client_info,
        Common.ClientOperation.Control,
        'falha a atualizar token',
        request.body
    )
}


export const revoke_token = async (request, response) => {
    try {

        let token: string = await Auth.extract_token(request.get('Authorization'))
        await Control.revoke_token(token)
        response.status(200).send()

    } catch (e) {
        if (e instanceof Control.ControlOperationError) {
            logger.verbose(`@api.control failed revoke token: ${e.message}`)
            response.status(400).send(`failed revoke token: ${e.message}`)
        }
        else if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                    .set('WWW-Authenticate', e.to_string())
                    .send()
        }
        else if (e instanceof Auth.NonExistingTokenError) {
            logger.warn(`@api.control failed to revoke token: ${e}`)
            response.status(400).send(`failed to revoke token`)
        }
        else {
            logger.warn(`@api.control failed to revoke token: ${e}`)
            response.status(500).send(`failed to execute control operation`)
        }
    }
}


export const login = async (request, response) => {
    if (request.body.username === undefined) {
        logger.verbose('@api.control no username supplied')
        response.status(400)
                .send('No username supplied')
        return
    }
    if (request.body.password === undefined) {
        logger.verbose('@api.control no password supplied')
        response.status(400)
                .send('No password supplied')
        return
    }

    try {

        let new_token =
            await Control.login(request.body.username, request.body.password)
        response.status(200).send(new_token)

    } catch (e) {
        if (e instanceof Control.ControlOperationError) {
            logger.verbose(`@api.control failed to login: ${e.message}`)
            response.status(401).send(`${e.message}`)
        }
        else {
            logger.warn(`@api.control failed to login: ${e}`)
            response.status(500).send(`failed to execute control operation`)
        }
    }
}


export const logout = async (request, response) => {
    try {

        let token: string = await Auth.extract_token(request.get('Authorization'))
        await Control.logout(token)
        response.status(200).send()

    } catch (e) {
        if (e instanceof Control.ControlOperationError) {
            logger.verbose(`@api.control failed to logout: ${e.message}`)
            response.status(400).send(`failed to logout: ${e.message}`)
        }
        else if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.control failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                    .set('WWW-Authenticate', e.to_string())
                    .send()
        }
        else if (e instanceof Auth.NonExistingTokenError) {
            logger.warn(`@api.control failed to logout: ${e}`)
            response.status(400).send(`failed to logout`)
        }
        else {
            logger.warn(`@api.control failed to logout: ${e}`)
            response.status(500).send(`failed to execute control operation`)
        }
    }
}