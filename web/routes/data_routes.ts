import * as Common        from '../common'
import * as DataResources from '../api/data_resources'
import * as Auth          from '../auth'

import { Filter, FilterConverter, ParseError, Query }    from '../api/query'
import { JsonConvert, OperationMode, ValueCheckingMode } from 'json2typescript'

import { loggers }        from '../../logger.js'
import { DbOpError }      from '../../db/common'
import { QueryExecError } from '../api/resources'


const logger = loggers.web

const json_converter: JsonConvert    = new JsonConvert()
json_converter.ignorePrimitiveChecks = false
json_converter.valueCheckingMode     = ValueCheckingMode.DISALLOW_NULL


/**
 * To make queries
 */
export const read = async (request, response) => {

    if (request.body.query == null) {
        logger.verbose('@api.data.get query without body')
        response.status(400)
                .send('No query was found in the body')
        return
    }

    let q = request.body.query
    let queries : Query[] = undefined
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.Read])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.data.get failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                    .set('WWW-Authenticate', e.to_string())
                    .send()
        } else {
            logger.warn(`@api.data.get failed to execute query with unknown error:`)
            console.log(e)                
            response.status(500).send(`failed to execute query`)
        }
        return
    }

    try {
        if (q instanceof Array) {
            queries = json_converter.deserialize(q, Query)
        } else {
            let query   = json_converter.deserialize(q, Query)
            queries = [query]
        }
    } catch (exception) {
    logger.debug(`@api.data.get failed to parse query: ${exception.message}`)
        console.log('the query', q)
        if (exception instanceof ParseError)
            response.status(400).send(`failed to parse query: ${exception.message}`)
        else
            response.status(400).send(`failed to parse query`)
        
        await Common.log_err(
            client_info,
            Common.ClientOperation.Read,
            'pedido com formato inválido',
            request.body
        )
        return
    }


    if (queries !== undefined) {
        try {

            let result: object[] =
                await DataResources.execute_queries(queries, client_info)
            response.status(200).send(JSON.stringify(result))

            // await Common.log_info(
            //     client_info,
            //     Common.ClientOperation.Read,
            //     'operação executada com sucesso',
            //     request.body
            // )
            return

        } catch (e) {
            if (e instanceof QueryExecError || e instanceof DbOpError) {
                logger.verbose(`@api.data.get failed to execute query: ${e.message}`)
                response.status(400).send(`failed to execute query: ${e.message}`)
            } else {
                logger.warn(`@api.data.get failed to execute query with unknown error:`)
                console.log(e)                
                response.status(500).send(`failed to execute query`)
            }
        }
    } else {
        logger.error('@api.data.get no query')
        response.status(500).send()
    }

    await Common.log_err(
        client_info,
        Common.ClientOperation.Read,
        'execução falhada',
        request.body
    )
}


/**
 * To create new items
 */
export const write = async (request, response) => {
    if (request.body.resource == null) {
        logger.verbose('@api.data.write no resource indicated')
        response.status(400)
                .send('No resource name was indicated')
        return
    }
    if (request.body.data == null) {
        logger.verbose('@api.data.write no data supplied')
        response.status(400)
                .send('No data was supplied')
        return
    }

    logger.debug('@api.data.new executing a write')
    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.Write])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.data.write failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.data.write failed to execute write with unknown error:`)
            console.log(e)
            response.status(500).send(`failed to execute write`)
        }
        return
    }

    try {

        let resource: string = request.body.resource
        let data: object[]   = request.body.data
        let result: any[] = await DataResources.execute_write(resource, data, client_info)
        response.status(200).send(JSON.stringify(result))

        await Common.log_info(
            client_info,
            Common.ClientOperation.Write,
            'operação executada com sucesso',
            '{}'
        )
        return

    } catch (e) {
        if (e instanceof QueryExecError || e instanceof DbOpError) {
            logger.verbose(`@api.data.write failed to execute write: ${e.message}`)
            response.status(400).send(`failed to execute write: ${e.message}`)
        } else {
            logger.warn(`@api.data.write failed to execute write with unknown error:`)
            console.log(e)            
            response.status(500).send(`failed to execute write`)
        }
    }

    await Common.log_err(
        client_info,
        Common.ClientOperation.Write,
        'execução falhada',
        request.body
    )
}


/**
 * To update items
 */
export const update = async (request, response) => {
    if (request.body.resource == null) {
        logger.verbose('@api.data.put no resource indicated')
        response.status(400)
                .send('No resource name was indicated')
        return
    }
    if (request.body.where == null) {
        logger.verbose('@api.data.put no filter (where) indicated')
        response.status(400)
                .send('No filter (where to update) was indicated')
        return
    }
    if (request.body.data == null) {
        logger.verbose('@api.data.put no data supplied')
        response.status(400)
                .send('No data was supplied')
        return
    }

    logger.debug('@api.data.put executing an update')

    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.Write])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.data.update failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.data.update failed to execute update with unknown error:`)
            console.log(e)
            response.status(500).send(`failed to execute update`)
        }
        return
    }

    try {

        let resource: string = request.body.resource
        let new_data: object[] = request.body.data
        let where: Filter[] = new FilterConverter().deserialize(request.body.where)
        let result: number =
            await DataResources.execute_update(resource, new_data, where, client_info)
        response.status(200).send(JSON.stringify(result))

        await Common.log_info(
            client_info,
            Common.ClientOperation.Update,
            'operação executada com sucesso',
            '{}'
        )
        return

    } catch (e) {
        if (e instanceof QueryExecError || e instanceof DbOpError) {
            logger.verbose(`@api.data.put failed to execute update: ${e.message}`)
            response.status(400).send(`failed to execute update: ${e.message}`)
        } else {
            logger.warn(`@api.data.put failed to execute update with unknown error:`)
            console.log(e)            
            response.status(500).send(`failed to execute update`)
        }
    }

    await Common.log_err(
        client_info,
        Common.ClientOperation.Update,
        'execução falhada',
        request.body
    )
}


/**
 * To delete items
 */
export const del = async (request, response) => {
    if (request.body.resource == null) {
        logger.verbose('@api.data.delete no resource indicated')
        response.status(400)
                .send('No resource name was indicated')
        return
    }
    if (request.body.where == null) {
        logger.verbose('@api.data.delete no filter (where) indicated')
        response.status(400)
                .send('No filter (where to delete) was indicated')
        return
    }

    logger.debug('@api.data.delete executing a delete')

    let client_info

    try {
        let token = await Auth.extract_token(request.get('Authorization'))
        client_info = await Common.get_auth_info(token, [Auth.Capabilities.Write])
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@api.data.delete failed to validate client: ${e.to_string()}`)
            response.status(e.error_code)
                .set('WWW-Authenticate', e.to_string())
                .send()
        } else {
            logger.warn(`@api.data.delete failed to execute delete with unknown error:`)
            console.log(e)
            response.status(500).send(`failed to execute delete`)
        }
        return
    }

    try {

        let resource: string = request.body.resource
        let where: Filter[] = new FilterConverter().deserialize(request.body.where)
        let res = await DataResources.execute_delete(resource, where, client_info)
        response.status(200).send('{"status": "ok"}')

        await Common.log_info(
            client_info,
            Common.ClientOperation.Delete,
            'operação executada com sucesso',
            '{}'
        )
        return

    } catch (e) {
        if (e instanceof QueryExecError || e instanceof DbOpError) {
            logger.verbose(`@api.data.delete failed to execute delete: ${e.message}`)
            response.status(400).send(`failed to execute delete: ${e.message}`)
        } else {
            logger.warn(
                `@api.data.delete failed to execute delete with unknown error: `
            )
            console.log(e)
            response.status(500).send(`failed to execute delete`)
        }
    }

    await Common.log_err(
        client_info,
        Common.ClientOperation.Delete,
        'execução falhada',
        request.body
    )
}
