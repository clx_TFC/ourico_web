/**
 * The routes are divided in 5 categories:
 * * api.data      - to make CRUD operations over data
 * * api.compute   - to execute compute scripts
 * * api.control   - to execute client control operations (e.g create admin)
 * * api.extension - to call operations in extension packs
 * * api.mote      - to send instructions to motes
 */


import express     = require('express')
import body_parser = require('body-parser')

import { loggers }        from '../logger.js'

import * as DataRoutes    from './routes/data_routes'
import * as MoteRoutes    from './routes/mote_routes'
import * as ControlRoutes from './routes/control_routes'
import * as EPRoutes      from './routes/extension_routes'

const config = require('../config.json')


const logger = loggers.web
let rsa_pub_key : Buffer = undefined
let rsa_priv_key: Buffer = undefined


const app = express()
app.set('port', config.web_server_port)
app.use(body_parser.urlencoded({ extended: true, limit: '50mb' }))
app.use(body_parser.json({ limit: '50mb' }))



/****************************************************************************************/
// api.data routes

app.post  ('/api.data/get', DataRoutes.read)
app.post  ('/api.data/new', DataRoutes.write)
app.put   ('/api.data/',    DataRoutes.update)
app.delete('/api.data/',    DataRoutes.del)



/****************************************************************************************/
// api.mote routes

/**
 * Execute mote operation
 */
app.post('/api.mote/', MoteRoutes.unique)



/****************************************************************************************/
// api.control routes


/**
 * Create a new administrator account.
 */
app.post('/api.control/new_admin', ControlRoutes.new_admin)


/**
 * Delete a client's account. Only admins can perform this operation
 */
app.post('/api.control/delete_client', ControlRoutes.delete_client)


/**
 * Update a client's control information (capabilities). Only admins can perform
 * this operation
 */
app.post('/api.control/update_client', ControlRoutes.update_client)


/**
 * Invalidate the current token and create a new one
 */
app.post('/api.control/update_token', ControlRoutes.update_token)


/**
 * Invalidate the current token
 */
app.post('/api.control/revoke_token', ControlRoutes.revoke_token)


/**
 * Execute login
 */
app.post('/api.control/login', ControlRoutes.login)


/**
 * Execute logout
 */
app.post('/api.control/logout', ControlRoutes.logout)



/****************************************************************************************/
// api.extension routes

/**
 * Subcribe extension pack to events from certain motes
 */
app.post('/api.extension_pack/subscribe_to_motes', EPRoutes.subscribe_to_mote_events)


/**
 * Unubcribe extension pack from events from certain motes
 */
app.post('/api.extension_pack/unsubscribe_to_motes', EPRoutes.unsubscribe_to_mote_events)


/**
 * Send a client request to the extension pack
 */
app.post('/api.extension_pack/client_request', EPRoutes.send_client_request)


/**
 * Create extension pack
 */
app.post('/api.extension_pack/create', EPRoutes.create_extension_pack)


/**
 * Delete extension pack
 */
app.post('/api.extension_pack/delete', EPRoutes.delete_extension_pack)


/**
 * Delete extension pack
 */
app.post('/api.extension_pack/update', EPRoutes.update_extension_pack)


/**
 * Start extension pack
 */
app.post('/api.extension_pack/start', EPRoutes.start_extension_pack)


/**
 * Stop extension pack
 */
app.post('/api.extension_pack/stop', EPRoutes.stop_extension_pack)



export function start () {
    app.listen(app.get('port'), () => {
        logger.info(`@server started in port ${app.get('port')}`)
    })
}
