/**
 * Provides functinality to validate and generate JWT (RFC 7516) Authentication tokens.
 * The authentication framework follows the directives of RFC 6750 (Bearer Token Usage).
 *
 * JWT payloads are as follows:
 * ```json
 * {
 *   "iss": "Ourico",
 *   "aud": ["user"|"ext_client"]'%'<cliend_id>'%'<group id>,
 *   "exp": <expiration date as seconds since epoch (unix time)>,
 *   "scope": <array of user capabilities>,
 *   "slt": <salt>
 * }```
 * When the audience(owner) (`"aud"`) starts with `"user"` the client is a regular user
 * and when it is `"ext_client"` the client is any
 * authorized entity (e.g. an App or a Web site with a token provided by a regular user).
 *
 * The salt is just used to invalidate the token. We hold it and use it to check the token
 * when we want to invalidate that token we just delete it.
 *
 * Tokens are signed with algorithm RSASSA-PKCS1-v1_5 using SHA-256 and a 2048 bits key, as
 * specified in RFC 7519 (JWA) and RFC 7518 (JWS).
 */



import * as Jwt         from 'jsonwebtoken'
import * as rand        from 'random-number-csprng'
import * as Redis       from 'redis'
import * as Fs          from 'fs'
import * as Bluebird    from 'bluebird'
import * as QueryString from 'query-string'
import { loggers }      from '../logger.js'


Bluebird.promisifyAll(Redis.RedisClient.prototype)
Bluebird.promisifyAll(Redis.Multi.prototype)

const config = require('../config.json')
const logger = loggers.web
const redis_client = Redis.createClient()

const rsa_pub_key  = Fs.readFileSync(config["rsa_2048_pub_key"])
const rsa_priv_key = Fs.readFileSync(config["rsa_2048_key"])


export class ClientAuthInfo {
    constructor (
        public client_id      : string,
        public client_type    : ClientType,
        public group_id       : number,
        public expiration_date: number,
        public capabilities   : Capabilities[]
    )
    { }

    to_string () {
        return `id=${this.client_id}, ` +
            `type=${this.client_type}, group=${this.group_id}`
    }
}


/**
 * Capabilities that a user can have
 */
export enum Capabilities {
    // Allowed to read values
    Read     = 1,
    // Allowed to read, write, update and delete values
    Write    = 2,
    // Allowed to execute compute operations
    Compute  = 3,
    // Allowed to receive real time events
    RtEvents = 4,
    // Allowed to perform any operation
    All      = 5
}


/**
 * Types of clients
 */
export enum ClientType {
    // Regular user
    User      = 1,
    // Entity using the api on behalf of a regular user
    ApiClient = 2
}


/** Issuer of the tokens (iss field) */
const ISSUER = "Ourico"


export class ValidationError {
    constructor (
        // one of the values in Error
        public error     : string,
        // an HTTP error code (4**)
        public error_code: number,
        public error_desc: string
    )
    { }

    to_string (): string {
        if (this.error == '' || this.error_desc == '')
            return `Bearer realm="ourico"`
        return `Bearer realm="ourico", error="${this.error}", error_description="${this.error_desc}"`
    }
}

export class NonExistingTokenError {
    constructor (public message: string) { }
}


/**
 * Token validation error codes defined by RFC 6750
 */
export const Error = {
    // expired, revoked or malformed token
    InvalidToken      : "invalid_token",
    // request missing parameters
    InvalidRequest    : "invalid_request",
    // client does not have the required capabilities
    InsufficientScope : "insufficient_scope"
}


/**
 * Validate a JWT token signed with RS256
 *
 * @param token
 * @param required_caps capabilities necessary to validate token
 *                      token has to satisfy every capability in the list
 * @param check_expiration indicates if expiration date in token should be checked
 *
 * @return the token's payload or the object { err, err_code, error_desc } with error info
 */
export async function validate_token (

    token: string,
    necessary_caps: Capabilities[],
    check_expiration: boolean = true

): Promise<ClientAuthInfo|ValidationError> {

    if (!check_expiration)
        logger.info(`@auth.validate_token validating token without checking expiration`)
    else
        logger.info(`@auth.validate_token validating token checking expiration`)

    let salt = ''
    if (check_expiration) {
        // if token has expired the salt will have been
        // removed from redis
        // so we only get it if expiration check is necessary
        try {

            // get salt used in the token
            salt = await redis_client.getAsync(`token_salt:${token}`)
            if (salt == null) {
                // logger.warn('@auth invalid token: no salt')
                throw new ValidationError (Error.InvalidToken, 401, 'invalid token')
            }

        } catch (e) {
            logger.warn('@auth.validate_token failed to get token\'s salt')
            logger.warn(`@auth.validate_token the error: `, e)
            throw new ValidationError(Error.InvalidRequest, 400, "unable to validate token")
        }
    }

    try {

        var payload = Jwt.verify(token, rsa_pub_key, {
            algorithms     : ['RS256'],
            checkExpiration: check_expiration,
            issuer         : ISSUER
        })

        // verifiy capabilities
        for (var ncap of necessary_caps) {
            // if client has All he will be allowed to perform any operation
            if (!payload.scope.find(cap => cap == Capabilities.All || cap == ncap)) {
                let error = new ValidationError(
                    Error.InsufficientScope,
                    403,
                    'operation not permitted with current capabilities'
                )
                return error
            }
        }
        // verify salt
        if (check_expiration && payload.slt !== salt) {
            logger.warn(`@auth.validate_token salt mismatch: ${payload.slt} vs ${salt}`)
            throw new ValidationError (Error.InvalidToken, 401, 'invalid token')
        }


        let aud_parts = payload.aud.split('%')
        let jwt_payload = new ClientAuthInfo(
            aud_parts[1],
            parseInt(aud_parts[0]),
            parseInt(aud_parts[2]),
            parseInt(payload.exp),
            payload.scope
        )

        logger.debug(`@auth.validate_token validated ${jwt_payload.to_string()}`)

        return jwt_payload

    } catch (exception) {

        logger.debug('@auth validate_token error ', exception)

        if (exception instanceof Jwt.JsonWebTokenError) {
            logger.warn('@auth.validate_token failed to validate token with TokenError')
            logger.debug(`@auth.validate_token JWT validator error ${exception}`)
            throw new ValidationError (Error.InvalidToken, 401, 'invalid token')
        } else if (exception instanceof Jwt.TokenExpiredError) {
            logger.warn('@auth.validate_token failed to validate token with ExpiredError')
            throw new ValidationError(
                Error.InvalidToken,
                401,
                `token expiret at ${new Date(exception.expiredAt*1000)}`
            )
        } else {
            logger.warn('@auth.validate_token failed to validate token with UnkownError')
            logger.debug(`@auth.validate_token JWT validator error ${exception.message}`)
            throw new ValidationError(Error.InvalidRequest, 400, "unable to validate token")
        }
    }
}


/**
 * Create a new JWT token using RS256
 *
 * @param auth_info data to create token
 *
 * @return the token's payload object
 * @throws exception in case of failure
 */
export async function generate_token (auth_info: ClientAuthInfo): Promise<string> {

    logger.debug(`@auth.generate_token token for ${auth_info.to_string()}`)

    let token_payload = {
        iss  : 'Ourico',
        scope: auth_info.capabilities,
        aud  : `${auth_info.client_type}%${auth_info.client_id}%${auth_info.group_id}`,
        exp  : Date.now() + config.token_expiration_time,
        slt  : await rand_string(10)
    }
    let token = Jwt.sign(token_payload, rsa_priv_key, { algorithm: 'RS256' })
    // the salt expires at the same time as the token
    await redis_client.setAsync(
        `token_salt:${token}`, token_payload.slt, 'EX', config.token_expiration_time
    )

    logger.debug(`@auth.generate_token new token generated`)

    return token
}



/**
 * Generate a random string using a CSRNG
 *
 * @param len length of the string to produce
 *
 * @return the randomly generated string
 */
export async function rand_string (len: number): Promise<string> {

    let letters = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
        'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
        'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'
    ]
    let numbers_and_letters = [
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
        'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r',
        's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
        'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
    ]

    let result = ''
    // first character should be a letter
    result += letters[await rand(0, letters.length-1)]

    for (var i = 1; i < len; i++) {
        result += numbers_and_letters[await rand(0, numbers_and_letters.length-1)]
    }

    return result
}


/**
 * Extract the token from the value in the HTTP 'Authorization' header
 *
 * @param auth_field should have form 'Beader <token>'
 * @param url        url of the request. if the token is not present in the header
 *                   it is looked for in the query parameter access_token
 *
 * @return the token as a string
 * @throws ValidationError
 */
export function extract_token
(auth_field: string, url: string = undefined): string {

    if (auth_field !== undefined) {
        if (!auth_field.startsWith('Bearer '))
            throw new ValidationError(
                Error.InvalidRequest,
                400,
                'Authorization header format accepted is "Bearer <token>"'
            )
        logger.debug('@auth.extract_token token found in header')
        return auth_field.substr('Bearer '.length)
    }

    else if (url !== undefined) {
        let url_parts = QueryString.parse(url)
        if (url_parts.access_token !== undefined) {
            logger.debug('@auth.extract_token token found in url')
            return url_parts.access_token
        }
    }

    logger.debug('@auth.extract_token no token found')
    throw new ValidationError('', 401, '')
}



/**
 * Just removes the token's salt from the redis store, thus rendering the token
 * useless (no way to validate it)
 *
 * @param token
 * @param check_non_existance if true an error is thrown if the token did not exist
 */
export async function invalidate_token
(token: string, check_non_existance: boolean = true) {
    try {
        console.log('invalidate the token ', token)
        let res = await redis_client.delAsync(`token_salt:${token}`)
        console.log('received ', res, typeof res)
        if (res == 0) {// nothing deleted
            if (check_non_existance) {
                logger.warn(`@auth.invalidate_token token was not present in redis`)
                throw new NonExistingTokenError(`unkown token supplied`)
            }
            logger.info(`@auth.invalidate_token token was not present in redis`)
        }
    } catch (e) {
        logger.warn(`@auth.invalidate_token failed to remove salt from redis`)
        logger.warn(`@auth.invalidate_token the error:`)
        console.log(e)
        throw `failed to invalidate token`
    }
}


/**
 * Check if password follows requirements.
 *
 * @param password
 *
 * @throws  exception
 */
export function validate_password (password: string) {
    let error: string = 'password must be longer than 8 characters, ' +
        'have numbers, lowercase letters and uppercase letters'

    if (password.length < 8)
        throw error

    let contains_int = /\d/g
    let contains_uc  = /[A-Z]/g
    let contains_lc  = /[a-z]/g

    if (!contains_int.test(password)
     || !contains_lc.test(password)
     || !contains_uc.test(password))
        throw error
}
