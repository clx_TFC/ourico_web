import * as Auth from './auth'
import * as Fs   from 'fs'

import { UserOperationsLog, ApiOperationsLog } from '../db/models'


/**
 * Types of operations that clients can perform
 */
export enum ClientOperation {
    Read            = 'read',
    Write           = 'write',
    Update          = 'update',
    Delete          = 'delete',
    Compute         = 'compute',
    /// requested to listen to events
    RtEvents        = 'rt_events',
    /// sent a mote operation
    MoteInstruction = 'mote_instruction',
    /// client and token management operations
    Control         = 'control'
}


export enum LogLevel {
    Info    = 'info',
    Warning = 'warning',
    Error   = 'error'
}



/**
 * Log a client operation to the database
 *
 * @param client    client performing the operation
 * @param operation the operation performed
 * @param level     log level
 * @param message   log message
 * @param query     query data
 */
export async function log_client_operation (
    client: Auth.ClientAuthInfo,
    operation: ClientOperation,
    status: LogLevel,
    message: string,
    query: string
) {

    let log = {
        date_time: new Date(),
        operation: operation,
        level    : status,
        message  : message,
        query    : query,
        group_id : client.group_id
    }

    let query_builder

    if (client.client_type === Auth.ClientType.User) {
        log['client_id'] = client.client_id
        query_builder = UserOperationsLog
            .createQueryBuilder()
            .insert()
            .into(UserOperationsLog)

    } else { // ApiClient
        log['api_client_id'] = client.client_id
        query_builder = ApiOperationsLog
            .createQueryBuilder()
            .insert()
            .into(ApiOperationsLog)
    }
    query_builder.values(log)
    await query_builder.execute()
}


/**
 * @throws ValidationError if token not valid
 */
export async function get_auth_info (
    token: string,
    capabilites: Auth.Capabilities[],
    check_expiration: boolean = true
): Promise<Auth.ClientAuthInfo> {

    let auth_info = await Auth.validate_token(token, capabilites, check_expiration)
    if (auth_info instanceof Auth.ValidationError) {
        throw auth_info
    }

    return auth_info
}


export const log_err  =
(client: Auth.ClientAuthInfo, operation: ClientOperation, message: string, query: string) =>
log_client_operation(client, operation, LogLevel.Error, message, query)

export const log_info  =
(client: Auth.ClientAuthInfo, operation: ClientOperation, message: string, query: string) =>
log_client_operation(client, operation, LogLevel.Info, message, query)

export const log_warn  =
(client: Auth.ClientAuthInfo, operation: ClientOperation, message: string, query: string) =>
log_client_operation(client, operation, LogLevel.Warning, message, query)