/**
 * Query parsing and validation functionality
 *
 *  The query is defined in json format:
 *  {
 *      "resource": "<resource name>"
 *      "fields": ["a", "b", ...],
 *      "filter": [{ "attr_a": x }, { "attr_b": { ">": y } }, ...],
 *      "options": [ { "orddesc": "attr_c" }, ... ],
 *      "include": [ ... recursive query to related resources ... ]
 *  }
 *  "resource" - is the target of the query. it can be, for example, a table in a db
 *  "fields"   - the fields to be selected (if it applies)
 *  "filter"   - to filter the result. lists inside the filter list will be or'ed.
 *             - values in the same field will be and'ed
 *  "options"  - how to structure the result
 *  "include"  - subqueries to related resources (e.g. tables connected to the first one)
 */


import { loggers } from '../../logger.js'
import { JsonConverter, JsonCustomConvert, JsonProperty, JsonObject } from 'json2typescript'


export class ParseError {
    constructor (public message: string) { }
}


@JsonConverter
export class FilterConverter implements JsonCustomConvert<Filter[]> {

    private cc: ComparisonConverter

    constructor () { this.cc = new ComparisonConverter() }

    do_serialize (filter: Filter): any {
        return `${filter.field} ${this.cc.serialize(filter.comparison)}`
    }
    serialize (filters: Filter[]): any {
        let arr: any[] = []
        for (var filter of filter) {
            arr.push(this.do_serialize(filter))
        }
        return arr
    }

    do_deserialize (input: any): Filter {
        if (typeof input === 'object' && !(input instanceof Array)) {
            let keys = Object.keys(input)
            if (keys.length != 1)
                throw new ParseError(`filter object should be { "<field name>": {"<comparison>": <target>} }`)

            let filter = new Filter()
            filter.field      = keys[0]
            filter.comparison = this.cc.deserialize(input[keys[0]])
            return filter
        } else {
            throw new ParseError(`filter object should be { "<field name>": {"<comparison name>": <target>} }`)
        }
    }
    deserialize (input: any): Filter[] {
        if (!(input instanceof Array))
            throw new ParseError(`the 'filters' field should contain a list of filters`)
        let arr: Filter[] = []
        for (var i of input) {
            arr.push(this.do_deserialize(i))
        }
        return arr
    }

}


@JsonConverter
class ComparisonConverter implements JsonCustomConvert<Comparison> {

    serialize (comparison: Comparison): any {
        return `${comparison.name} ${comparison.target}`
    }

    deserialize (input: any): Comparison {
        if (typeof input === 'object' && !(input instanceof Array)) {
            let keys = Object.keys(input)
            if (keys.length != 1)
                throw new ParseError(`comparison objects should be { "<comparison>": <target> }`)

            let comparison = new Comparison()
            comparison.name   = keys[0]
            comparison.target = input[keys[0]]
            return comparison
        } else {
            let comparison = new Comparison()
            comparison.name   = 'eq'
            comparison.target = input
            return comparison
        }
    }

}


@JsonConverter
class OptionConverter implements JsonCustomConvert<Option[]> {

    do_serialize (option: Option): any {
        return `${option.name} ${option.target}`
    }
    serialize (options: Option[]): any {
        let arr: any[] = []
        for (var option of options) {
            arr.push(this.do_serialize(option))
        }
        return arr
    }


    do_deserialize (input: any): Option {
        if (typeof input === 'object' && !(input instanceof Array)) {
            let keys = Object.keys(input)
            if (keys.length != 1)
                throw new ParseError(`options objects should be { "<option name>": <target> }`)

            let option = new Option()
            option.name   = keys[0]
            option.target = <string>input[keys[0]]
            return option
        } else if (typeof input === 'string') {
            let option = new Option()
            option.name = input
            option.target = undefined
            return option
        } else {
            throw new ParseError(`an option should be an object like { "<option name>": <target> } or a string`)
        }
    }
    deserialize (input: any): Option[] {
        if (!(input instanceof Array))
            throw new ParseError(`the 'options' field should contain a list of options`)
        let arr: Option[] = []
        for (var i of input) {
            arr.push(this.do_deserialize(i))
        }
        return arr
    }

}


@JsonObject
export class Query {

    @JsonProperty('resource', String, false)
    resource: string          = undefined

    @JsonProperty('fields', [String], true)
    public fields  : string[] = undefined

    @JsonProperty('filters', FilterConverter, true)
    public filters : Filter[] = undefined

    @JsonProperty('options', OptionConverter, true)
    public options : Option[] = undefined

    @JsonProperty('include', [Query], true)
    public includes: Query[]  = undefined

}


export class Filter {
    public field     : string     = undefined
    public comparison: Comparison = undefined
}


export class Option {
    public name  : string = undefined
    public target: string = undefined
}


export class Comparison {
    public name   : string = undefined
    public target : any    = undefined
}
