import * as MoteComm from '../../mote_comm/comm'
import * as Protocol from '../../mote_comm/protocol'
import * as Models   from '../../db/models'
import * as Auth     from '../auth'

import { getRepository }  from 'typeorm'
import { QueryExecError } from './resources'



/**
 * Send an operation instruction to a mote
 *
 * @param mote_id
 * @param operation
 * @param data        data associated with the operation
 * @param client_info info of the client executing the operation
 *
 */
export async function execute (

    mote_id: number,
    operation: string,
    data: object,
    client_info: Auth.ClientAuthInfo

): Promise<any> {

    let mote = await getRepository(Models.Mote)
        .createQueryBuilder(Models.Mote.table_name)
        .where(
            `${Models.Mote.table_name}.id = :id and ${Models.Mote.table_name}.group_id = :gid`,
            { id: mote_id, gid: client_info.group_id })
        .getOne()
    
    if (mote === undefined) {
        throw new QueryExecError(`Unkown mote ${mote_id}`)
    } else if (!mote.active) {
        throw new QueryExecError(`Mote is inactive`)
    }

    switch (operation) {

        case 'reset':
            return await MoteComm.send(mote_id, data, Protocol.PacketType.RESET)

        case 'request_status_report':
            return await MoteComm.send(
                mote_id, data, Protocol.PacketType.REQUEST_STATUS_REPORT
            )

        case 'request_measurements':
            return await MoteComm.send(
                mote_id, data, Protocol.PacketType.REQUEST_MEASUREMENTS
            )

        case 'action_instruction':
            return await action_instruction(mote_id, data, client_info)

        case 'change_configuration':
            return await change_conf(mote_id, data, client_info)

        case 'set_alert':
            return await change_alert_state(
                mote_id, data, Protocol.PacketType.ALERT_SET, client_info
            )

        case 'remove_alert':
            return await change_alert_state(
                mote_id, data, Protocol.PacketType.ALERT_REMOVE, client_info
            )

        case 'set_trigger':
            return await change_trigger_state(
                mote_id, data, Protocol.PacketType.TRIGGER_SET, client_info
            )

        case 'remove_trigger':
            return await change_trigger_state(
                mote_id, data, Protocol.PacketType.TRIGGER_REMOVE, client_info
            )

        case 'activate_sensor':
            return await change_sensor_state(
                mote_id, data, Protocol.PacketType.SENSOR_ACTIVATE, client_info
            )

        case 'deactivate_sensor':
            return await change_sensor_state(
                mote_id, data, Protocol.PacketType.SENSOR_DEACTIVATE, client_info
            )

        case 'activate_actuator':
            return await change_actuator_state(
                mote_id, data, Protocol.PacketType.ACTUATOR_ACTIVATE, client_info
            )

        case 'deactivate_actuator':
            return await change_actuator_state(
                mote_id, data, Protocol.PacketType.ACTUATOR_DEACTIVATE, client_info
            )

        default:
            throw new QueryExecError(`unknown mote operation ${operation}`)
    }

}


/**
 * Change alert state in a mote
 *
 * @param mote_id
 * @param data        should contain alert_id field
 * @param operation   PacketType.ALERT_SET to set or PacketType.ALERT_REMOVE to unset
 * @param client_info info fo the client making the request
 */
async function change_alert_state (

    mote_id: number,
    data: object,
    operation: Protocol.PacketType,
    client_info: Auth.ClientAuthInfo

): Promise<any> {

    if (data['alert_id'] === undefined)
        throw new QueryExecError(`field "alert_id" is necessary to change alert state`)

    let alert_id = data['alert_id']
    let alerts = await getRepository(Models.Alert)
        .createQueryBuilder(Models.Alert.table_name)
        .where(
            `${Models.Alert.table_name}.id = :id and ` +
            `${Models.Alert.table_name}.group_id = :gid`,
            { id: alert_id, gid: client_info.group_id })
        .getMany()

    // check if alert identified by alert_id really exists
    if (alerts.length == 0)
        throw new QueryExecError(`unknown alert ${alert_id}`)

    let alert_uses = await getRepository(Models.AlertUse)
        .createQueryBuilder(Models.AlertUse.table_name)
        .where(
            `${Models.AlertUse.table_name}.group_id = :gid and ` +
            `${Models.AlertUse.table_name}.mote_id = :mid and ` +
            `${Models.AlertUse.table_name}.alert_id = :aid`,
            { aid: alert_id, gid: client_info.group_id, mid: mote_id })
        .getMany()


    let alert_use

    if (alert_uses.length == 0) {
        throw new QueryExecError(
            `alert ${alert_id} is not currently associated with mote ${mote_id}`
        )
    } else {
        alert_use = alert_uses[0]
        if (alert_use.active && operation == Protocol.PacketType.ALERT_SET) {
            throw new QueryExecError(
                `alert ${alert_id} is already active in mote ${mote_id}`
            )
        }
        else if (!alert_use.active && operation == Protocol.PacketType.ALERT_REMOVE) {
            throw new QueryExecError(
                `alert ${alert_id} is not active in mote ${mote_id}`
            )
        }
    }

    data['alert_num'] = data['alert_id']
    await MoteComm.send(mote_id, data, operation)
    // alert_use.active   = operation == Protocol.PacketType.ALERT_SET
    // await alert_use.save()
}


/**
 * Change trigger state in a mote
 *
 * @param mote_id
 * @param data        should contain trigger_id field
 * @param operation   PacketType.TRIGGER_SET to set or PacketType.TRIGGER_REMOVE to unset
 * @param client_info info fo the client making the request
 */
async function change_trigger_state (

    mote_id: number,
    data: object,
    operation: Protocol.PacketType,
    client_info: Auth.ClientAuthInfo

): Promise<any> {

    if (data['trigger_id'] === undefined)
        throw new QueryExecError(`field "trigger_id" is necessary to change trigger state`)

    let trigger_id = data['trigger_id']
    let triggers = await getRepository(Models.Trigger)
        .createQueryBuilder(Models.Trigger.table_name)
        .where(
            `${Models.Trigger.table_name}.id = :id and ` +
            `${Models.Trigger.table_name}.group_id = :gid`,
            { id: trigger_id, gid: client_info.group_id })
        .getMany()

    // check if trigger identified by trigger_id really exists
    if (triggers.length == 0)
        throw new QueryExecError(`unknown trigger ${trigger_id}`)

    let tr_uses = await getRepository(Models.TriggerUse)
        .createQueryBuilder(Models.TriggerUse.table_name)
        .where(
            `${Models.TriggerUse.table_name}.group_id = :gid and ` +
            `${Models.TriggerUse.table_name}.mote_id = :mid and ` +
            `${Models.TriggerUse.table_name}.trigger_id = :tid`,
            { mid: mote_id, gid: client_info.group_id, tid: trigger_id })
        .getMany()

    let trigger_use

    if (tr_uses.length == 0) {
        throw new QueryExecError(
            `trigger ${trigger_id} is not currently associated with mote ${mote_id}`
        )
    } else {
        trigger_use = tr_uses[0]
        // check if trigger is already in the state required

        if (trigger_use.active && operation == Protocol.PacketType.TRIGGER_SET) {
            throw new QueryExecError(
                `trigger ${trigger_id} is already active in mote ${mote_id}`
            )
        }
        else if (!trigger_use.active && operation == Protocol.PacketType.TRIGGER_REMOVE) {
            throw new QueryExecError(
                `trigger ${trigger_id} is not active in mote ${mote_id}`
            )
        }
    }

    data['trigger_num'] = data['trigger_id']
    await MoteComm.send(mote_id, data, operation)
    // trigger_use.active = operation == Protocol.PacketType.TRIGGER_SET
    // await trigger_use.save()
}


/**
 * Change the configurations in a mote
 *
 * @param mote_id     id of the mote
 * @param data        must contain conf_name - the name of the configuration to activate
 * @param client_info info fo the client making the request
 */
async function change_conf (

    mote_id: number,
    data: object,
    client_info: Auth.ClientAuthInfo

): Promise<any> {

    if (data['conf_name'] === undefined)
        throw new QueryExecError(`field "conf_name" is necessary to change configurations`)

    let conf_name = data['conf_name']
    let conf
    let confs = await getRepository(Models.MoteConfiguration)
        .createQueryBuilder(Models.MoteConfiguration.table_name)
        .where(
            `${Models.MoteConfiguration.table_name}.name = :name and ` +
            `${Models.MoteConfiguration.table_name}.mote_id = :mid and ` +
            `${Models.MoteConfiguration.table_name}.group_id = :gid`,
            { name: conf_name, gid: client_info.group_id, mid: mote_id })
        .getMany()

    if (confs.length == 0)
        throw new QueryExecError(`unknown configuration "${conf_name}" for mote ${mote_id}`)

    conf = confs[0]
    if (conf.active)
        throw new QueryExecError(
            `configuration ${conf_name} is already active in mote ${mote_id}`
        )

    // the configurations to set
    data['measurement_period'] = conf.measurement_period
    data['send_data_period']   = conf.send_data_period
    data['report_period']      = conf.status_report_period
    data['comm_encrypt']       = conf.use_crypto ? 1 : 0

    await MoteComm.send(mote_id, data, Protocol.PacketType.CONF_SET)
    // conf.active = true
    // await conf.save()
}


/**
 * Change the state of a sensor
 *
 * @param mote_id     id of the mote
 * @param data        must contain sensor_id and num
 * @param client_info info fo the client making the request
 */
async function change_sensor_state (

    mote_id: number,
    data: object,
    operation: Protocol.PacketType,
    client_info: Auth.ClientAuthInfo

): Promise<any> {

    if (data['sensor_id'] === undefined)
        throw new QueryExecError('field "sensor_id" is necessary to change sensor state')
    else if (data['num'] === undefined)
        throw new QueryExecError('field "num" is necessary to change sensor state')

    console.log(`change_sensor_state data`, data)
    let sensor_id = data['sensor_id']
    let num       = data['num']

    let sensor_use
    let sensor_uses = await getRepository(Models.SensorUse)
        .createQueryBuilder(Models.SensorUse.table_name)
        .where(
            `${Models.SensorUse.table_name}.group_id = :gid and ` +
            `${Models.SensorUse.table_name}.sensor_id = :sid and ` +
            `${Models.SensorUse.table_name}.mote_id = :mid and ` +
            `${Models.SensorUse.table_name}.num = :num`,
            { gid: client_info.group_id, sid: sensor_id, mid: mote_id, num: num })
        .getMany()

    if (sensor_uses.length == 0)
        throw new QueryExecError(
            `there is no sensor ${sensor_id} currently associated with mote ${mote_id}`
        )

    sensor_use = sensor_uses[0]
    if (sensor_use.active && operation == Protocol.PacketType.SENSOR_ACTIVATE)
        throw new QueryExecError(`sensor ${sensor_id} is already active in mote ${mote_id}`)

    else if (!sensor_use.active && operation == Protocol.PacketType.SENSOR_DEACTIVATE)
        throw new QueryExecError(`sensor ${sensor_id} is not active in mote ${mote_id}`)

    // set the interface where the mote shoud change the sensor state
    data['interface'] = sensor_use.interface_num

    await MoteComm.send(mote_id, data, operation)

    // sensor_use.active = operation == Protocol.PacketType.SENSOR_ACTIVATE
    // await sensor_use.save()
}


/**
 * Change the state of a actuator
 *
 * @param mote_id     id of the mote
 * @param data        must contain actuator_id and num
 * @param client_info info fo the client making the request
 */
async function change_actuator_state (

    mote_id: number,
    data: object,
    operation: Protocol.PacketType,
    client_info: Auth.ClientAuthInfo

): Promise<any> {

    if (data['actuator_id'] === undefined)
        throw new QueryExecError(
            'field "actuator_id" is necessary to change actuator state'
        )
    else if (data['num'] === undefined)
        throw new QueryExecError('field "num" is necessary to change actuator state')

    let actuator_id = data['actuator_id']
    let num         = data['num']

    let actuator_use
    let actuator_uses = await getRepository(Models.ActuatorUse)
        .createQueryBuilder(Models.ActuatorUse.table_name)
        .where(
            `${Models.ActuatorUse.table_name}.group_id = :gid and ` +
            `${Models.ActuatorUse.table_name}.actuator_id = :aid and ` +
            `${Models.ActuatorUse.table_name}.mote_id = :mid and ` +
            `${Models.ActuatorUse.table_name}.num = :num`,
            { gid: client_info.group_id, aid: actuator_id, mid: mote_id, num: num })
        .getMany()

    if (actuator_uses.length == 0)
        throw new QueryExecError(
            `there is no actuator ${actuator_id} currently associated with mote ${mote_id}`
        )

    actuator_use = actuator_uses[0]
    if (actuator_use.active && operation == Protocol.PacketType.ACTUATOR_ACTIVATE)
        throw new QueryExecError(`actuator ${actuator_id} is already active in mote ${mote_id}`)

    else if (!actuator_use.active && operation == Protocol.PacketType.ACTUATOR_DEACTIVATE)
        throw new QueryExecError(`actuator ${actuator_id} is not active in mote ${mote_id}`)

    // set the interface where the mote shoud change the actuator state
    data['interface'] = actuator_use.interface_num

    await MoteComm.send(mote_id, data, operation)

    // actuator_use.active = operation == Protocol.PacketType.ACTUATOR_ACTIVATE
    // await actuator_use.save()
}


/**
 * Send an action instruction
 *
 * @param mote_id     id of the mote
 * @param data        must contain actuator_id, num and action number
 * @param client_info info fo the client making the request
 */
async function action_instruction (

    mote_id: number,
    data: object,
    client_info: Auth.ClientAuthInfo

): Promise<any> {

    if (data['actuator_id'] === undefined)
        throw new QueryExecError(
            'field "actuator_id" is necessary to send action instruction'
        )
    else if (data['actuator_use_num'] === undefined)
        throw new QueryExecError(
            'field "actuator_use_num" is necessary to send action instruction'
        )
    else if (data['action_num'] === undefined)
        throw new QueryExecError(
            'field "action_num" is necessary to send action instruction'
        )

    let actuator_id      = data['actuator_id']
    let actuator_use_num = data['actuator_use_num']
    let action_num       = data['action_num']

    let actuator_uses = await getRepository(Models.ActuatorUse)
        .createQueryBuilder(Models.ActuatorUse.table_name)
        .where(
            `${Models.ActuatorUse.table_name}.group_id = :gid and ` +
            `${Models.ActuatorUse.table_name}.actuator_id = :aid and ` +
            `${Models.ActuatorUse.table_name}.mote_id = :mid and ` +
            `${Models.ActuatorUse.table_name}.num = :num`,
            { gid: client_info.group_id, aid: actuator_id, mid: mote_id, num: action_num })
        .getMany()

    if (actuator_uses.length == 0)
        throw new QueryExecError(
            `there is no actuator ${actuator_id} currently associated with mote ${mote_id}`
        )

    // set the interface where the actuator to 'call' is
    data['interface'] = actuator_uses[0].interface_num
    data['action']    = action_num

    await MoteComm.send(mote_id, data, Protocol.PacketType.ACTION_INSTRUCTION)
}
