import { ExtensionPack, Client } from '../../db/models'
import * as Request   from 'request-promise'
import * as Mustache  from 'mustache'
import * as Auth      from '../auth'
import * as Fs        from 'fs-extra'
import * as _Git      from 'simple-git/promise'
import * as Dockerode from 'dockerode'

import { getConnection } from 'typeorm'

import { loggers } from '../../logger.js'
const logger = loggers.web
const CLIENT_DB_SETUP_TEMPLATE = './setup_client_db.sql'
const config = require('../../config.json')

const Git = _Git()

/**
 * Languages with supported extension packs and their locations
 */
const EXTENSION_LANGS = {
    python: 'extensions/python'
}

export class ExtPackError {
    constructor (public message: string) {}
}

/**
 * Tables that extension pack can access
 */
const TABLES = [
    "mote",
    "mote_error",
    "mote_configuration",
    "mote_connection",
    "sensor",
    "sensor_use",
    "parameter",
    "measurement",
    "actuator",
    "actuator_use",
    "action",
    "action_result",
    "alert",
    "alert_use",
    "alert_occurrence",
    "trigger",
    "trigger_use",
    "trigger_occurrence",
    "extension_pack_log"
]


/**
 * Send extension pack instruction to subscribe to events from
 * certain motes
 *
 * @param motes          data sent from the client
 * @param extension_pack information about the extension pack
 */
export async function subscribe_to_events
(motes: string[], extension_pack: ExtensionPack): Promise<void> {
    await do_sub_unsub(motes, extension_pack, true)
}


/**
 * Send extension pack instruction to unsubscribe to events from
 * certain motes
 *
 * @param motes          data sent from the client
 * @param extension_pack information about the extension pack
 */
export async function unsubscribe_to_events
(motes: string[], extension_pack: ExtensionPack): Promise<void> {
    await do_sub_unsub(motes, extension_pack, false)
}


async function do_sub_unsub
(motes: string[], extension_pack: ExtensionPack, sub: boolean = true) {

    if (!extension_pack.active)
        throw new ExtPackError(`Extension pack is not active`)

    let instruction = {
        operation: sub ? 'sub' : 'unsub',
        motes: motes
    }

    try {

        let response = await Request({
            uri    : `http://localhost:${extension_pack.container_address}/sub_unsub`,
            headers: { 'Content-Type': 'application/json' },
            method : 'POST',
            body   : JSON.stringify(instruction)
        })
        logger.info(`@extension_pack.sub_unsub received ${response}`)
        return response

    } catch (error) {
        if (error.statusCode === 500)
            throw new ExtPackError(
                'Execution of the request failed with unexpected error'
            )
        else if (error.statusCode === 400)
            throw new ExtPackError(
                'Extension pack reject the request. Check the request format'
            )

        logger.warn('@extension_pack.sub_unsub failed with error: ', error)
        console.log(error)
        throw new ExtPackError(
            'Failed to send request. Extension may be down at this moment'
        )
    }

}


export async function send_client_request
(request: object, extension_pack: ExtensionPack): Promise<any> {

    if (!extension_pack.active)
        throw new ExtPackError(`Extension pack is not active`)

    let data = ''
    if (request !== undefined) {
        try {
            data = JSON.stringify(request)
        } catch (exception) {
            throw new ExtPackError('Request must be in JSON format')
        }
    }

    try {

        let response = await Request({
            uri   : `http://localhost:${extension_pack.container_address}/client_request`,
            method: 'POST',
            body  : data
        })
        logger.info(`@extension_pack.client_request received response`)
        return response

    } catch (error) {
        if (error.statusCode === 500)
            throw new ExtPackError(
                'Execution of the request failed with unexpected error'
            )
        else if (error.statusCode === 400)
            throw new ExtPackError(
                'Exntesion pack reject the request. Check the request format'
            )

        logger.warn('@extension_pack.client_request failed with error: ', error)
        console.log(error)
        throw new ExtPackError(
            'Failed to send request. Extension may be down at this moment'
        )
    }
}


/**
 * Create an extension pack
 */
export async function create_extension_pack
(client_info: Auth.ClientAuthInfo, repository_url: string, language: string) {

    let client = await Client.findOne({ id: client_info.client_id })
    if (!client.admin)
        throw new ExtPackError(`Only admininstators can create the extension pack`)

    let existing = await ExtensionPack.find({ group_id: client_info.group_id })
    if (existing.length !== 0)
        throw new ExtPackError(`Extension pack already exists`)

    if (!EXTENSION_LANGS.hasOwnProperty(language.toLocaleLowerCase()))
        throw new ExtPackError(`Language ${language} is not currently supported`)

    // set extension pack's database settings
    // let db_setup_template = Fs.readFileSync(CLIENT_DB_SETUP_TEMPLATE, 'utf8')
    let template = Fs.readFileSync('./setup_client_db.sql', 'utf8')
    let template_data = {
        username: client.username,
        group_id: client.group_id,
        tables: TABLES,
        create: true
    }
    // sql to set clients permissions
    let query = Mustache.render(template, template_data)
    // location of common structure and libraries around the extension pack
    let structure_location = EXTENSION_LANGS[language.toLocaleLowerCase()]
    // copy the the extensions 'backbone' to its location
    let extension_location =
        config['extensions_base_dir'] + `/${template_data.username}_extension`


    let connection = getConnection()
    let txn = connection.createQueryRunner()
    txn.connect()

    // create client's database. these can't be in the transation
    await connection.query(`drop   database if exists ${template_data.username}_db`)
    await connection.query(`create database           ${template_data.username}_db`)

    let new_extension = new ExtensionPack()
    let docker = new Dockerode({ socketPath: config['docker_socket'] })

    // do the rest inside a transaction
    txn.startTransaction()
    try {
        await txn.query(query)

        Fs.emptyDirSync(extension_location) // make sure the directory is empty
        logger.verbose(`@extension_pack.create copying ${structure_location} ` +
            `extension structure to ${extension_location}...`)
        Fs.copySync(structure_location, extension_location)
        // get clients code into the extension's location
        try {
            logger.verbose(`@extension_pack.create cloning ${repository_url}`)
            await Git.clone(repository_url, extension_location + '/app')
        } catch (exception) {
            logger.warn('@extension_pack.create failed to get client\'s code \
            with error: ', exception)
            console.log(exception)
            // delete created directory
            Fs.removeSync(extension_location)
            throw new ExtPackError(`Failed to get code from ${repository_url}. \
            It must be a valid git repository`)
        }

        new_extension.active        = false
        new_extension.client_id     = client.id
        new_extension.group_id      = client.group_id
        new_extension.code_repo_url = repository_url
        new_extension.prog_language = language.toLocaleLowerCase()
        new_extension.container_id  = client.username + '_extension'
        await txn.manager.save(new_extension)
        new_extension.container_address = `${6000 + (new_extension.id % 10000)}`
        await txn.manager.save(new_extension)

        // build the extension's container
        let stream = await build_container(docker, extension_location, new_extension.container_id)
        let build_end_promise = new Promise((resolve, reject) => {
            let with_error = false
            stream.on('end', () => {
                logger.verbose('@extension_pack.create build finished')
                if (with_error) reject()
                else            resolve()
            })
            stream.on('error', (error) => {
                logger.verbose('@extension_pack.create build failed with error', error)
                console.log(error)
                reject()
            })
            stream.on('data', (data) => {
                try {
                    let msg = JSON.parse(data)
                    if (msg.hasOwnProperty('error')) {
                        logger.warn(`@extension_pack.create build failed with error: \
                        ${msg['error']}`)
                        with_error = true
                    } else if (msg.hasOwnProperty('errorDetail')) {
                        logger.warn(`@extension_pack.create build failed with error: \
                        ${msg['errorDetail']}`)
                        with_error = true
                    } else {
                        logger.verbose(`@extension_pack.create -->`)
                        console.log(msg)
                    }
                } catch (exception) {
                    logger.verbose(`@extension_pack.create (notjson) -> ${data}`)
                }
            })
        })

        await build_end_promise
        await txn.commitTransaction()

    } catch (exception) {
        txn.rollbackTransaction()
        try { Fs.removeSync(extension_location) } catch (e) {}
        logger.warn('@extension_pack.create failed to create extension with error: ', exception)
        console.log(exception)
        throw exception
    }

    try {
        // create and start it
        let container = await create_container(
            docker,
            new_extension.container_id,
            parseInt(new_extension.container_address),
            client.username
        )
        await container.start()
    } catch (exception) {
        logger.error('@extension_pack.create database info was commited but creation failed!!')
        throw exception
    }

    try {
        new_extension.active = true
        connection.manager.save(new_extension)
    } catch (exception) {
        logger.error('@extension_pack.create extension was created and \
        started but new state couldnt be saved to DB')
        throw new ExtPackError('Extension was created but could not be started')
    }

}


/**
 * Delete a client's extension pack
 */
export async function delete_extension_pack (client_info: Auth.ClientAuthInfo) {


    let client = await Client.findOne({ id: client_info.client_id })
    if (!client.admin)
        throw new ExtPackError(`Only admininstators can delete the extension pack`)

    let extension_pack = await ExtensionPack.findOne({ client_id: client_info.client_id })
    if (extension_pack === undefined)
        throw new ExtPackError(`Extension is not yet created`)

    // set extension pack's database settings
    let db_setup_template = Fs.readFileSync(CLIENT_DB_SETUP_TEMPLATE, 'utf8')
    let template = Fs.readFileSync('./setup_client_db.sql', 'utf8')
    let template_data = {
        username: client.username,
        group_id: client.group_id,
        tables  : TABLES,
        create  : false // commands to delete client and permissions
    }

    // to remove client and permissions
    let query = Mustache.render(template, template_data)
    // location of the extension
    let extension_location =
        config['extensions_base_dir'] + `/${template_data.username}_extension`


    let connection = getConnection()
    let txn        = connection.createQueryRunner()
    await txn.connect()

    let docker = new Dockerode({ socketPath: config['docker_socket'] })
    // stop and remove container
    logger.verbose(`@extension_pack.delete container ${extension_pack.container_id}...`)
    let container = await docker.getContainer(extension_pack.container_id)
    if (extension_pack.active) {
        logger.verbose(`@extension_pack.delete stopping first...`)
        await container.stop()
    }
    await container.remove()

    // drop the client's database
    await connection.query(`drop database if exists ${template_data.username}_db`)

    // do the rest inside a transaction
    txn.startTransaction()
    try {

        await txn.query(query)
        await txn.manager.remove(extension_pack)
        // delete the directory with the extension pack
        Fs.removeSync(extension_location)
        await txn.commitTransaction()

    } catch (exception) {
        txn.rollbackTransaction()
        logger.warn('@extension_pack.delete failed to delete extension with error: ', exception)
        console.log(exception)
        throw exception
    }

}


/**
 * XXX: Unstable, Unsafe
 */
export async function update_extension_pack
(client_info: Auth.ClientAuthInfo, language: string, repository_url: string) {

    let client = await Client.findOne({ id: client_info.client_id })
    if (!client.admin)
        throw new ExtPackError(`Only admininstators can delete the extension pack`)

    let extension_pack = await ExtensionPack.findOne({ client_id: client_info.client_id })
    if (extension_pack === undefined)
        throw new ExtPackError(`Extension is not yet created`)

    if (language !== undefined && !EXTENSION_LANGS.hasOwnProperty(language.toLocaleLowerCase()))
        throw new ExtPackError(`Language ${language} is not currently supported`)

    if (language !== undefined)
        extension_pack.prog_language = language.toLocaleLowerCase()
    if (repository_url !== undefined)
        extension_pack.code_repo_url = repository_url

    let structure_location = EXTENSION_LANGS[extension_pack.prog_language.toLocaleLowerCase()]
    let extension_location = config['extensions_base_dir'] + `/${client.username}_extension`

    Fs.emptyDirSync(extension_location) // make sure the directory is empty
    Fs.copySync(structure_location, extension_location)
    // get clients code into the extension's location
    try {
        await Git.clone(extension_pack.code_repo_url, extension_location + '/app')
    } catch (exception) {
        logger.warn('@extension_pack.update failed to get client\'s code with error: ', exception)
        console.log(exception)
        // delete created directory
        Fs.removeSync(extension_location)
        throw new ExtPackError(`Failed to get code from ${repository_url}. \
        It must be a valid git repository`)
    }

    let docker = new Dockerode({ socketPath: config['docker_socket'] })
    // stop and remove container
    logger.verbose(`@extension_pack.update delete container ${extension_pack.container_id}...`)
    let container = await docker.getContainer(extension_pack.container_id)
    if (extension_pack.active) {
        logger.verbose(`@extension_pack.update stopping first...`)
        await container.stop()
    }


    // build the extension's container
    let stream = await build_container(
        docker, extension_location, extension_pack.container_id
    )
    let build_end_promise = new Promise((resolve, reject) => {
        let with_error = false
        stream.on('end', () => {
            logger.verbose('@extension_pack.update build finished')
            if (with_error) reject()
            else resolve()
        })
        stream.on('error', (error) => {
            logger.verbose('@extension_pack.update build failed with error', error)
            console.log(error)
            reject()
        })
        stream.on('data', (data) => {
            try {
                let msg = JSON.parse(data)
                if (msg.hasOwnProperty('error')) {
                    logger.warn(`@extension_pack.update build \
                    failed with error: ${msg['error']}`)
                    with_error = true
                } else if (msg.hasOwnProperty('errorDetail')) {
                    logger.warn(`@extension_pack.update build \
                    failed with error: ${msg['errorDetail']}`)
                    with_error = true
                } else {
                    logger.verbose(`@extension_pack.update --> ${msg['stream']}`)
                }
            } catch (exception) {
                logger.verbose(`@extension_pack.update (notjson) -> ${data}`)
            }
        })
    })

    await build_end_promise

    // remove the current container
    await container.remove()

    let connection = getConnection()
    extension_pack.active = false
    await connection.manager.save(extension_pack)

    // create and start it
    let new_container = await create_container(
        docker,
        extension_pack.container_id,
        parseInt(extension_pack.container_address),
        client.username
    )
    await new_container.start()

    try {
        extension_pack.active = true
        await connection.manager.save(extension_pack)
    } catch (exception) {
        logger.error('@extension_pack.create extension was created \
        and started but new state couldnt be saved to DB')
        throw new ExtPackError('Extension was created but could not be started.')
    }
}


/**
 * Start a paused/stopped extension
 *
 * @param client_info info of the owner of the extension
 */
export async function start_extension_pack (client_info: Auth.ClientAuthInfo) {

    let client = await Client.findOne({ id: client_info.client_id })
    if (!client.admin)
        throw new ExtPackError(`Only admininstators can start the extension pack`)

    let extension_pack = await ExtensionPack.findOne({ client_id: client_info.client_id })
    if (extension_pack === undefined)
        throw new ExtPackError(`Extension is not yet created`)
    if (extension_pack.active === true)
        throw new ExtPackError(`Extension is already started`)

    let connection = getConnection()
    let docker = new Dockerode({ socketPath: config['docker_socket'] })
    await start_container(docker, extension_pack.container_id)

    extension_pack.active = true
    await connection.manager.save(extension_pack)
}


/**
 * Stop extension pack without removing/destroying it
 *
 * @param client_info info of the owner of the extension
 */
export async function stop_extension_pack (client_info: Auth.ClientAuthInfo) {

    let client = await Client.findOne({ id: client_info.client_id })
    if (!client.admin)
        throw new ExtPackError(`Only admininstators can stop the extension pack`)

    let extension_pack = await ExtensionPack.findOne({ client_id: client_info.client_id })
    if (extension_pack === undefined)
        throw new ExtPackError(`Extension is not yet created`)
    if (extension_pack.active === false)
        throw new ExtPackError(`Extension is already stoped`)

    let connection = getConnection()
    let docker = new Dockerode({ socketPath: config['docker_socket'] })
    logger.verbose(`@extension_pack.stop container ${extension_pack.container_id}...`)
    let container = await docker.getContainer(extension_pack.container_id)
    await container.stop()

    extension_pack.active = false
    await connection.manager.save(extension_pack)
}


/**
 * Build a docker container
 *
 * @param docker   connection to docker
 * @param location location of container's files
 * @param name     name to assign to the container
 */
async function build_container
(docker: Dockerode.Docker, location: string, name: string): Promise<NodeJS.ReadStream> {
    logger.verbose(`@extension_pack.build_container source at ${location} and name ${name}...`)
    let response = await docker.buildImage({ context: location }, { t: name })
    return response
}


/**
 * Creates and starts a container for an extension pack
 *
 * @param docker   connection to docker
 * @param name     name of the container
 * @param port     port to listen to requests
 * @param username username of the container owner
 *
 * @return the created container
 */
async function create_container
(docker: Dockerode.Docker, name: string, port: number, username: string): Promise<any> {

    // enviroment variables
    let environ = [
        `DB_URL=postgresql+psycopg2://${username}@ouricoservices/ourico_test`,
        `REDIS_HOST=ouricoservices`,
        `APP_PORT=${port}`,
        `CLIENT_DB_URL=postgresql+psycopg2://${username}@ouricoservices/${username}_db`,
    ]
    // configuration to connet to services outside the container
    let port_bindings  = { "80/tcp": [{ "HostPort": `${port}` }] }
    let network_mode   = config['docker_containers_net_mode']
    let extra_host     = [`ouricoservices:${config['services_addr']}` ]
    let options        = {
        "Image"       : name,
        "PortBindings": port_bindings,
        "Env"         : environ,
        "ExtraHosts"  : extra_host,
        "NetworkMode" : network_mode,
        "Tty"         : false,
        name          : name
    }
    logger.verbose(`@extension_pack.start_container ${name} with options`)
    console.log(options)
    return await docker.createContainer(options)
}


/**
 * Start an extension pack container
 *
 * @param docker connection to docker
 * @param name   name of the container
 */
async function start_container (docker: Dockerode.Docker, name: string) {
    logger.verbose(`@extension_pack.start container ${name}...`)
    let container = await docker.getContainer(name)
    await container.start()
}
