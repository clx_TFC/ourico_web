/**
 * Client account and session management operations
 */


import * as Auth   from '../auth'
import * as Bcrypt from 'bcrypt'

import { ApiClient, Client } from '../../db/models'
import { getRepository }     from 'typeorm'

import { loggers } from '../../logger'

const logger = loggers.web
const config = require('../../config.json')

export class ControlOperationError {
    constructor (public message: string) { }
}



/**
 * Authenticates client. If successful returns a new session token
 *
 * @param username
 * @param password
 *
 * @returns new session token as string
 * @throws  ControlOperationError
 */
export async function login (username: string, password: string): Promise<string> {

    // validate username
    let clients = await getRepository(Client).find({ username: username })
    if (clients.length == 0)
        throw new ControlOperationError(`username`)

    let client = clients[0]
    console.log(client)
    // validate password
    let match = await Bcrypt.compare(password, client.password)
    if (!match)
        throw new ControlOperationError(`password`)

    let capabilities: Auth.Capabilities[] = []
    if (client.admin)
        capabilities.push(Auth.Capabilities.All)
    else {
        capabilities.push(Auth.Capabilities.Read) // default
        if (client.write_capability)
            capabilities.push(Auth.Capabilities.Write)
        if (client.rt_events_capability)
            capabilities.push(Auth.Capabilities.RtEvents)
        if (client.compute_capability)
            capabilities.push(Auth.Capabilities.Compute)
    }
    let jwt_payload = new Auth.ClientAuthInfo(
        client.id,
        Auth.ClientType.User,
        client.group_id,
        null,
        capabilities
    )

    return await Auth.generate_token(jwt_payload)
}


export async function logout (token: string) {
    return await Auth.invalidate_token(token)
}


/**
 * Create new administrator and generate a new token
 *
 * @param client_data data for the new admin
 *
 * @return return the new token
 * @throws ControlOperationError
 */
export async function new_admin (client_data: object): Promise<string> {

    if (client_data['name'] === undefined)
        throw new ControlOperationError(`client "name" was not specified`)
    else if (client_data['username'] === undefined)
        throw new ControlOperationError(`client "username" was not specified`)
    else if (client_data['email'] === undefined)
        throw new ControlOperationError(`client "email" was not specified`)
    else if (client_data['password'] === undefined)
        throw new ControlOperationError(`client "password" was not specified`)

    try {
        Auth.validate_password(client_data['password'])
    } catch (e) {
        throw new ControlOperationError(e)
    }

    // check that username is unique
    let usernames = await getRepository(Client)
        .find({ username: client_data['username'] })
    if (usernames.length != 0)
        throw new ControlOperationError(
            `username not available`
        )

    // check that email is unique
    let emails = await getRepository(Client).find({ email: client_data['email'] })
    if (emails.length != 0)
        throw new ControlOperationError(
            `email not available`
        )

    let client = new Client()
    client.id        = await Auth.rand_string(20)
    client.name      = client_data['name']
    client.username  = client_data['username']
    client.email     = client_data['email']
    client.password  =
        // save hashed password
        await Bcrypt.hash(client_data['password'], config.bcrypt_salt_rounds)
    client.admin     = true
    client.client_id = client.id // owns itself
    client.write_capability = // admin has hall the capabilities
        client.compute_capability = client.rt_events_capability = true

    await client.save()

    let new_token
    try {
        let jwt_payload = new Auth.ClientAuthInfo(
            client.id,
            Auth.ClientType.User,
            client.group_id,
            null,
            [Auth.Capabilities.All]
        )
        new_token = await Auth.generate_token(jwt_payload)
        client.access_token = new_token
        client.save() // save token
    } catch (exception) {
        logger.warn('@control client was created but token creation failed')
        logger.warn(`@control the error: ${exception.message}`)
        logger.warn('@control deleting created client...')
        await client.remove()
        logger.warn('@control client deleted')
        throw exception
    }

    return new_token
}


/**
 * Updates information about a client and generates a new token
 *
 * @param admin       information of the admin making the request
 * @param token       the old/current token
 * @param client_data should contain client id, client type and the new data
 *
 * @return a new token
 * @throws ControlOperationError
 */
export async function update_client_info (

    admin: Auth.ClientAuthInfo,
    token: string,
    client_data: object,

): Promise<string> {

    if (client_data['type'] === undefined)
        throw new ControlOperationError(`client "type" was not indicated`)
    else if (client_data['id'] === undefined)
        throw new ControlOperationError(`client "id" was not indicated`)
    else if (client_data['new_data'] === undefined)
        throw new ControlOperationError(`client "new_data" object was not indicated`)

    let client
    if (client_data['type'] === Auth.ClientType.User) {
        for (var attr in client_data['new_data']) {
            // check if fields are updatable
            if (!Client.updatable_fields.find(f => f == attr))
                throw new ControlOperationError(
                    `field "${attr}" is not an updatable field of a client`
                )
        }
        client = await getRepository(Client).findOne({
            id       : client_data['id'],
            client_id: admin.client_id
        })
    }
    else if (client_data['type'] === Auth.ClientType.ApiClient) {
        for (var attr in client_data['new_data']) {
            // check if fields are updatable
            if (!ApiClient.updatable_fields.find(f => f == attr))
                throw new ControlOperationError(
                    `field "${attr}" is not an updatable field of an api client`
                )
        }
        client = await getRepository(ApiClient).findOne({
            id       : client_data['id'],
            client_id: admin.client_id
        })
    }
    else
        throw new ControlOperationError(`unknown client type ${client_data['type']}`)


    // make sure admin status is not changed
    delete client_data['new_data']['admin']
    // set new fields
    for (var attr in client_data['new_data']) {
        if (attr === 'password') {
            try {
                Auth.validate_password(client_data['password'])
            } catch (e) {
                throw new ControlOperationError(e)
            }
            // save hash password
            client[attr] = await Bcrypt.hash(client_data['new_data'][attr], config.bcrypt_salt_rounds)

        } else {
            client[attr] = client_data['new_data'][attr]
        }
    }

    let capabilities: Auth.Capabilities[] = []
    if (client_data['type'] === Auth.ClientType.User && client.admin)
        capabilities.push(Auth.Capabilities.All)
    else {
        capabilities.push(Auth.Capabilities.Read) // default
        if (client.write_capability)
            capabilities.push(Auth.Capabilities.Write)
        if (client.rt_events_capability)
            capabilities.push(Auth.Capabilities.RtEvents)
        if (client.compute_capability)
            capabilities.push(Auth.Capabilities.Compute)
    }

    let jwt_payload = new Auth.ClientAuthInfo(
        client.id,
        client_data['type'],
        admin.group_id,
        null,
        capabilities
    )
    console.log('client_data ', client_data)
    console.log('the client to update', client)
    await Auth.invalidate_token(token, false)
    let new_token = await Auth.generate_token(jwt_payload)
    client.access_token = new_token


    await client.save()
    return new_token
}


/**
 * Delete a client and invalidate his token
 *
 * @param admin       information of the admin making the request
 * @param token       the current token
 * @param client_data information of the client to delete
 *
 * @throws ControlOperationError
 */
export async function delete_client
(admin: Auth.ClientAuthInfo, token: string, client_data: object) {

    if (client_data['type'] === undefined)
        throw new ControlOperationError(`client "type" was not indicated`)
    else if (client_data['id'] === undefined)
        throw new ControlOperationError(`client "id" was not indicated`)

    // await Auth.invalidate_token(token, false)

    if (client_data['type'] === Auth.ClientType.User) {
        await getRepository(Client).delete({
            id       : client_data['id'],
            client_id: admin.client_id
        })
    }
    else if (client_data['type'] === Auth.ClientType.ApiClient) {
        await getRepository(ApiClient).delete({
            id       : client_data['id'],
            client_id: admin.client_id
        })
    }
    else
        throw new ControlOperationError(`unknown client type ${client_data['type']}`)

}


/**
 * Invalidates the current token and creates a new one for the client
 *
 * @param token       current token
 * @param client_data data of the client whose token will be updated
 *
 * @returns the new token
 * @throws  ControlOperationError
 */
export async function update_token
(token: string, client_data: Auth.ClientAuthInfo): Promise<string> {

    logger.debug(`@control.update_token updating `)
    console.log(client_data)

    let client
    if (client_data.client_type === Auth.ClientType.User)
        client = await getRepository(Client).findOne({ id: client_data.client_id })
    else if (client_data.client_type === Auth.ClientType.ApiClient)
        client = await getRepository(ApiClient).findOne({ id: client_data.client_id })
    else {
        logger.warn(`@control.update_token unknown client type ${client_data.client_type}`)
        throw new ControlOperationError(`unknown client type`)
    }

    let capabilities: Auth.Capabilities[] = []
    if (client_data.client_type === Auth.ClientType.User && client.admin)
        capabilities.push(Auth.Capabilities.All)
    else {
        capabilities.push(Auth.Capabilities.Read) // default
        if (client.write_capability)
            capabilities.push(Auth.Capabilities.Write)
        if (client.rt_events_capability)
            capabilities.push(Auth.Capabilities.RtEvents)
        if (client.compute_capability)
            capabilities.push(Auth.Capabilities.Compute)
    }

    let jwt_payload = new Auth.ClientAuthInfo(
        client.id,
        client_data.client_type,
        client.group_id,
        null,
        capabilities
    )

    await Auth.invalidate_token(token, false)
    return await Auth.generate_token(jwt_payload)
}


export async function revoke_token (token: string) {
    await Auth.invalidate_token(token)
}
