import * as Models        from '../../db/models'
// import '../../db/mote'
import * as Auth          from '../auth'
import { loggers }        from '../../logger'
import { QueryExecError } from './resources'
import {
    Comparison,
    Filter,
    Option,
    Query
} from './query'

const logger = loggers.web


/**
 * Resources that this module can handle
 * Each key (name of the table for the resource) matchs to a model used to
 * execute database operations
 * NOTE: data resources are defined through the decorator DataResource
 *       direcly in the model definition in db/models
 */
const resources = { }



/**
 * Decorator to declare a class as a data resource
 *
 * @param resource_name name to identify the resource in queries
 * @param hidden_fields model used to execute database operations
 */
export function DataResource
(resource_name: string, model: { new (): Models.Model }) {
    let dec = (constructor: Function) => {
        if (resources.hasOwnProperty(resource_name))
            throw new Error(`duplicate resource name ${resource_name}`)
        resources[resource_name] = model
        logger.verbose(`@data_resources registered resource ${resource_name}`)
    }

    return dec
}



/**
 * Check if this module can handle a certain resource
 *
 * @param resource_name
 */
export function has (resource_name: string): boolean {
    return resources.hasOwnProperty(resource_name)
}


/**
 * Execute a query
 *
 * @param query
 * @param user_info info of the user execuuting the query
 */
export async function execute_query
(query: Query, user_info: Auth.ClientAuthInfo): Promise<object[]> {
    let data_resource = resources[query.resource]
    if (data_resource === undefined)
        throw new QueryExecError(`resource '${query.resource}' does not exist`)

    logger.verbose(`@data_resources executing READ on resource ${query.resource}`)
    let dt = new data_resource()
    return await dt.read(query, user_info)
}


/**
 * Execute multiple queries
 *
 * @param queries
 * @param user_info info of the user execuuting the query
 * 
 * @return object[]
 */
export async function execute_queries
(queries: Query[], user_info: Auth.ClientAuthInfo): Promise<object[]> {
    let results: object[] = []
    for (var query of queries) {
        results.push(await execute_query(query, user_info))
    }

    return results
}


/**
 * Write data to database
 *
 * @param resource  name of resource(model) where to write
 * @param data
 * @param user_info info of the user executing the write
 *
 * @returns ids of new items
 */
export async function execute_write
(resource: string, data: object[], user_info: Auth.ClientAuthInfo): Promise<any[]> {
    let data_resource = resources[resource]
    if (data_resource === undefined)
        throw new QueryExecError(`resource '${resource}' does not exist`)

    logger.verbose(`@data_resources executing WRITE on resource ${resource}`)
    let dt = new data_resource()
    return await dt.write(data, user_info)
}


/**
 * Update data in database
 *
 * @param resource   name of resource(model) to update
 * @param new_data
 * @param where
 * @param client_info info of the user executing the write
 *
 * @returns number of updated items
 */
export async function execute_update (

    resource: string,
    new_data: object[],
    where: Filter[],
    client_info: Auth.ClientAuthInfo

): Promise<number> {
    let data_resource = resources[resource]
    if (data_resource === undefined)
        throw new QueryExecError(`resource '${resource}' does not exist`)

    logger.verbose(`@data_resources executing UPDATE on resource ${resource}`)
    let dt = new data_resource()
    return await dt.update(where, new_data, client_info)
}


/**
 * Delete data from database
 *
 * @param resource    name of resource (model) where to delete
 * @param where
 * @param client_info info of the user executing the write
 *
 * TODO: @return number of deleted items
 */
export async function execute_delete
(resource: string, where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {
    let data_resource = resources[resource]
    if (data_resource === undefined)
        throw new QueryExecError(`resource '${resource}' does not exist`)

    logger.verbose(`@data_resources executing DELETE on resource ${resource}`)
    let dt = new data_resource()
    await dt.delete(where, client_info)
}
