import { Query, Filter }          from './query'
import * as Auth          from '../auth'
import * as Fs            from 'fs'
import { loggers }        from '../../logger.js'
import * as DataResources from './data_resources'

const config = require('../../config.json')
const logger = loggers.web


export class QueryExecError {
    constructor(public message: string) { }
}

// // to validate tokens
// let rsa_pub_key : Buffer = undefined
//
//
// /**
//  * @throws ValidationError if token not valid
//  * @throws Error if keys not found
//  */
// async function get_auth_info
// (token: string, capabilites: Auth.Capabilities[]): Promise<Auth.ClientAuthInfo> {
//     if (rsa_pub_key === undefined)
//         throw new Error('resources module was not initialized. key not found')
//
//     let auth_info = await Auth.validate_token(token, capabilites, rsa_pub_key)
//     if (auth_info instanceof Auth.ValidationError) {
//         throw auth_info
//     }
//
//     return auth_info
// }
//
//
// /**
//  * Initialize module
//  * Reads the crypto keys
//  */
// export function setup () {
//     rsa_pub_key  = Fs.readFileSync(config["rsa_2048_pub_key"])
// }
//
//
// /**
//  * Executes a single query
//  * User capabilites are checked before
//  *
//  * @param query
//  * @param token authentication token of the client making the request
//  *
//  * @return query execution result as an object
//  * @throws if keys are not defined | QueryExecError
//  */
// export async function execute_query
// (query: Query, token: string): Promise<object[]> {
//     let auth_info = get_auth_info(token, [Auth.Capabilities.Read])
//     if (DataResources.has(query.resource))
//         return await DataResources.execute_query(query, auth_info)
//     else
//         throw new QueryExecError(`resource '${query.resource}' does not exist`)
// }
//
//
// /**
//  * Executes a single query
//  * User capabilites are checked before the query is executed
//  *
//  * @param query
//  * @param auth_info authentication information of the client making the request
//  *
//  * @return query execution result as an object
//  */
// export async function execute_query_with_auth
// (query: Query, auth_info: Auth.ClientAuthInfo): Promise<object[]> {
//     if (DataResources.has(query.resource))
//         return await DataResources.execute_query(query, auth_info)
//     else
//         throw new QueryExecError(`resource '${query.resource}' does not exist`)
// }
//
//
// /**
//  * Same as execute_query, but a list of queries are executed
//  *
//  * @param queries list of queries to execute
//  * @param token authentication token of the client making the request
//  *
//  * @return query execution result as an object
//  * @throws if keys are not defined | ValidationError | QueryExecError
//  */
// export async function execute_queries
// (queries: Query[], token: string): Promise<object[]> {
//
//     let auth_info = get_auth_info(token, [Auth.Capabilities.Read])
//
//     let result: object[] = []
//     for (var query of queries) {
//         let query_res = await execute_query_with_auth(query, auth_info)
//         if (query_res instanceof QueryExecError)
//             return query_res
//         result.push(query_res)
//     }
//
//     return result
// }
//
//
//
// /**
//  * Write data to resource. Only applies to data resources
//  *
//  * @param resource  resource where to write data
//  * @param data
//  * @param user_info info of the user executing the write
//  *
//  * @returns ids of new items
//  */
// export async function execute_write
// (resource: string, data: object[], token: string): Promise<any[]> {
//     let client_info = get_auth_info(token, [Auth.Capabilities.Write])
//     return await DataResources.execute_write(resource, data, client_info)
// }
//
//
//
// /**
//  * Update data in resource. Only applies to data resources
//  *
//  * @param resource   name of resource to update
//  * @param new_data
//  * @param where
//  * @param token      client authentication token
//  *
//  * @returns number of updated items
//  */
// export async function execute_update
// (resource: string, new_data: object[], where: Filter[], token: string): Promise<number> {
//     let client_info = get_auth_info(token, [Auth.Capabilities.Write])
//     return await DataResources.execute_update(resource, new_data, where, client_info)
// }
//
//
// /**
//  * Delete data from resource. Only applies to data resources
//  *
//  * @param where
//  * @param token  client authentication token
//  *
//  */
// export async function execute_delete
// (resource: string, where: Filter[], token: string): Promise<void> {
//     let client_info = get_auth_info(token, [Auth.Capabilities.Write])
//     await DataResources.execute_delete(resource, where, client_info)
// }
