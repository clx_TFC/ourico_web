/**
 * Forward realtime mote events to clients.
 *
 * Events are published through redis from another server.
 * When an event from a certain mote occurrs (the other server publishes)
 * we send the event, through a websocket, to every client that's
 * interested in that mote (and have authorization).
 *
 * Mote events are sent through channels mote_data:x and mote_error:x, where
 * x is the mote id.
 *
 * Client subscribiptions are stored, per mote, in sets mote_sub:x client_a client_b ...,
 * where x is the mote id.
 */


import * as Websocket from 'ws'
import * as Redis     from 'redis'
import * as Auth      from './auth'
import * as Bluebird  from 'bluebird'
import { loggers }    from '../logger'

import * as Models       from '../db/models'
import { getRepository } from 'typeorm'

const config = require('../config.json')

Bluebird.promisifyAll(Redis.RedisClient.prototype)
Bluebird.promisifyAll(Redis.Multi.prototype)

const logger = loggers.web
let   nr_connections  = 0
let   max_connections = 100

// map of client_id => websocket connection
let connections: object = {}

let redis_client = undefined


export function ws_server_start () {
    // client used to store user info
    redis_client = Redis.createClient()

    // subscribe to mote event channels
    let redis_sub_client = Redis.createClient()
    redis_sub_client.psubscribe('mote_data:?')
    redis_sub_client.psubscribe('mote_error:?')
    redis_client.on('psubscribe', (pt, n) => {
        logger.info(`@rte_resources psubscribed to ${pt}`)
    })
    redis_sub_client.on('pmessage', mote_event)

    const server = new Websocket.Server({
        port: config.web_socket_port,
        verifyClient: verify_connection
    })
    server.on('connection', handle_connection)
    logger.info(
        `@rte_resources websocket server started in port ${config.web_socket_port}`
    )
}


/**
 * To authenticate client making the connection
 *
 * @param info information about the connection
 * @param next callback to call after done with the validation
 */
async function verify_connection (info, next) {
    try {
        // Bearer tokens are expected in the Authorization header
        // but browsers do not support setting headers in the current
        // websockets APIs so we also accept them in the query parameter
        // access_token, as defined in §2.3 in RFC6750
        let token: string = await Auth.extract_token(
            info.req.headers['authorization'], info.req.url.substr(1)
        )

        let client_info = await Auth.validate_token(token, [Auth.Capabilities.RtEvents])

        if (client_info instanceof Auth.ValidationError) throw client_info
        info.req.__client_info_ = client_info
        info.req.__conn_id_     = (nr_connections++) % max_connections
        next(true, 200, '')
    } catch (e) {
        if (e instanceof Auth.ValidationError) {
            logger.warn(`@rte_resources failed to validate client: ${e.to_string()}`)
            next(false, e.error_code, e.to_string())
        }
        else {
            logger.warn(`@rte_resources failed to validate client with unknown error: ${e}`)
            next(false, 500, 'failed to handle request')
        }
    }
}


/**
 * Called when connection has been successfully established
 *
 * @param new_ws websocket connection created
 * @param req    data contained in the HTTP GET request made by the client
 */
function handle_connection (new_ws, req) {
    // const ip = req.headers['x-forwarded-for'].split(/\s*,\s*/)[0]
    logger.info(`@rte_resources received connection`)
    // store connection by id to use later to send notifications
    // from mote_event callback
    connections[`${req.__client_info_.client_id}.${req.__conn_id_}`] = new_ws
    new_ws.__client_info_ = req.__client_info_
    new_ws.__conn_id_     = req.__conn_id_

    new_ws.on('message', handle_client_msg)
    new_ws.on('close', handle_client_close)
}


/**
 * Called when a client sends a message.
 * Clients send messages to subscribe or unsubscribe from
 * notifications on particular motes.
 * TODO: add client info to log msgs
 */
async function handle_client_msg (msg) {

    let sub
    let unsub

    logger.debug(`@rte_resources.msg received ${msg}`)

    try {
        let _msg = JSON.parse(msg)

        if (_msg.sub != undefined) {
            if (typeof _msg.sub === 'number')
                sub = [_msg.sub]
            else if (_msg.sub instanceof Array)
                sub = _msg.sub.split(',').map(id => +id)
            else
                sub = _msg.sub
        } else {
            sub = []
        }
        if (_msg.unsub != undefined) {
            if (typeof _msg.unsub === 'number')
                unsub = [_msg.unsub]
            else if (_msg.unsub instanceof Array)
                unsub = _msg.unsub.split(',').map(id => +id)
            else
                unsub = _msg.unsub
        } else {
            unsub = []
        }
    } catch (e) {
        logger.verbose(`@rte_resources.msg failed to parse ${msg}`)
        logger.verbose(`@rte_resources.msg the error ${e}`)
        let res = { status: 'err', msg: 'message does not have valid format' }
        this.send(JSON.stringify(res))
        return
    }

    if (sub === 'all') {
        // subscribe to all of the clients' motes
        let motes = await Models.Mote.find({
            group_id: this.__client_info_.group_id
        })
        sub = motes.map(m => m.id)

        if (unsub === 'all') unsub = sub

    } else {

        try {
            // check if client has access to motes that he is trying to subscribe to

            let ids = await getRepository(Models.Mote)
                .createQueryBuilder(Models.Mote.table_name)
                .select(`${Models.Mote.table_name}.id`)
                .where(
                    `${Models.Mote.table_name}.group_id = :gid`,
                    { gid: this.__client_info_.group_id })
                .andWhere(`${Models.Mote.table_name}.id in (:...ids)`, { ids: sub })
                .getMany()

            console.log(ids)

            for (var id of sub) {
                if (!ids.find(i => i.id == id)) {
                    logger.verbose(`@rte_resources.msg client tried to access mote he did not own`)
                    // found unknown or not owned mote id
                    let res = { status: 'err', msg: `no mote with id ${id} was found` }
                    this.send(JSON.stringify(res))
                    return
                }
            }

        } catch (e) {
            logger.verbose(`@rte_resources.msg failed to get mote ids from db: ${e}`)
            let res = { status: 'err', msg: `error processing request` }
            this.send(JSON.stringify(res))
            return
        }
    }

    // if (unsub === 'all') {
    // }

    try {
        for (var mote_id of sub) {
            logger.debug(`@rte_resources.msg adding client \
                ${this.__client_info_.client_id}.${this.__conn_id_} \
                to events of mote ${mote_id}`)
            // add client to receivers of events from mote identified
            // by mote_id
            await redis_client.saddAsync(
                `mote_sub:${mote_id}`,
                `${this.__client_info_.client_id}.${this.__conn_id_}`
            )
            // add this mote_id to the set of 'subscribed to' motes of this client
            await redis_client.saddAsync(
                `client_sub:${this.__client_info_.client_id}.${this.__conn_id_}`,
                mote_id
            )
        }
        for (var mote_id of unsub) {
            logger.debug(`@rte_resources.msg removing client \
                ${this.__client_info_.client_id}.${this.__conn_id_} \
                from events of mote ${mote_id}`)
            // remove client from receivers of events from mote identified
            // by mote_id
            await redis_client.sremAsync(
                `mote_sub:${mote_id}`,
                `${this.__client_info_.client_id}.${this.__conn_id_}`
            )
            // remove this mote_id from the set of 'subscribed to' motes of this client
            await redis_client.sremAsync(
                `client_sub:${this.__client_info_.client_id}.${this.__conn_id_}`,
                mote_id
            )
        }
        let res = { status: 'ok' }
        this.send(JSON.stringify(res))
    } catch (e) {
        logger.verbose(`@rte_resources.msg failed to sub/unsub client to events: ${e}`)
        let res = { status: 'err', msg: `failed to sub/unsub` }
        this.send(JSON.stringify(res))
        return
    }

}


/**
 * Called when client closes the connection
 *
 * @param code   close code
 * @param reason
 */
async function handle_client_close (code, reason) {
    logger.verbose(`@rte_resources.close_conn code ${code} and reason "${reason}"`)

    try {

        let subscribed_to_motes = await
            // list of motes that this client is subscribed to
            redis_client.smembersAsync(
                `client_sub:${this.__client_info_.client_id}.${this.__conn_id_}`
            )

        // remove all of this client's subscribiptions
        for (var mote_id of subscribed_to_motes) {
            logger.debug(`@rte_resources.close_conn removing client \
                ${this.__client_info_.client_id}.${this.__conn_id_} \
                from events of mote ${mote_id}`)
            await redis_client.sremAsync(
                `mote_sub:${mote_id}`,
                `${this.__client_info_.client_id}.${this.__conn_id_}`
            )
        }

        await redis_client.delAsync(
            `client_sub:${this.__client_info_.client_id}.${this.__conn_id_}`
        )

    } catch (e) {
        logger.warn(`@rte_resources.close_conn failed to remove subscriptions`)
        logger.warn(`@rte_resources.close_conn the error: ${e}`)
    }
}


/**
 * Called when a mote event is published in a redis channel
 *
 * @param pattern the pattern used in the subscription
 * @param channel the actual channel name mote_[event|error]:<mote_id>
 * @param message
 */
async function mote_event (pattern, channel, message) {
    let mote_id
    let client_ids

    logger.verbose(`@rte_resources receive event from ${channel}`)

    try {
        let parts = channel.split(':')
        mote_id = parts[1]
        client_ids = await redis_client.smembersAsync(`mote_sub:${mote_id}`)
    } catch (e) {
        logger.warn(`@rte_resources error processing mote event`)
        logger.warn(`@rte_resources channel=${message}, message=${message}`)
        logger.warn(`@rte_resources the error: ${e}`)
        return
    }

    let nr_clients = client_ids.length
    let nr_sent    = 0
    for (var client of client_ids) {
        // catch exception in each send so that failing to send to one client
        // does not affect others
        try {

            connections[client].send(message)
            nr_sent++

        } catch (e) {
            logger.warn(`@rte_resources failed to send event to client ${client}`)
            logger.warn(`@rte_resources the error: ${e}`)
        }
    }

    logger.verbose(
        `@rte_resources sent event from ${channel} to ${nr_sent}` +
        ` out of ${nr_clients} subscribed client(s)`
    )
}
