import * as Auth from '../web/auth'

export async function rand_token (): Promise<[string, Auth.ClientAuthInfo]> {
    let client_id    = 'ZMLB0RXX7oLcQJpax8Q4zpJp8X8i6eNwB2kH6VAxqKzQfe9qYSDacuyCTYYZ'
    let clien_type   = 1
    let now_offset   = -500 + (Math.floor(Math.random()*1000))
    let expiration   = Date.now() + now_offset
    let capabilities = [Auth.Capabilities.All]
    let auth_info    =
        new Auth.ClientAuthInfo(client_id, clien_type, 1, expiration, capabilities)
    return [await Auth.generate_token(auth_info), auth_info]
}
