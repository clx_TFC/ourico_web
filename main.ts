import 'reflect-metadata';

import { setup as loggers_setup, loggers } from './logger.js'

loggers_setup() // always first to be initialized


import { start as server_start    } from './web/server'
import { ws_server_start          } from './web/rte_resources'
import { setup as db_conn_setup   } from './db/connection'

db_conn_setup()
server_start()
ws_server_start()
