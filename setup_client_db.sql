
{{#create}}
    -- user that extension pack will use to connecto to the DBs
    drop user if exists {{username}};
    create user {{username}};

    -- give client access to his DB
    grant connect on database {{username}}_db to {{username}};
    grant connect on database ourico_test to {{username}};

    -- add row level security and access restriction to the tables
    {{#tables}}
    drop policy if exists {{username}}_{{.}} on {{.}};
    create policy {{username}}_{{.}}
        on {{.}}
        for all -- select, update, delete, insert
        to {{username}}
        using (group_id = {{group_id}})       -- only read/alter data in his group
        with check (group_id = {{group_id}}); -- only add data to his group
    {{/tables}}
{{/create}}
{{^create}}
    drop owned by {{username}};
    drop user if exists {{username}};
    {{#tables}}
    -- drop the create policies
    drop policy if exists {{username}}_{{.}} on {{.}};
    {{/tables}}
{{/create}}

