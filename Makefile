all:
	rm -r out/*
	tsc
	cp config.json out
	cp mote_comm/protocol_spec.json out/mote_comm/

run:
	node ./out/main.js

debug:
	node --inspect ./out/main.js
