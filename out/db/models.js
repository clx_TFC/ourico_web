"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var typeorm_1 = require("typeorm");
var data_resources_1 = require("../web/api/data_resources");
/// Data associated with a mote
var Mote = /** @class */ (function (_super) {
    __extends(Mote, _super);
    function Mote() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Mote_1 = Mote;
    var Mote_1;
    Mote.table_name = 'mote';
    Mote.hidden_fields = [];
    Mote.exposed_fields = [];
    Mote.non_writable_fields = [];
    Mote.related_models = {};
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], Mote.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, type: "text" }),
        __metadata("design:type", String)
    ], Mote.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column({ type: "real", nullable: true }),
        __metadata("design:type", Number)
    ], Mote.prototype, "location_lat", void 0);
    __decorate([
        typeorm_1.Column({ type: "real", nullable: true }),
        __metadata("design:type", Number)
    ], Mote.prototype, "location_lng", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], Mote.prototype, "active", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, type: "text" }),
        __metadata("design:type", String)
    ], Mote.prototype, "current_ip", void 0);
    __decorate([
        typeorm_1.Column({ type: "timestamp", nullable: true }),
        __metadata("design:type", Date
        /// Maximum time that a `Mote` can take without sending `MoteStatusReport`s
        /// before being considered inactive
        )
    ], Mote.prototype, "last_status_report", void 0);
    __decorate([
        typeorm_1.Column({ type: "time", nullable: true }),
        __metadata("design:type", Date
        /// Key used to encrypt the communication with the `Mote`
        )
    ], Mote.prototype, "max_time_with_no_report", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, type: "text", select: false }),
        __metadata("design:type", String)
    ], Mote.prototype, "crypto_key", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, type: "text" }),
        __metadata("design:type", String)
    ], Mote.prototype, "crypto_alg", void 0);
    __decorate([
        typeorm_1.Column({ type: "int", nullable: true, select: false }),
        __metadata("design:type", Number)
    ], Mote.prototype, "crypto_last_nonce_in", void 0);
    __decorate([
        typeorm_1.Column({ type: "int", nullable: true, select: false }),
        __metadata("design:type", Number)
    ], Mote.prototype, "crypto_last_nonce_out", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, type: "text" }),
        __metadata("design:type", String)
    ], Mote.prototype, "purpose_description", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, type: "text" }),
        __metadata("design:type", String)
    ], Mote.prototype, "other_data", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Mote.prototype, "group_id", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return MoteError; }, function (error) { return error.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "errors", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return MoteConfiguration; }, function (conf) { return conf.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "configurations", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return MoteStatusReport; }, function (sr) { return sr.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "status_reports", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return MoteConnection; }, function (conn) { return conn.mote_here; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "conns_from_here", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return MoteConnection; }, function (conn) { return conn.mote_there; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "conns_to_here", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return SensorUse; }, function (mote_use) { return mote_use.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "sensors", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ActuatorUse; }, function (act_use) { return act_use.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "actuators", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return AlertUse; }, function (alert_use) { return alert_use.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "alerts", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return TriggerUse; }, function (trigger_use) { return trigger_use.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "triggers", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ClientMote; }, function (client) { return client.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "clients", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Packet; }, function (packet) { return packet.mote; }),
        __metadata("design:type", Array)
    ], Mote.prototype, "packets", void 0);
    Mote = Mote_1 = __decorate([
        typeorm_1.Entity("mote"),
        data_resources_1.DataResource('mote', Mote_1)
    ], Mote);
    return Mote;
}(typeorm_1.BaseEntity));
exports.Mote = Mote;
/// Runtime errors sent from `Mote`s
var MoteError = /** @class */ (function (_super) {
    __extends(MoteError, _super);
    function MoteError() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MoteError_1 = MoteError;
    var MoteError_1;
    MoteError.table_name = 'mote_error';
    MoteError.hidden_fields = [];
    MoteError.exposed_fields = [];
    MoteError.non_writable_fields = [];
    MoteError.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "timestamp" }),
        __metadata("design:type", Date
        /// References `Mote::id`
        )
    ], MoteError.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.errors; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], MoteError.prototype, "mote", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], MoteError.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteError.prototype, "err_code", void 0);
    __decorate([
        typeorm_1.Column({ type: "text" }),
        __metadata("design:type", String)
    ], MoteError.prototype, "err_message", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], MoteError.prototype, "group_id", void 0);
    MoteError = MoteError_1 = __decorate([
        typeorm_1.Entity("mote_error"),
        data_resources_1.DataResource('mote_error', MoteError_1)
    ], MoteError);
    return MoteError;
}(typeorm_1.BaseEntity));
exports.MoteError = MoteError;
/// Defines a set of paramenters to coordinate the operation of the `Mote`
var MoteConfiguration = /** @class */ (function (_super) {
    __extends(MoteConfiguration, _super);
    function MoteConfiguration() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MoteConfiguration_1 = MoteConfiguration;
    var MoteConfiguration_1;
    MoteConfiguration.table_name = 'mote_configuration';
    MoteConfiguration.hidden_fields = [];
    MoteConfiguration.exposed_fields = [];
    MoteConfiguration.non_writable_fields = [];
    MoteConfiguration.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "text" }),
        __metadata("design:type", String)
    ], MoteConfiguration.prototype, "name", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.configurations; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], MoteConfiguration.prototype, "mote", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], MoteConfiguration.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteConfiguration.prototype, "measurement_period", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteConfiguration.prototype, "send_data_period", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteConfiguration.prototype, "status_report_period", void 0);
    __decorate([
        typeorm_1.Column({ type: "boolean" }),
        __metadata("design:type", Boolean)
    ], MoteConfiguration.prototype, "use_crypto", void 0);
    __decorate([
        typeorm_1.Column({ type: "boolean" }),
        __metadata("design:type", Boolean)
    ], MoteConfiguration.prototype, "active", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], MoteConfiguration.prototype, "group_id", void 0);
    MoteConfiguration = MoteConfiguration_1 = __decorate([
        typeorm_1.Entity("mote_configuration"),
        data_resources_1.DataResource('mote_configuration', MoteConfiguration_1)
    ], MoteConfiguration);
    return MoteConfiguration;
}(typeorm_1.BaseEntity));
exports.MoteConfiguration = MoteConfiguration;
/// Data sent in a status report
var MoteStatusReport = /** @class */ (function (_super) {
    __extends(MoteStatusReport, _super);
    function MoteStatusReport() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MoteStatusReport_1 = MoteStatusReport;
    var MoteStatusReport_1;
    MoteStatusReport.table_name = 'mote_status_report';
    MoteStatusReport.hidden_fields = [];
    MoteStatusReport.exposed_fields = [];
    MoteStatusReport.non_writable_fields = [];
    MoteStatusReport.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.status_reports; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], MoteStatusReport.prototype, "mote", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], MoteStatusReport.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.PrimaryColumn({ type: "timestamp" }),
        __metadata("design:type", Date
        /// Time that the report data gathering ended
        )
    ], MoteStatusReport.prototype, "start_date_time", void 0);
    __decorate([
        typeorm_1.Column({ type: "timestamp" }),
        __metadata("design:type", Date
        /// Number of packets transmitted during the report data gathering period
        )
    ], MoteStatusReport.prototype, "end_date_time", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteStatusReport.prototype, "nr_tx_packets", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteStatusReport.prototype, "nr_lost_packets", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteStatusReport.prototype, "nr_tx_bytes", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], MoteStatusReport.prototype, "nr_lost_bytes", void 0);
    MoteStatusReport = MoteStatusReport_1 = __decorate([
        typeorm_1.Entity("mote_status_report"),
        data_resources_1.DataResource('mote_status_report', MoteStatusReport_1)
    ], MoteStatusReport);
    return MoteStatusReport;
}(typeorm_1.BaseEntity));
exports.MoteStatusReport = MoteStatusReport;
/// A connetion between `Mote`s
var MoteConnection = /** @class */ (function (_super) {
    __extends(MoteConnection, _super);
    function MoteConnection() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    MoteConnection_1 = MoteConnection;
    var MoteConnection_1;
    MoteConnection.table_name = 'mote_connection';
    MoteConnection.hidden_fields = [];
    MoteConnection.exposed_fields = [];
    MoteConnection.non_writable_fields = [];
    MoteConnection.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.conns_from_here; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], MoteConnection.prototype, "mote_here", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], MoteConnection.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.conns_to_here; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "other_mote", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], MoteConnection.prototype, "mote_there", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], MoteConnection.prototype, "other_mote", void 0);
    __decorate([
        typeorm_1.PrimaryColumn(),
        __metadata("design:type", String)
    ], MoteConnection.prototype, "direction", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], MoteConnection.prototype, "group_id", void 0);
    MoteConnection = MoteConnection_1 = __decorate([
        typeorm_1.Entity("mote_connection"),
        data_resources_1.DataResource('mote_connection', MoteConnection_1)
    ], MoteConnection);
    return MoteConnection;
}(typeorm_1.BaseEntity));
exports.MoteConnection = MoteConnection;
/// Data associated with a sensor
var Sensor = /** @class */ (function (_super) {
    __extends(Sensor, _super);
    function Sensor() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Sensor_1 = Sensor;
    var Sensor_1;
    Sensor.table_name = 'sensor';
    Sensor.hidden_fields = [];
    Sensor.exposed_fields = [];
    Sensor.non_writable_fields = [];
    Sensor.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "int" }),
        __metadata("design:type", Number)
    ], Sensor.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({ type: "int" }),
        __metadata("design:type", Number)
    ], Sensor.prototype, "output_size", void 0);
    __decorate([
        typeorm_1.Column({ type: "text" }),
        __metadata("design:type", String)
    ], Sensor.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column({ type: "text" }),
        __metadata("design:type", String)
    ], Sensor.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column({ type: "boolean" }),
        __metadata("design:type", Boolean)
    ], Sensor.prototype, "async", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Parameter; }, function (sp) { return sp.sensor; }),
        __metadata("design:type", Array)
    ], Sensor.prototype, "parameters", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return SensorUse; }, function (su) { return su.sensor; }),
        __metadata("design:type", Array)
    ], Sensor.prototype, "uses", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Sensor.prototype, "group_id", void 0);
    Sensor = Sensor_1 = __decorate([
        typeorm_1.Entity("sensor"),
        data_resources_1.DataResource('sensor', Sensor_1)
    ], Sensor);
    return Sensor;
}(typeorm_1.BaseEntity));
exports.Sensor = Sensor;
/// Data associated with a parameter
/// #Examples
/// ```
/// Parameter {
///   id: 12
///   name: "Temperature in Celsius"
///   units: "ºC"
///   description: ""
/// }
/// ```
var Parameter = /** @class */ (function (_super) {
    __extends(Parameter, _super);
    function Parameter() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Parameter_1 = Parameter;
    var Parameter_1;
    Parameter.table_name = 'parameter';
    Parameter.hidden_fields = [];
    Parameter.exposed_fields = [];
    Parameter.non_writable_fields = [];
    Parameter.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "int" }),
        __metadata("design:type", Number)
    ], Parameter.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column({ type: "text" }),
        __metadata("design:type", String)
    ], Parameter.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column({ type: "text" }),
        __metadata("design:type", String)
    ], Parameter.prototype, "units", void 0);
    __decorate([
        typeorm_1.Column({ type: "text" }),
        __metadata("design:type", String)
    ], Parameter.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column({ name: "type", type: "text" }),
        __metadata("design:type", String)
    ], Parameter.prototype, "type_", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Parameter.prototype, "size", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Parameter.prototype, "position", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Sensor; }, function (sensor) { return sensor.parameters; }),
        typeorm_1.JoinColumn({ name: "sensor_id", referencedColumnName: "id" }),
        __metadata("design:type", Sensor)
    ], Parameter.prototype, "sensor", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], Parameter.prototype, "sensor_id", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Measurement; }, function (meas) { return meas.parameter; }),
        __metadata("design:type", Array)
    ], Parameter.prototype, "measurements", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Parameter.prototype, "group_id", void 0);
    Parameter = Parameter_1 = __decorate([
        typeorm_1.Entity("parameter"),
        data_resources_1.DataResource('parameter', Parameter_1)
    ], Parameter);
    return Parameter;
}(typeorm_1.BaseEntity));
exports.Parameter = Parameter;
/// Indicates that a `Sensor` is being used in a `Mote`.
var SensorUse = /** @class */ (function (_super) {
    __extends(SensorUse, _super);
    function SensorUse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    SensorUse_1 = SensorUse;
    var SensorUse_1;
    SensorUse.table_name = 'sensor_use';
    SensorUse.hidden_fields = [];
    SensorUse.exposed_fields = [];
    SensorUse.non_writable_fields = [];
    SensorUse.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "int" }),
        __metadata("design:type", Number)
    ], SensorUse.prototype, "num", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Sensor; }, function (sensor) { return sensor.uses; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "sensor_id", referencedColumnName: "id" }),
        __metadata("design:type", Sensor)
    ], SensorUse.prototype, "sensor", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], SensorUse.prototype, "sensor_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.sensors; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], SensorUse.prototype, "mote", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], SensorUse.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], SensorUse.prototype, "interface_num", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], SensorUse.prototype, "interface_name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], SensorUse.prototype, "use_description", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], SensorUse.prototype, "active", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date
        /// last time it was deactivated
        )
    ], SensorUse.prototype, "activated_at", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date)
    ], SensorUse.prototype, "deactivated_at", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Measurement; }, function (measurement) { return measurement.sensor_use; }),
        __metadata("design:type", Array)
    ], SensorUse.prototype, "measurements", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], SensorUse.prototype, "group_id", void 0);
    SensorUse = SensorUse_1 = __decorate([
        typeorm_1.Entity("sensor_use"),
        data_resources_1.DataResource('sensor_use', SensorUse_1)
    ], SensorUse);
    return SensorUse;
}(typeorm_1.BaseEntity));
exports.SensorUse = SensorUse;
/** Data associated with a mesasurement */
var Measurement = /** @class */ (function (_super) {
    __extends(Measurement, _super);
    function Measurement() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Measurement_1 = Measurement;
    var Measurement_1;
    Measurement.table_name = 'measurement';
    Measurement.hidden_fields = [];
    Measurement.exposed_fields = [];
    Measurement.non_writable_fields = [];
    Measurement.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Parameter; }, function (param) { return param.measurements; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "parameter_id", referencedColumnName: "id" }),
        __metadata("design:type", Parameter
        /// References `SensorUse`
        )
    ], Measurement.prototype, "parameter", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return SensorUse; }, function (su) { return su.measurements; }, { primary: true }),
        typeorm_1.JoinColumn([
            { name: "sensor_use_num", referencedColumnName: "num" },
            { name: "sensor_id", referencedColumnName: "sensor_id" },
            { name: "mote_id", referencedColumnName: "mote_id" }
        ]),
        __metadata("design:type", SensorUse
        /// Date and time of the measurement
        )
    ], Measurement.prototype, "sensor_use", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("timestamp"),
        __metadata("design:type", Date
        /// string representation of the bytes of the measurement
        )
    ], Measurement.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Measurement.prototype, "value", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Measurement.prototype, "group_id", void 0);
    Measurement = Measurement_1 = __decorate([
        typeorm_1.Entity("measurement"),
        data_resources_1.DataResource('measurement', Measurement_1)
    ], Measurement);
    return Measurement;
}(typeorm_1.BaseEntity));
exports.Measurement = Measurement;
/// Data associated with an alert
var Alert = /** @class */ (function (_super) {
    __extends(Alert, _super);
    function Alert() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Alert_1 = Alert;
    var Alert_1;
    Alert.table_name = 'alert';
    Alert.hidden_fields = [];
    Alert.exposed_fields = [];
    Alert.non_writable_fields = [];
    Alert.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn("int"),
        __metadata("design:type", Number)
    ], Alert.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Alert.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Alert.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Alert.prototype, "level", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return AlertUse; }, function (au) { return au.alert; }),
        __metadata("design:type", Array)
    ], Alert.prototype, "uses", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Alert.prototype, "group_id", void 0);
    Alert = Alert_1 = __decorate([
        typeorm_1.Entity("alert"),
        data_resources_1.DataResource('alert', Alert_1)
    ], Alert);
    return Alert;
}(typeorm_1.BaseEntity));
exports.Alert = Alert;
/// Indicates that an `Alert` is being used in a `Mote`
var AlertUse = /** @class */ (function (_super) {
    __extends(AlertUse, _super);
    function AlertUse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AlertUse_1 = AlertUse;
    var AlertUse_1;
    AlertUse.table_name = 'alert_use';
    AlertUse.hidden_fields = [];
    AlertUse.exposed_fields = [];
    AlertUse.non_writable_fields = [];
    AlertUse.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Alert; }, function (alert) { return alert.uses; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "alert_id", referencedColumnName: "id" }),
        __metadata("design:type", Alert)
    ], AlertUse.prototype, "alert", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("int"),
        __metadata("design:type", Number)
    ], AlertUse.prototype, "alert_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.alerts; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], AlertUse.prototype, "mote", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("int"),
        __metadata("design:type", Number)
    ], AlertUse.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], AlertUse.prototype, "active", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date
        /// last time it was deactivated
        )
    ], AlertUse.prototype, "activated_at", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date)
    ], AlertUse.prototype, "deactivated_at", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return AlertOccurrence; }, function (ao) { return ao.alert_use; }),
        __metadata("design:type", Array)
    ], AlertUse.prototype, "occurrences", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], AlertUse.prototype, "group_id", void 0);
    AlertUse = AlertUse_1 = __decorate([
        typeorm_1.Entity("alert_use"),
        data_resources_1.DataResource('alert_use', AlertUse_1)
    ], AlertUse);
    return AlertUse;
}(typeorm_1.BaseEntity));
exports.AlertUse = AlertUse;
/// An occurrence of an alert
var AlertOccurrence = /** @class */ (function (_super) {
    __extends(AlertOccurrence, _super);
    function AlertOccurrence() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AlertOccurrence_1 = AlertOccurrence;
    var AlertOccurrence_1;
    AlertOccurrence.table_name = 'alert_occurrence';
    AlertOccurrence.hidden_fields = [];
    AlertOccurrence.exposed_fields = [];
    AlertOccurrence.non_writable_fields = [];
    AlertOccurrence.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return AlertUse; }, function (au) { return au.occurrences; }, { primary: true }),
        typeorm_1.JoinColumn([
            { name: "alert_id", referencedColumnName: "alert_id" },
            { name: "mote_id", referencedColumnName: "mote_id" }
        ]),
        __metadata("design:type", AlertUse
        /// Date and time of the alert occurrence
        /// Every `Measurement` with this
        /// `date_time` from the `Mote` referred by `mote_id`
        /// are associated with this `AlertOccurrence`
        )
    ], AlertOccurrence.prototype, "alert_use", void 0);
    __decorate([
        typeorm_1.PrimaryColumn({ type: "timestamp" }),
        __metadata("design:type", Date
        /// the group thath this item belongs to
        )
    ], AlertOccurrence.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], AlertOccurrence.prototype, "group_id", void 0);
    AlertOccurrence = AlertOccurrence_1 = __decorate([
        typeorm_1.Entity("alert_occurrence"),
        data_resources_1.DataResource('alert_occurrence', AlertOccurrence_1)
    ], AlertOccurrence);
    return AlertOccurrence;
}(typeorm_1.BaseEntity));
exports.AlertOccurrence = AlertOccurrence;
/// Data associated with an trigger
var Trigger = /** @class */ (function (_super) {
    __extends(Trigger, _super);
    function Trigger() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Trigger_1 = Trigger;
    var Trigger_1;
    Trigger.table_name = 'trigger';
    Trigger.hidden_fields = [];
    Trigger.exposed_fields = [];
    Trigger.non_writable_fields = [];
    Trigger.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn("int"),
        __metadata("design:type", Number)
    ], Trigger.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Trigger.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Trigger.prototype, "description", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return TriggerUse; }, function (tu) { return tu.trigger; }),
        __metadata("design:type", Array)
    ], Trigger.prototype, "uses", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Trigger.prototype, "group_id", void 0);
    Trigger = Trigger_1 = __decorate([
        typeorm_1.Entity("trigger"),
        data_resources_1.DataResource('trigger', Trigger_1)
    ], Trigger);
    return Trigger;
}(typeorm_1.BaseEntity));
exports.Trigger = Trigger;
/// Indicates that a `Trigger` is being used in a `Mote`
var TriggerUse = /** @class */ (function (_super) {
    __extends(TriggerUse, _super);
    function TriggerUse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TriggerUse_1 = TriggerUse;
    var TriggerUse_1;
    TriggerUse.table_name = 'trigger_use';
    TriggerUse.hidden_fields = [];
    TriggerUse.exposed_fields = [];
    TriggerUse.non_writable_fields = [];
    TriggerUse.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Trigger; }, function (trigger) { return trigger.uses; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "trigger_id", referencedColumnName: "id" }),
        __metadata("design:type", Trigger)
    ], TriggerUse.prototype, "trigger", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("int"),
        __metadata("design:type", Number)
    ], TriggerUse.prototype, "trigger_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.triggers; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], TriggerUse.prototype, "mote", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("int"),
        __metadata("design:type", Number)
    ], TriggerUse.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], TriggerUse.prototype, "active", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date
        /// last time it was deactivated
        )
    ], TriggerUse.prototype, "activated_at", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date)
    ], TriggerUse.prototype, "deactivated_at", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return TriggerOccurrence; }, function (to) { return to.trigger_use; }),
        __metadata("design:type", Array)
    ], TriggerUse.prototype, "occurrences", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], TriggerUse.prototype, "group_id", void 0);
    TriggerUse = TriggerUse_1 = __decorate([
        typeorm_1.Entity("trigger_use"),
        data_resources_1.DataResource('trigger_use', TriggerUse_1)
    ], TriggerUse);
    return TriggerUse;
}(typeorm_1.BaseEntity));
exports.TriggerUse = TriggerUse;
/// An occurrence of an trigger
var TriggerOccurrence = /** @class */ (function (_super) {
    __extends(TriggerOccurrence, _super);
    function TriggerOccurrence() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    TriggerOccurrence_1 = TriggerOccurrence;
    var TriggerOccurrence_1;
    TriggerOccurrence.table_name = 'trigger_occurrence';
    TriggerOccurrence.hidden_fields = [];
    TriggerOccurrence.exposed_fields = [];
    TriggerOccurrence.non_writable_fields = [];
    TriggerOccurrence.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return TriggerUse; }, function (tu) { return tu.occurrences; }, { primary: true }),
        typeorm_1.JoinColumn([
            { name: "trigger_id", referencedColumnName: "trigger_id" },
            { name: "mote_id", referencedColumnName: "mote_id" }
        ]),
        __metadata("design:type", TriggerUse
        /// Date and time of the trigger occurrence
        /// Every `Measurement` and `ActionResult` with this
        /// `date_time` from the `Mote` referred to by `mote_id`
        /// are associated withthis `TriggerOccurrence`
        )
    ], TriggerOccurrence.prototype, "trigger_use", void 0);
    __decorate([
        typeorm_1.PrimaryColumn({ type: "timestamp" }),
        __metadata("design:type", Date
        /// the group thath this item belongs to
        )
    ], TriggerOccurrence.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], TriggerOccurrence.prototype, "group_id", void 0);
    TriggerOccurrence = TriggerOccurrence_1 = __decorate([
        typeorm_1.Entity("trigger_occurrence"),
        data_resources_1.DataResource('trigger_occurrence', TriggerOccurrence_1)
    ], TriggerOccurrence);
    return TriggerOccurrence;
}(typeorm_1.BaseEntity));
exports.TriggerOccurrence = TriggerOccurrence;
/// Data associated with an actuator
var Actuator = /** @class */ (function (_super) {
    __extends(Actuator, _super);
    function Actuator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Actuator_1 = Actuator;
    var Actuator_1;
    Actuator.table_name = 'actuator';
    Actuator.hidden_fields = [];
    Actuator.exposed_fields = [];
    Actuator.non_writable_fields = [];
    Actuator.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "int" }),
        __metadata("design:type", Number)
    ], Actuator.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Actuator.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Actuator.prototype, "name", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Action; }, function (action) { return action.actuator; }),
        __metadata("design:type", Array)
    ], Actuator.prototype, "actions", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ActuatorUse; }, function (au) { return au.actuator; }),
        __metadata("design:type", Array)
    ], Actuator.prototype, "uses", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Actuator.prototype, "group_id", void 0);
    Actuator = Actuator_1 = __decorate([
        typeorm_1.Entity("actuator"),
        data_resources_1.DataResource('actuator', Actuator_1)
    ], Actuator);
    return Actuator;
}(typeorm_1.BaseEntity));
exports.Actuator = Actuator;
/// Indicates a possible action that an `Actuator` can perform.
var Action = /** @class */ (function (_super) {
    __extends(Action, _super);
    function Action() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Action_1 = Action;
    var Action_1;
    Action.table_name = 'action';
    Action.hidden_fields = [];
    Action.exposed_fields = [];
    Action.non_writable_fields = [];
    Action.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "int" }),
        __metadata("design:type", Number)
    ], Action.prototype, "num", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Actuator; }, function (actuator) { return actuator.actions; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "actuator_id", referencedColumnName: "id" }),
        __metadata("design:type", Actuator)
    ], Action.prototype, "actuator", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], Action.prototype, "actuator_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Action.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column("bigint"),
        __metadata("design:type", Number)
    ], Action.prototype, "signal", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Action.prototype, "description", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Action.prototype, "group_id", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ActionResult; }, function (ar) { return ar.action; }),
        __metadata("design:type", Array)
    ], Action.prototype, "results", void 0);
    Action = Action_1 = __decorate([
        typeorm_1.Entity("action"),
        data_resources_1.DataResource('action', Action_1)
    ], Action);
    return Action;
}(typeorm_1.BaseEntity));
exports.Action = Action;
/// Indicates that a `Actuator` is being used in a `Mote`.
var ActuatorUse = /** @class */ (function (_super) {
    __extends(ActuatorUse, _super);
    function ActuatorUse() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ActuatorUse_1 = ActuatorUse;
    var ActuatorUse_1;
    ActuatorUse.table_name = 'actuator_use';
    ActuatorUse.hidden_fields = [];
    ActuatorUse.exposed_fields = [];
    ActuatorUse.non_writable_fields = [];
    ActuatorUse.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn({ type: "int" }),
        __metadata("design:type", Number)
    ], ActuatorUse.prototype, "num", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Actuator; }, function (actuator) { return actuator.uses; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "actuator_id", referencedColumnName: "id" }),
        __metadata("design:type", Actuator)
    ], ActuatorUse.prototype, "actuator", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], ActuatorUse.prototype, "actuator_id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.actuators; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], ActuatorUse.prototype, "mote", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], ActuatorUse.prototype, "mote_id", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], ActuatorUse.prototype, "interface_num", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ActuatorUse.prototype, "interface_name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ActuatorUse.prototype, "use_description", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ActuatorUse.prototype, "active", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date
        /// last time it was deactivated
        )
    ], ActuatorUse.prototype, "activated_at", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date)
    ], ActuatorUse.prototype, "deactivated_at", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ActionResult; }, function (ar) { return ar.actuator_use; }),
        __metadata("design:type", Array)
    ], ActuatorUse.prototype, "results", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], ActuatorUse.prototype, "group_id", void 0);
    ActuatorUse = ActuatorUse_1 = __decorate([
        typeorm_1.Entity("actuator_use"),
        data_resources_1.DataResource('actuator_use', ActuatorUse_1)
    ], ActuatorUse);
    return ActuatorUse;
}(typeorm_1.BaseEntity));
exports.ActuatorUse = ActuatorUse;
/// Result of an `Action` execution
var ActionResult = /** @class */ (function (_super) {
    __extends(ActionResult, _super);
    function ActionResult() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ActionResult_1 = ActionResult;
    var ActionResult_1;
    ActionResult.table_name = 'action_result';
    ActionResult.hidden_fields = [];
    ActionResult.exposed_fields = [];
    ActionResult.non_writable_fields = [];
    ActionResult.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Action; }, function (ac) { return ac.results; }, { primary: true }),
        typeorm_1.JoinColumn([
            { name: "action_num", referencedColumnName: "num" },
            { name: "actuator_id", referencedColumnName: "actuator_id" }
        ]),
        __metadata("design:type", Action
        /// References `ActuatorUse`
        )
    ], ActionResult.prototype, "action", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return ActuatorUse; }, function (au) { return au.results; }, { primary: true }),
        typeorm_1.JoinColumn([
            { name: "actuator_use_num", referencedColumnName: "num" },
            { name: "actuator_id", referencedColumnName: "actuator_id" },
            { name: "mote_id", referencedColumnName: "mote_id" }
        ]),
        __metadata("design:type", ActuatorUse
        /// Data and time of the action result
        )
    ], ActionResult.prototype, "actuator_use", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("timestamp"),
        __metadata("design:type", Date
        /// Result of the action
        )
    ], ActionResult.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], ActionResult.prototype, "result", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], ActionResult.prototype, "group_id", void 0);
    ActionResult = ActionResult_1 = __decorate([
        typeorm_1.Entity("action_result"),
        data_resources_1.DataResource('action_result', ActionResult_1)
    ], ActionResult);
    return ActionResult;
}(typeorm_1.BaseEntity));
exports.ActionResult = ActionResult;
/// Data associated with a user
var Client = /** @class */ (function (_super) {
    __extends(Client, _super);
    function Client() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Client_1 = Client;
    var Client_1;
    Client.table_name = 'client';
    Client.hidden_fields = [];
    Client.exposed_fields = [];
    Client.non_writable_fields = [];
    Client.updatable_fields = [];
    Client.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn(),
        __metadata("design:type", String)
    ], Client.prototype, "id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "name", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "username", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "password", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "email", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], Client.prototype, "admin", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Client_1; }, function (cli) { return cli.owned_users; }),
        typeorm_1.JoinColumn({ name: "client_id", referencedColumnName: "id" }),
        __metadata("design:type", Client)
    ], Client.prototype, "owner", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], Client.prototype, "client_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], Client.prototype, "write_capability", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], Client.prototype, "compute_capability", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], Client.prototype, "rt_events_capability", void 0);
    __decorate([
        typeorm_1.Column({ nullable: true, select: false }),
        __metadata("design:type", String)
    ], Client.prototype, "access_token", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ClientMote; }, function (um) { return um.client; }),
        __metadata("design:type", Array)
    ], Client.prototype, "motes", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ApiClient; }, function (um) { return um.owner; }),
        __metadata("design:type", Array)
    ], Client.prototype, "api_clients", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return UserOperationsLog; }, function (log) { return log.owner; }),
        __metadata("design:type", Array)
    ], Client.prototype, "logs", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ExtensionPack; }, function (pack) { return pack.owner; }),
        __metadata("design:type", Array)
    ], Client.prototype, "extension_packs", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return Client_1; }, function (cli) { return cli.owner; }),
        __metadata("design:type", Array)
    ], Client.prototype, "owned_users", void 0);
    __decorate([
        typeorm_1.Generated(),
        typeorm_1.Column(),
        __metadata("design:type", Number)
    ], Client.prototype, "group_id", void 0);
    Client = Client_1 = __decorate([
        typeorm_1.Entity("client"),
        data_resources_1.DataResource('client', Client_1)
    ], Client);
    return Client;
}(typeorm_1.BaseEntity));
exports.Client = Client;
/// Association between a `Client` and a `Mote`
var ClientMote = /** @class */ (function (_super) {
    __extends(ClientMote, _super);
    function ClientMote() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ClientMote_1 = ClientMote;
    var ClientMote_1;
    ClientMote.table_name = 'user_mote';
    ClientMote.hidden_fields = [];
    ClientMote.exposed_fields = [];
    ClientMote.non_writable_fields = [];
    ClientMote.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Client; }, function (client) { return client.motes; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "client_id", referencedColumnName: "id" }),
        __metadata("design:type", Client
        /// References `Mote::id`
        )
    ], ClientMote.prototype, "client", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.clients; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Number)
    ], ClientMote.prototype, "mote", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ClientMote.prototype, "receive_alerts", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ClientMote.prototype, "receive_triggers", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ClientMote.prototype, "receive_action_results", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ClientMote.prototype, "receive_measurements", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ClientMote.prototype, "receive_errors", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], ClientMote.prototype, "group_id", void 0);
    ClientMote = ClientMote_1 = __decorate([
        typeorm_1.Entity("user_mote"),
        data_resources_1.DataResource('client_mote', ClientMote_1)
    ], ClientMote);
    return ClientMote;
}(typeorm_1.BaseEntity));
exports.ClientMote = ClientMote;
/// Data associated with a client of the WEB API
var ApiClient = /** @class */ (function (_super) {
    __extends(ApiClient, _super);
    function ApiClient() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ApiClient_1 = ApiClient;
    var ApiClient_1;
    ApiClient.table_name = 'api_client';
    ApiClient.hidden_fields = [];
    ApiClient.exposed_fields = [];
    ApiClient.non_writable_fields = [];
    ApiClient.updatable_fields = [];
    ApiClient.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn(),
        __metadata("design:type", String)
    ], ApiClient.prototype, "id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Client; }, function (client) { return client.api_clients; }),
        typeorm_1.JoinColumn({ name: "client_id", referencedColumnName: "id" }),
        __metadata("design:type", Client)
    ], ApiClient.prototype, "owner", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiClient.prototype, "client_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiClient.prototype, "access_token", void 0);
    __decorate([
        typeorm_1.Column("timestamp with time zone"),
        __metadata("design:type", Date
        /// Token to be used to update authorization after `access_token` expires
        )
    ], ApiClient.prototype, "token_expire_date", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiClient.prototype, "token_update_token", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ApiClient.prototype, "write_capability", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ApiClient.prototype, "compute_capability", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ApiClient.prototype, "rt_events_capability", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiClient.prototype, "allowed_origin_address", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiClient.prototype, "name", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ApiOperationsLog; }, function (log) { return log.owner; }),
        __metadata("design:type", Array)
    ], ApiClient.prototype, "logs", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], ApiClient.prototype, "group_id", void 0);
    ApiClient = ApiClient_1 = __decorate([
        typeorm_1.Entity("api_client"),
        data_resources_1.DataResource('api_client', ApiClient_1)
    ], ApiClient);
    return ApiClient;
}(typeorm_1.BaseEntity));
exports.ApiClient = ApiClient;
// /// Indicates that a `Mote` can be accessed by an `ApiClient`
// @Entity("api_use_mote")
// @DataResource('api_client_mote', ApiClientMote)
// export class ApiClientMote {
//     /// References `ApiClient::id`
//     @ManyToOne(type => ApiClient, api_client => api_client.motes, { primary: true })
//     @JoinColumn({ name: "api_client_id", referencedColumnName: "id" })
//     api_client             : ApiClient
//     /// References `Mote::id`
//     @ManyToOne(type => Mote, mote => mote.api_clients, { primary: true })
//     @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
//     mote                   : Mote
//     // /// "R" `ApiClient` can only read data from the `Mote`
//     // /// "RW" `ApiClient` can only read and write data from the `Mote`
//     // permission             : string
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Alert`s from the `Mote`
//     @Column()
//     receive_alerts         : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Alert`s from the `Mote`
//     @Column()
//     receive_triggers       : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Action` results from the `Mote`
//     @Column()
//     receive_action_results : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Sensor` measurements from the `Mote`
//     @Column()
//     receive_measurements   : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of errors[`MoteError`] from the `Mote`
//     @Column()
//     receive_errors         : boolean
//
//
//     static readonly table_name: string = 'api_use_mote'
//     hidden_fields : string[] = []
//     related_models: object = {
//         'api_client': ['api_client', ApiClient],
//         'mote'      : ['mote', Mote]
//     }
//
//     read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
//     __sub_read: <Entity>(
//     query: Query,
//     query_builder: SelectQueryBuilder<Entity>,
//     first: boolean,
//     visited: string[],
// used_alias: string
// ) => void
//     write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
//     update: (
//         where: Filter[],
//         new_data: object  ,
//         client_id: string,
//         client_type: ClientType
//     ) => Promise<number>
//     delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<number>
//
// }
/// Data associated with an extension pack.
/// Extension packs are added by clients to perform operations on
/// `Mote`'s datas in the server side.
var ExtensionPack = /** @class */ (function (_super) {
    __extends(ExtensionPack, _super);
    function ExtensionPack() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ExtensionPack_1 = ExtensionPack;
    var ExtensionPack_1;
    ExtensionPack.table_name = 'extension_pack';
    ExtensionPack.hidden_fields = [];
    ExtensionPack.exposed_fields = [];
    ExtensionPack.non_writable_fields = [];
    ExtensionPack.related_models = {};
    __decorate([
        typeorm_1.PrimaryGeneratedColumn(),
        __metadata("design:type", Number)
    ], ExtensionPack.prototype, "id", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Client; }, function (client) { return client.extension_packs; }),
        typeorm_1.JoinColumn({ name: "client_id", referencedColumnName: "id" }),
        __metadata("design:type", Client)
    ], ExtensionPack.prototype, "owner", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ExtensionPack.prototype, "client_id", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ExtensionPack.prototype, "prog_language", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ExtensionPack.prototype, "code_repo_url", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ExtensionPack.prototype, "container_address", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ExtensionPack.prototype, "receive_alerts", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ExtensionPack.prototype, "receive_triggers", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ExtensionPack.prototype, "receive_action_results", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ExtensionPack.prototype, "receive_measurements", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ExtensionPack.prototype, "receive_errors", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", Boolean)
    ], ExtensionPack.prototype, "active", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ExtensionPack.prototype, "container_id", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], ExtensionPack.prototype, "group_id", void 0);
    __decorate([
        typeorm_1.OneToMany(function (type) { return ExtensionPackLog; }, function (log) { return log.owner; }),
        __metadata("design:type", Array)
    ], ExtensionPack.prototype, "logs", void 0);
    ExtensionPack = ExtensionPack_1 = __decorate([
        typeorm_1.Entity("extension_pack"),
        data_resources_1.DataResource('extension_pack', ExtensionPack_1)
    ], ExtensionPack);
    return ExtensionPack;
}(typeorm_1.BaseEntity));
exports.ExtensionPack = ExtensionPack;
/// To log operations performed by an `ApiClient`
var ApiOperationsLog = /** @class */ (function (_super) {
    __extends(ApiOperationsLog, _super);
    function ApiOperationsLog() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ApiOperationsLog_1 = ApiOperationsLog;
    var ApiOperationsLog_1;
    ApiOperationsLog.table_name = 'api_operations_log';
    ApiOperationsLog.hidden_fields = [];
    ApiOperationsLog.exposed_fields = [];
    ApiOperationsLog.non_writable_fields = [];
    ApiOperationsLog.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return ApiClient; }, function (api_client) { return api_client.logs; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "api_client_id", referencedColumnName: "id" }),
        __metadata("design:type", ApiClient)
    ], ApiOperationsLog.prototype, "owner", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiOperationsLog.prototype, "api_client_id", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("timestamp with time zone"),
        __metadata("design:type", Date
        /// The HTTP query used to perform the operation
        /// with all the body data
        )
    ], ApiOperationsLog.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiOperationsLog.prototype, "query", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiOperationsLog.prototype, "level", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiOperationsLog.prototype, "operation", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ApiOperationsLog.prototype, "message", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], ApiOperationsLog.prototype, "group_id", void 0);
    ApiOperationsLog = ApiOperationsLog_1 = __decorate([
        typeorm_1.Entity("api_operations_log"),
        data_resources_1.DataResource('api_operations_log', ApiOperationsLog_1)
    ], ApiOperationsLog);
    return ApiOperationsLog;
}(typeorm_1.BaseEntity));
exports.ApiOperationsLog = ApiOperationsLog;
/// To log operations performed by an `Client`
var UserOperationsLog = /** @class */ (function (_super) {
    __extends(UserOperationsLog, _super);
    function UserOperationsLog() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    UserOperationsLog_1 = UserOperationsLog;
    var UserOperationsLog_1;
    UserOperationsLog.table_name = 'user_operations_log';
    UserOperationsLog.hidden_fields = [];
    UserOperationsLog.exposed_fields = [];
    UserOperationsLog.non_writable_fields = [];
    UserOperationsLog.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Client; }, function (client) { return client.logs; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "client_id", referencedColumnName: "id" }),
        __metadata("design:type", Client)
    ], UserOperationsLog.prototype, "owner", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserOperationsLog.prototype, "client_id", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("timestamp with time zone"),
        __metadata("design:type", Date
        /// The HTTP query used to perform the operation
        /// with all the body data
        )
    ], UserOperationsLog.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserOperationsLog.prototype, "query", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserOperationsLog.prototype, "level", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserOperationsLog.prototype, "operation", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], UserOperationsLog.prototype, "message", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], UserOperationsLog.prototype, "group_id", void 0);
    UserOperationsLog = UserOperationsLog_1 = __decorate([
        typeorm_1.Entity("user_operations_log"),
        data_resources_1.DataResource('user_operations_log', UserOperationsLog_1)
    ], UserOperationsLog);
    return UserOperationsLog;
}(typeorm_1.BaseEntity));
exports.UserOperationsLog = UserOperationsLog;
/// To log operations performed by an `ExtensionPack`
var ExtensionPackLog = /** @class */ (function (_super) {
    __extends(ExtensionPackLog, _super);
    function ExtensionPackLog() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    ExtensionPackLog_1 = ExtensionPackLog;
    var ExtensionPackLog_1;
    ExtensionPackLog.table_name = 'extension_pack_log';
    ExtensionPackLog.hidden_fields = [];
    ExtensionPackLog.exposed_fields = [];
    ExtensionPackLog.non_writable_fields = [];
    ExtensionPackLog.related_models = {};
    __decorate([
        typeorm_1.ManyToOne(function (type) { return ExtensionPack; }, function (ext_pack) { return ext_pack.logs; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "extension_pack_id", referencedColumnName: "id" }),
        __metadata("design:type", ExtensionPack)
    ], ExtensionPackLog.prototype, "owner", void 0);
    __decorate([
        typeorm_1.Column("bigint"),
        __metadata("design:type", Number)
    ], ExtensionPackLog.prototype, "extension_pack_id", void 0);
    __decorate([
        typeorm_1.PrimaryColumn("timestamp with time zone"),
        __metadata("design:type", Date
        /// Indicates the severity level of the log
        /// Defined levels are info, warning, error
        )
    ], ExtensionPackLog.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ExtensionPackLog.prototype, "level", void 0);
    __decorate([
        typeorm_1.Column(),
        __metadata("design:type", String)
    ], ExtensionPackLog.prototype, "message", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], ExtensionPackLog.prototype, "group_id", void 0);
    ExtensionPackLog = ExtensionPackLog_1 = __decorate([
        typeorm_1.Entity("extension_pack_log"),
        data_resources_1.DataResource('extension_pack_log', ExtensionPackLog_1)
    ], ExtensionPackLog);
    return ExtensionPackLog;
}(typeorm_1.BaseEntity));
exports.ExtensionPackLog = ExtensionPackLog;
var Packet = /** @class */ (function (_super) {
    __extends(Packet, _super);
    function Packet() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Packet_1 = Packet;
    var Packet_1;
    Packet.table_name = 'packet';
    Packet.hidden_fields = [];
    Packet.exposed_fields = [];
    Packet.non_writable_fields = [];
    Packet.related_models = {};
    __decorate([
        typeorm_1.PrimaryColumn("timestamp"),
        __metadata("design:type", Date
        // mote that sent the packet
        )
    ], Packet.prototype, "date_time", void 0);
    __decorate([
        typeorm_1.ManyToOne(function (type) { return Mote; }, function (mote) { return mote.packets; }, { primary: true }),
        typeorm_1.JoinColumn({ name: "mote_id", referencedColumnName: "id" }),
        __metadata("design:type", Mote)
    ], Packet.prototype, "mote", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], Packet.prototype, "packet_type", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], Packet.prototype, "length", void 0);
    __decorate([
        typeorm_1.Column("int"),
        __metadata("design:type", Number)
    ], Packet.prototype, "packet_nr", void 0);
    __decorate([
        typeorm_1.Column({ select: false }),
        __metadata("design:type", Number)
    ], Packet.prototype, "group_id", void 0);
    Packet = Packet_1 = __decorate([
        typeorm_1.Entity("packet"),
        data_resources_1.DataResource("packet", Packet_1)
    ], Packet);
    return Packet;
}(typeorm_1.BaseEntity));
exports.Packet = Packet;
require("./models_ops/mote");
require("./models_ops/mote_error");
require("./models_ops/mote_configuration");
require("./models_ops/mote_status_report");
require("./models_ops/mote_connection");
require("./models_ops/parameter");
require("./models_ops/action_result");
require("./models_ops/action");
require("./models_ops/actuator_use");
require("./models_ops/actuator");
require("./models_ops/alert_occurrence");
require("./models_ops/alert_use");
require("./models_ops/alert");
require("./models_ops/api_client");
require("./models_ops/api_operations_log");
require("./models_ops/client_mote");
require("./models_ops/client");
require("./models_ops/extension_pack");
require("./models_ops/extension_pack_log");
require("./models_ops/measurement");
require("./models_ops/sensor_use");
require("./models_ops/sensor");
require("./models_ops/trigger_occurrence");
require("./models_ops/trigger");
require("./models_ops/trigger_use");
require("./models_ops/user_operations_log");
require("./models_ops/packet");
//# sourceMappingURL=models.js.map