"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
require("reflect-metadata");
var logger_js_1 = require("./logger.js");
logger_js_1.setup(); // always first to be initialized
var server_1 = require("./web/server");
var rte_resources_1 = require("./web/rte_resources");
var connection_1 = require("./db/connection");
connection_1.setup();
server_1.start();
rte_resources_1.ws_server_start();
//# sourceMappingURL=main.js.map