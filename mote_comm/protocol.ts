const protocol_spec = require('./protocol_spec.json')
import { loggers } from '../logger'

const logger = loggers.mote_comm

export enum PacketType {
    ACK                   = 0,
    NACK                  = 1,
    ERROR                 = 2,
    // send new configurations to mote
    CONF_SET              = 10,
    // send action instruction
    ACTION_INSTRUCTION    = 11,
    // request status report
    REQUEST_STATUS_REPORT = 12,
    // request measurements
    REQUEST_MEASUREMENTS  = 13,
    // activate an alert
    ALERT_SET             = 14,
    // deactivate an alert
    ALERT_REMOVE          = 15,
    // activate a trigger
    TRIGGER_SET           = 16,
    // deactivate a trigger
    TRIGGER_REMOVE        = 17,
    // activate a sensor
    SENSOR_ACTIVATE       = 18,
    // deactivate a sensor
    SENSOR_DEACTIVATE     = 19,
    // activate an actuator
    ACTUATOR_ACTIVATE     = 20,
    // deactivate an actuator
    ACTUATOR_DEACTIVATE   = 21,
    // reset mote
    RESET                 = 22
}

// to map packet type to name in protocol_spec.json
export const name_in_spec = {}
name_in_spec[PacketType.ACK]                   = 'ack'
name_in_spec[PacketType.NACK]                  = 'nack'
name_in_spec[PacketType.ERROR]                 = 'error'
name_in_spec[PacketType.CONF_SET]              = 'conf_set'
name_in_spec[PacketType.ACTION_INSTRUCTION]    = 'action_instr'
name_in_spec[PacketType.REQUEST_MEASUREMENTS]  = 'request_measurements'
name_in_spec[PacketType.REQUEST_STATUS_REPORT] = 'request_status_report'
name_in_spec[PacketType.ALERT_SET]             = 'alert_set'
name_in_spec[PacketType.ALERT_REMOVE]          = 'alert_remove'
name_in_spec[PacketType.TRIGGER_SET]           = 'trigger_set'
name_in_spec[PacketType.TRIGGER_REMOVE]        = 'trigger_remove'
name_in_spec[PacketType.SENSOR_ACTIVATE]       = 'sensor_activate'
name_in_spec[PacketType.SENSOR_DEACTIVATE]     = 'sensor_deactivate'
name_in_spec[PacketType.ACTUATOR_ACTIVATE]     = 'actuator_activate'
name_in_spec[PacketType.ACTUATOR_DEACTIVATE]   = 'actuator_deactivate'
name_in_spec[PacketType.RESET]                 = 'reset'


export class ProtocolError {
    constructor (public message: string) { }
}


/**
 * Set packet header fields
 * header - mote_id(2 bytes)|packet_no(1)|packet_type(1)|packet_len(2)
 *
 * @param mote_id     mote where packet is being sent (2 bytes)
 * @param packet_no   packet number (used to sync acks and nacks) (1 byte)
 * @param packet_type (1 byte)
 * @param packet_len  packet length in bytes (2 bytes)
 * @param packet
 *
 */
function make_header (

    mote_id    : number,
    packet_no  : number,
    packet_type: PacketType,
    packet_len : number,
    packet     : Uint8Array

) {

    packet[0] = 0xff & (mote_id >> 8)
    packet[1] = 0xff & (mote_id)
    packet[2] = 0xff & packet_no
    packet[3] = 0xff & packet_type
    packet[4] = 0xff & (packet_len >> 8);
    packet[5] = 0xff & (packet_len);

}


/**
 * Create a new packet
 *
 * @param packet_type
 * @param packet_no   packet number (used to sync acks and nacks)
 * @param data        data according to 'packet_type'
 *
 * TODO: encrypt packet
 *
 * @return the packet as a byte array
 * @throws ProtocolError
 */
export function make_packet
(mote_id: number, packet_no: number, packet_type: PacketType, data: object): Uint8Array {

    let fields     = protocol_spec[name_in_spec[packet_type]]
    // payload length of this packet type
    let pt_len     = protocol_spec[name_in_spec[packet_type] + 'payload_length']
    let header_len = protocol_spec['header_length']
    // create packet with space for the header and for the payload
    let packet     = new Uint8Array(pt_len + header_len)

    let pos = header_len
    for (var field in fields) {
        if (data[field] === undefined)
            throw new ProtocolError(`field ${field} is necessary to construct
                 packet of type ${name_in_spec[packet_type]}`)

        let field_value: number = data[field]
        let field_len  : number   = fields[field]
        for (var i = 0; i < field_len; i++) {
            packet[pos + i] = ((field_value & 0xff) >> (8*(field_len-i-1)))
        }
        pos += field_len
    }

    logger.verbose(`@protocol created ${name_in_spec[packet_type]} with ${pos} bytes`)

    make_header(mote_id, packet_no, packet_type, pos - header_len, packet)
    return packet
}


/**
 * Parse a mote response packet
 * Response packets are ACK or ERROR
 *
 * @returns packet fields in object
 * @throws ProtocolError
 */
export function parse_mote_response (data: Buffer): object {

    let res: object = {}

    if (data.length < protocol_spec['header_len']) {
        logger.verbose(`@protocol ${data} with length less than header length`)
        throw new ProtocolError(`corrupted data received from mote`)
    }

    res['mote_id']     = 0
    res['mote_id']    += (0xff & data[0]) << 8
    res['mote_id']    += (0xff & data[1])
    res['packet_no']   = (0xff & data[2])
    res['packet_type'] = (0xff & data[3])
    res['packet_len']  = 0
    res['packet_len'] += (0xff & data[4]) << 8
    res['packet_len'] += (0xff & data[5])

    let pos = 6
    if (res['packet_type'] === PacketType.ERROR) {
        let err_fields = protocol_spec[name_in_spec[PacketType.ERROR]]
        // extract error fields from packet
        for (var f in err_fields) {
            res[f] = 0
            let field_len = err_fields[f]

            if (data.length < pos + field_len) {
                logger.verbose(
                    `@protocol ${data} with length less than expected (${pos+field_len})`
                )
                throw new ProtocolError(`corrupted data received from mote`)
            }

            for (var i = 0; i < field_len; i++) {
                res[f] += ((data[pos + i] & 0xff) << (8*(field_len-i-1)))
            }
            pos += field_len
        }
    }

    return res
}
