import * as Protocol from './protocol'
import * as Redis    from 'redis'
import * as Bluebird from 'bluebird'

import { loggers }       from '../logger'

const config = require('../config.json')
const logger = loggers.mote_comm

Bluebird.promisifyAll(Redis.RedisClient.prototype)
Bluebird.promisifyAll(Redis.Multi.prototype)
const redis_client = Redis.createClient()

export { ProtocolError } from './protocol'
export class MoteCommError {
    constructor (public message: string) { }
}


/**
 * Send a command to a mote
 *
 * @param mote_id
 * @param data
 * @param packet_type
 *
 * @throws MoteCommError
 */
export async function send 
(mote_id: number, data: object, packet_type: Protocol.PacketType): Promise<any> {

    data['msg_type'] = Protocol.name_in_spec[packet_type];
    data['mote_id']  = mote_id;
    
    await redis_client.publishAsync(`mote_conn:${mote_id}`, JSON.stringify(data))
}
