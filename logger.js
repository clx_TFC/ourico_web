const winston = require('winston')


/** Each module will have its own logger, with its own output file */
module.exports.loggers = {
    db        : null,
    web       : null,
    main      : null,
    mote_comm : null
}


/**
 * Initialize the logging utility
 */
module.exports.setup = () => {

    var console_target = new winston.transports.Console()
    var print_format   = winston.format.printf(i => {
        return `${i.timestamp} [${i.label}] ${i.level} ${i.message}`
    })


    // logger for db module
    module.exports.loggers.db = winston.createLogger({
        transports: [
            console_target
        ],
        format: winston.format.combine(winston.format.combine(
            winston.format.label({ label: 'db' }),
            winston.format.colorize(),
            winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
            print_format
        )),
        level: 'debug'
    })
    // logger for web_api module
    module.exports.loggers.web = winston.createLogger({
        transports: [
            console_target
        ],
        format: winston.format.combine(winston.format.combine(
            winston.format.label({ label: 'web' }),
            winston.format.colorize(),
            winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
            print_format
        )),
        level: 'debug'
    })
    // logger for the main file
    module.exports.loggers.main = winston.createLogger({
        transports: [
            console_target
        ],
        format: winston.format.combine(winston.format.combine(
            winston.format.label({ label: 'main' }),
            winston.format.colorize(),
            winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
            print_format
        )),
        level: 'debug'
    })
    // logger for the mote_comm module
    module.exports.loggers.mote_comm = winston.createLogger({
        transports: [
            console_target
        ],
        format: winston.format.combine(winston.format.combine(
            winston.format.label({ label: 'mote_comm' }),
            winston.format.colorize(),
            winston.format.timestamp({ format: 'DD-MM-YYYY HH:mm:ss' }),
            print_format
        )),
        level: 'debug'
    })

}

// module.exports.loggers = loggers
