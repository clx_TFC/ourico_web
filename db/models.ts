import {

    BaseEntity,
    Column,
    Entity,
    JoinColumn,
    ManyToOne,
    OneToMany,
    PrimaryColumn,
    PrimaryGeneratedColumn,
    Generated,
    SelectQueryBuilder

} from "typeorm"

import { DataResource } from '../web/api/data_resources'

import { Query, Filter } from '../web/api/query'
import { ClientAuthInfo } from '../web/auth'


export interface Model {

    /**
     * fields that clients can not access
     */
    // static hidden_fields: string[]

    /**
     * models that can be joined with this
     * name in field (resource_name) => model
     * TODO: should get direcly from model metadata
     */
    // static related_models: object

    /**
     * fields that clients can access
     */
    // static exposed_fields: string[]

    /**
     * returns a list of objects read
     */
    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>

    /**
     * Execute subqueries inside a query
     * NOTE: should only be called from inside the models
     */
    __sub_read: <Entity> (
        query: Query,
        query_builder: SelectQueryBuilder<Entity>,
        first: boolean,
        visited: string[],
        used_alias: string
    ) => void

    /**
     * returns the number of inserted items or the ids of the new items
     */
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>

    /**
     * returns the number of updated items
     */
    update: (where: Filter[], new_data: object, client_info: ClientAuthInfo) => Promise<number>

    /**
     * returns the number of deleted items
     */
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data associated with a mote
@Entity("mote")
@DataResource('mote', Mote)
export class Mote extends BaseEntity {
    @PrimaryGeneratedColumn()
    id                      : number
    /// Name to describe de `Mote`
    @Column({ nullable: true, type: "text" })
    name                    : string
    /// Latitude of the `Mote`'s location
    @Column({ type: "real", nullable: true })
    location_lat            : number
    /// Longitude of the `Mote`'s location
    @Column({ type: "real", nullable: true })
    location_lng            : number
    /// Indicates if the `Mote` is active
    @Column()
    active                  : boolean
    /// The address currently being used by the `Mote` to receive
    /// connections
    @Column({ nullable: true, type: "text" })
    current_ip              : string
    /// Last time that the `Mote` sent a `MoteStatusReport`
    @Column({ type: "timestamp", nullable: true })
    last_status_report      : Date
    /// Maximum time that a `Mote` can take without sending `MoteStatusReport`s
    /// before being considered inactive
    @Column({ type: "time", nullable: true })
    max_time_with_no_report : Date
    /// Key used to encrypt the communication with the `Mote`
    @Column({ nullable: true, type: "text", select: false })
    crypto_key              : string
    /// Cryptographic algorithm used to encrypt the communication with the `Mote`
    @Column({ nullable: true, type: "text" })
    crypto_alg              : string
    /// Last nonce sent by the `Mote`
    @Column({ type: "int", nullable: true, select: false })
    crypto_last_nonce_in    : number
    /// Last nonce sent to the `Mote`
    @Column({ type: "int", nullable: true, select: false })
    crypto_last_nonce_out   : number
    /// Describes what the `Mote` is being used for
    @Column({ nullable: true, type: "text" })
    purpose_description     : string
    /// Any other data associated with the `Mote` that the application
    /// may need to store
    @Column({ nullable: true, type: "text" })
    other_data              : string


    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    @OneToMany(type => MoteError, error => error.mote)
    errors         :  MoteError[]
    @OneToMany(type => MoteConfiguration, conf => conf.mote)
    configurations :  MoteConfiguration[]
    @OneToMany(type => MoteStatusReport, sr => sr.mote)
    status_reports :  MoteStatusReport[]
    @OneToMany(type => MoteConnection, conn => conn.mote_here)
    conns_from_here:  MoteConnection[]
    @OneToMany(type => MoteConnection, conn => conn.mote_there)
    conns_to_here  :  MoteConnection[]

    @OneToMany(type => SensorUse, mote_use => mote_use.mote)
    sensors       :  SensorUse[]
    @OneToMany(type => ActuatorUse, act_use => act_use.mote)
    actuators     :  ActuatorUse[]
    @OneToMany(type => AlertUse, alert_use => alert_use.mote)
    alerts        :  AlertUse[]
    @OneToMany(type => TriggerUse, trigger_use => trigger_use.mote)
    triggers      :  TriggerUse[]

    @OneToMany(type => ClientMote, client => client.mote)
    clients    : ClientMote[]
    @OneToMany(type => Packet, packet => packet.mote)
    packets    : Packet[]

    static readonly table_name: string = 'mote'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
        query: Query,
        query_builder: SelectQueryBuilder<Entity>,
        first: boolean,
        visited: string[],
        used_alias: string
    ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Runtime errors sent from `Mote`s
@Entity("mote_error")
@DataResource('mote_error', MoteError)
export class MoteError extends BaseEntity {
    /// Date and time of the error
    @PrimaryColumn({ type: "timestamp" })
    date_time   : Date
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.errors, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote        : Mote
    @Column("int")
    mote_id     : number
    /// Integer code identifying the error
    @Column({ type: "int" })
    err_code    : number
    /// Message describing the error
    @Column({ type: "text" })
    err_message : string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'mote_error'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Defines a set of paramenters to coordinate the operation of the `Mote`
@Entity("mote_configuration")
@DataResource('mote_configuration', MoteConfiguration)
export class MoteConfiguration extends BaseEntity {
    /// Name of the configuration can be
    /// e.g. "power_saving" or "standard"
    @PrimaryColumn({ type: "text" })
    name                 : string
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.configurations, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote                 : Mote
    @Column("int")
    mote_id              : number
    /// Number of seconds the `Mote` should take between measurements
    @Column({ type: "int"})
    measurement_period   : number
    /// `Mote` should send back measurements every `send_data_period` minutes
    @Column({ type: "int" })
    send_data_period     : number
    /// Number of minutes the `Mote` should take
    /// between status reports(`MoteStatusReport`)
    @Column({ type: "int" })
    status_report_period : number
    /// Indicates if cryptography is enabled on the mote
    @Column({ type: "boolean" })
    use_crypto           : boolean
    /// Indicates if the mote is active
    @Column({ type: "boolean" })
    active               : boolean

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'mote_configuration'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data sent in a status report
@Entity("mote_status_report")
@DataResource('mote_status_report', MoteStatusReport)
export class MoteStatusReport extends BaseEntity {
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.status_reports, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote            : Mote
    @Column("int")
    mote_id         : number
    /// Time that the report data gathering started
    @PrimaryColumn({ type: "timestamp" })
    start_date_time : Date
    /// Time that the report data gathering ended
    @Column({ type: "timestamp" })
    end_date_time   : Date
    /// Number of packets transmitted during the report data gathering period
    @Column({ type: "int" })
    nr_tx_packets   : number
    /// Number of packets lost in transmission
    /// during the report data gathering period
    @Column({ type: "int" })
    nr_lost_packets : number
    /// Number of bytes transmitted during the report data gathering period
    @Column({ type: "int" })
    nr_tx_bytes     : number
    /// Number of bytes lost in transmission
    /// during the report data gathering period
    @Column({ type: "int" })
    nr_lost_bytes   : number


    static readonly table_name: string = 'mote_status_report'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// A connetion between `Mote`s
@Entity("mote_connection")
@DataResource('mote_connection', MoteConnection)
export class MoteConnection extends BaseEntity {
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.conns_from_here, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote_here  : Mote
    @Column()
    mote_id    : number
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.conns_to_here, { primary: true })
    @JoinColumn({ name: "other_mote", referencedColumnName: "id" })
    mote_there : Mote
    @Column()
    other_mote : number
    /// Connection direction
    /// Here_to_There, There_to_Here or Bi
    @PrimaryColumn()
    direction  : string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'mote_connection'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data associated with a sensor
@Entity("sensor")
@DataResource('sensor', Sensor)
export class Sensor extends BaseEntity {
    @PrimaryColumn({ type: "int" })
    id          : number
    /// Number of bytes produced by this sensor
    @Column({ type: "int" })
    output_size : number
    /// Name of the sensor
    @Column({ type: "text" })
    name         : string
    /// Describes the purpose of the `Sensor`
    @Column({ type: "text" })
    description  : string
    /// Indicates if the sensor reads data asynchronously
    /// e.g. a motion sensor is asynchronous
    @Column({ type: "boolean" })
    async        : boolean

    @OneToMany(type => Parameter, sp => sp.sensor)
    parameters   : Parameter[]
    @OneToMany(type => SensorUse, su => su.sensor)
    uses         : SensorUse[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'sensor'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data associated with a parameter
/// #Examples
/// ```
/// Parameter {
///   id: 12
///   name: "Temperature in Celsius"
///   units: "ºC"
///   description: ""
/// }
/// ```
@Entity("parameter")
@DataResource('parameter', Parameter)
export class Parameter extends BaseEntity {
    @PrimaryColumn({ type: "int" })
    id          : number
    /// Name of the parameter
    @Column({ type: "text" })
    name        : string
    /// Symbol indicating the `Parameter`'s units
    @Column({ type: "text" })
    units       : string
    /// Description of the parameter
    @Column({ type: "text" })
    description : string
    /// Type of values of this `Parameter`
    /// Possible values are integer number string or boolean
    @Column({ name: "type", type: "text" })
    type_       : string
    /// Size of the parameter in bytes
    @Column()
    size        : number
    /// Position in the sensor output format
    @Column()
    position    : number
    /// References `Sensor::id`
    @ManyToOne(type => Sensor, sensor => sensor.parameters)
    @JoinColumn({ name: "sensor_id", referencedColumnName: "id" })
    sensor          : Sensor
    @Column("int")
    sensor_id       : number

    @OneToMany(type => Measurement, meas => meas.parameter)
    measurements: Measurement[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'parameter'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Indicates that a `Sensor` is being used in a `Mote`.
@Entity("sensor_use")
@DataResource('sensor_use', SensorUse)
export class SensorUse extends BaseEntity {
    /// The same `Sensor` could be use many times in a single mote
    /// this value destinguishes between `Sensor`s of the same type
    /// in a single `Mote`.
    @PrimaryColumn({ type: "int" })
    num             : number
    /// References `Sensor::id`
    @ManyToOne(type => Sensor, sensor => sensor.uses, { primary: true })
    @JoinColumn({ name: "sensor_id", referencedColumnName: "id" })
    sensor          : Sensor
    @Column("int")
    sensor_id       : number
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.sensors, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote            : Mote
    @Column("int")
    mote_id         : number
    /// Identifies the interface where the component is installed.
    /// Must be different among components SensorUse's and ActuatorUse's
    /// in the same mote
    @Column("int")
    interface_num   : number
    /// Name of the interface where the component is installed
    @Column()
    interface_name  : string
    /// To descibe the function that the `Sensor` performs
    /// in the mote
    @Column()
    use_description : string
    /// Indicates if the `Sensor` is currently active in the `Mote`
    @Column()
    active          : boolean
    /// last time it was activated
    @Column("timestamp with time zone")
    activated_at    : Date
    /// last time it was deactivated
    @Column("timestamp with time zone")
    deactivated_at  : Date

    @OneToMany(type => Measurement, measurement => measurement.sensor_use)
    measurements    : Measurement[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'sensor_use'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/** Data associated with a mesasurement */
@Entity("measurement")
@DataResource('measurement', Measurement)
export class Measurement extends BaseEntity {
    /// References `Parameter::id`
    @ManyToOne(type => Parameter, param => param.measurements, { primary: true })
    @JoinColumn({ name: "parameter_id", referencedColumnName: "id" })
    parameter      : Parameter
    /// References `SensorUse`
    @ManyToOne(type => SensorUse, su => su.measurements, { primary: true })
    @JoinColumn([
        { name: "sensor_use_num", referencedColumnName: "num"       },
        { name: "sensor_id",      referencedColumnName: "sensor_id" },
        { name: "mote_id",        referencedColumnName: "mote_id"   }
    ])
    sensor_use     : SensorUse
    /// Date and time of the measurement
    @PrimaryColumn("timestamp")
    date_time      : Date
    /// string representation of the bytes of the measurement
    @Column()
    value          : string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string   = 'measurement'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data associated with an alert
@Entity("alert")
@DataResource('alert', Alert)
export class Alert extends BaseEntity {
    /// Identifier
    @PrimaryColumn("int")
    id          : number
    /// Name of the `Alert`
    @Column()
    name        : string
    /// Description of the `Alert`
    @Column()
    description : string
    /// Can be NORMAL WARNING or CRITICAL
    @Column()
    level       : string

    @OneToMany(type => AlertUse, au => au.alert)
    uses: AlertUse[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'alert'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Indicates that an `Alert` is being used in a `Mote`
@Entity("alert_use")
@DataResource('alert_use', AlertUse)
export class AlertUse extends BaseEntity {
    /// References `Alert::id`
    @ManyToOne(type => Alert, alert => alert.uses, { primary: true })
    @JoinColumn({ name: "alert_id", referencedColumnName: "id" })
    alert   : Alert
    @PrimaryColumn("int")
    alert_id: number
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.alerts, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote    : Mote
    @PrimaryColumn("int")
    mote_id : number

    @Column()
    active        : boolean
    /// last time it was activated
    @Column("timestamp with time zone")
    activated_at  : Date
    /// last time it was deactivated
    @Column("timestamp with time zone")
    deactivated_at: Date

    @OneToMany(type => AlertOccurrence, ao => ao.alert_use)
    occurrences:  AlertOccurrence[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'alert_use'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// An occurrence of an alert
@Entity("alert_occurrence")
@DataResource('alert_occurrence', AlertOccurrence)
export class AlertOccurrence extends BaseEntity {
    /// Refenrences `AlertUse`
    @ManyToOne(type => AlertUse, au => au.occurrences, { primary: true })
    @JoinColumn([
        { name: "alert_id", referencedColumnName: "alert_id" },
        { name: "mote_id",  referencedColumnName: "mote_id"  }
    ])
    alert_use : AlertUse
    /// Date and time of the alert occurrence
    /// Every `Measurement` with this
    /// `date_time` from the `Mote` referred by `mote_id`
    /// are associated with this `AlertOccurrence`
    @PrimaryColumn({ type: "timestamp" })
    date_time : Date

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'alert_occurrence'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data associated with an trigger
@Entity("trigger")
@DataResource('trigger', Trigger)
export class Trigger extends BaseEntity {
    /// Identifier
    @PrimaryColumn("int")
    id          : number
    /// Name of the `Trigger`
    @Column()
    name        : string
    /// Description of the `Trigger`
    @Column()
    description : string
    
    @OneToMany(type => TriggerUse, tu => tu.trigger)
    uses: TriggerUse[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'trigger'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Indicates that a `Trigger` is being used in a `Mote`
@Entity("trigger_use")
@DataResource('trigger_use', TriggerUse)
export class TriggerUse extends BaseEntity {
    /// References `Trigger::id`
    @ManyToOne(type => Trigger, trigger => trigger.uses, { primary: true })
    @JoinColumn({ name: "trigger_id", referencedColumnName: "id" })
    trigger    : Trigger
    @PrimaryColumn("int")
    trigger_id : number
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.triggers, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote       : Mote
    @PrimaryColumn("int")
    mote_id    : number

    @Column()
    active          : boolean
    /// last time it was activated
    @Column("timestamp with time zone")
    activated_at    : Date
    /// last time it was deactivated
    @Column("timestamp with time zone")
    deactivated_at  : Date

    @OneToMany(type => TriggerOccurrence, to => to.trigger_use)
    occurrences:  TriggerOccurrence[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'trigger_use'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// An occurrence of an trigger
@Entity("trigger_occurrence")
@DataResource('trigger_occurrence', TriggerOccurrence)
export class TriggerOccurrence extends BaseEntity {
    /// Refenrences `TriggerUse`
    @ManyToOne(type => TriggerUse, tu => tu.occurrences, { primary: true })
    @JoinColumn([
        { name: "trigger_id", referencedColumnName: "trigger_id" },
        { name: "mote_id",    referencedColumnName: "mote_id"    }
    ])
    trigger_use: TriggerUse
    /// Date and time of the trigger occurrence
    /// Every `Measurement` and `ActionResult` with this
    /// `date_time` from the `Mote` referred to by `mote_id`
    /// are associated withthis `TriggerOccurrence`
    @PrimaryColumn({ type: "timestamp" })
    date_time  : Date

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'trigger_occurrence'
    static hidden_fields : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models: object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data associated with an actuator
@Entity("actuator")
@DataResource('actuator', Actuator)
export class Actuator extends BaseEntity {
    @PrimaryColumn({ type: "int" })
    id          : number
    /// Describes the operation of the `Actuator`
    @Column()
    description : string
    /// Name the `Actuator`
    @Column()
    name        : string

    @OneToMany(type => Action, action => action.actuator)
    actions   : Action[]
    @OneToMany(type => ActuatorUse, au => au.actuator)
    uses      : ActuatorUse[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'actuator'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Indicates a possible action that an `Actuator` can perform.
@Entity("action")
@DataResource('action', Action)
export class Action extends BaseEntity {
    @PrimaryColumn({ type: "int" })
    num         : number
    /// Foreign key that references `Actuator::id`
    @ManyToOne(type => Actuator, actuator => actuator.actions, { primary: true })
    @JoinColumn({ name: "actuator_id", referencedColumnName: "id" })
    actuator    : Actuator
    @Column("int")
    actuator_id    : number
    @Column()
    name        : string
    /// The byte sequence that is passed to the actuator
    /// to perform this action
    @Column("bigint")
    signal      : number
    /// Describes the results of the `Action`
    @Column()
    description : string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number

    @OneToMany(type => ActionResult, ar => ar.action)
    results: ActionResult[]


    static readonly table_name: string = 'action'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Indicates that a `Actuator` is being used in a `Mote`.
@Entity("actuator_use")
@DataResource('actuator_use', ActuatorUse)
export class ActuatorUse extends BaseEntity {
    /// The same `Actuator` could be use many times in a single mote
    /// this value destinguishes between `Actuator`s of the same type
    /// in a single mote.
    @PrimaryColumn({ type: "int" })
    num             : number
    /// References `Actuator::id`
    @ManyToOne(type => Actuator, actuator => actuator.uses, { primary: true })
    @JoinColumn({ name: "actuator_id", referencedColumnName: "id" })
    actuator       : Actuator
    @Column("int")
    actuator_id    : number
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.actuators, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote            : Mote
    @Column("int")
    mote_id         : number
    /// Identifies the interface where the component is installed.
    /// Must be different among components in the same mote
    @Column("int")
    interface_num   : number
    /// Name of the interface where the component is installed
    @Column()
    interface_name  : string
    /// To descibe the function that the `Actuator` performs
    /// in the mote
    @Column()
    use_description : string
    /// Indicates if the `Actuator` is currently active in the `Mote`
    @Column()
    active          : boolean
    /// last time it was activated
    @Column("timestamp with time zone")
    activated_at    : Date
    /// last time it was deactivated
    @Column("timestamp with time zone")
    deactivated_at  : Date

    @OneToMany(type => ActionResult, ar => ar.actuator_use)
    results: ActionResult[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'actuator_use'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Result of an `Action` execution
@Entity("action_result")
@DataResource('action_result', ActionResult)
export class ActionResult extends BaseEntity {
    /// The `Action` that this result refers to
    @ManyToOne(type => Action, ac => ac.results, { primary: true })
    @JoinColumn([
        { name: "action_num", referencedColumnName: "num" },
        { name: "actuator_id", referencedColumnName: "actuator_id" }
    ])
    action          : Action // Action
    /// References `ActuatorUse`
    @ManyToOne(type => ActuatorUse, au => au.results, { primary: true })
    @JoinColumn([
        { name: "actuator_use_num", referencedColumnName: "num"         },
        { name: "actuator_id",      referencedColumnName: "actuator_id" },
        { name: "mote_id",          referencedColumnName: "mote_id"     }
    ])
    actuator_use     : ActuatorUse
    /// Data and time of the action result
    @PrimaryColumn("timestamp")
    date_time        : Date
    /// Result of the action
    @Column("int")
    result           : number

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'action_result'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// Data associated with a user
@Entity("client")
@DataResource('client', Client)
export class Client extends BaseEntity {
    /// Should be a crypto secure random 60 characters string with
    /// first character being a letter
    @PrimaryColumn()
    id                    : string
    @Column()
    name                  : string
    @Column()
    username              : string
    @Column()
    password              : string
    @Column()
    email                 : string
    //// Only admin user can create new items (motes sensors extension packs etc)
    @Column()
    admin                 : boolean
    /// The `Client` that created this one
    /// Will be None if this user is an admin
    /// It must be an admin
    @ManyToOne(type => Client, cli => cli.owned_users)
    @JoinColumn({ name: "client_id", referencedColumnName: "id" })
    owner                 : Client
    @Column()
    client_id             : string // owner's id
    /// Indicates if user can write and create items.
    /// User will be able to create/change triggers alerts and mote configurations
    @Column()
    write_capability      : boolean
    /// Indicates if user can execute computing operations (scritps etc)
    @Column()
    compute_capability    : boolean
    /// Indicates if user can receive numbertime events from motes
    @Column()
    rt_events_capability  : boolean
    /// To authenticate a user that has logged in
    @Column({ nullable: true, select: false })
    access_token          : string


    @OneToMany(type => ClientMote, um => um.client)
    motes          : ClientMote[]
    @OneToMany(type => ApiClient, um => um.owner)
    api_clients    : ApiClient[]
    @OneToMany(type => UserOperationsLog, log => log.owner)
    logs           : UserOperationsLog[]
    @OneToMany(type => ExtensionPack, pack => pack.owner)
    extension_packs: ExtensionPack[]
    @OneToMany(type => Client, cli => cli.owner)
    owned_users    : Client[]

    /// the group thath this item belongs to
    @Generated()
    @Column()
    group_id : number


    static readonly table_name: string = 'client'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static updatable_fields   : string[] = []
    static related_models     : object   = {}

    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}



/// Association between a `Client` and a `Mote`
@Entity("user_mote")
@DataResource('client_mote', ClientMote)
export class ClientMote extends BaseEntity {
    /// References `Client::id`
    @ManyToOne(type => Client, client => client.motes, { primary: true })
    @JoinColumn({ name: "client_id", referencedColumnName: "id" })
    client                 : Client
    /// References `Mote::id`
    @ManyToOne(type => Mote, mote => mote.clients, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote                   : number
    /// Indicates if the user[`Client`] should receive
    /// notifications of `Alert`s from the `Mote`
    @Column()
    receive_alerts         : boolean
    /// Indicates if the user[`Client`] should receive
    /// notifications of `Alert`s from the `Mote`
    @Column()
    receive_triggers       : boolean
    /// Indicates if the user[`Client`] should receive
    /// notifications of `Action` results from the `Mote`
    @Column()
    receive_action_results : boolean
    /// Indicates if the user[`Client`] should receive
    /// notifications of `Sensor` measurements from the `Mote`
    @Column()
    receive_measurements   : boolean
    /// Indicates if the user[`Client`] should receive
    /// notifications of errors[`MoteError`] from the `Mote`
    @Column()
    receive_errors         : boolean

    /// the group thath this item belons to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'user_mote'
    static hidden_fields      : string[] = []
    static exposed_fields     :   string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}



/// Data associated with a client of the WEB API
@Entity("api_client")
@DataResource('api_client', ApiClient)
export class ApiClient extends BaseEntity {
    @PrimaryColumn()
    id                     : string
    /// id of user whose resources api will use
    /// References `Client::id`
    @ManyToOne(type => Client, client => client.api_clients)
    @JoinColumn({ name: "client_id", referencedColumnName: "id" })
    owner                  : Client
    @Column()
    client_id              : string // owner's id
    /// Token used to identify the client[`ApiClient`]
    @Column()
    access_token           : string
    /// Date when this permission expires
    /// If it is `None` the permission does not expire
    @Column("timestamp with time zone")
    token_expire_date      : Date
    /// Token to be used to update authorization after `access_token` expires
    @Column()
    token_update_token     : string
    /// Indicates if client can write and create items.
    /// It is limited by what a non-admin user can write
    @Column()
    write_capability      : boolean
    /// Indicates if client can execute computing operations (scritps etc)
    @Column()
    compute_capability    : boolean
    /// Indicates if client can receive realtime events from motes
    @Column()
    rt_events_capability  : boolean
    /// Address that has to be in the origin of this client's[`ApiClient`]
    /// requests.
    /// If it is "any" every address is allowed
    @Column()
    allowed_origin_address : string
    @Column()
    name                   : string

    // @OneToMany(type => ApiClientMote, aum => aum.api_client)
    // motes      : ApiClientMote[]
    @OneToMany(type => ApiOperationsLog, log => log.owner)
    logs       : ApiOperationsLog[]

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'api_client'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static updatable_fields   : string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


// /// Indicates that a `Mote` can be accessed by an `ApiClient`
// @Entity("api_use_mote")
// @DataResource('api_client_mote', ApiClientMote)
// export class ApiClientMote {
//     /// References `ApiClient::id`
//     @ManyToOne(type => ApiClient, api_client => api_client.motes, { primary: true })
//     @JoinColumn({ name: "api_client_id", referencedColumnName: "id" })
//     api_client             : ApiClient
//     /// References `Mote::id`
//     @ManyToOne(type => Mote, mote => mote.api_clients, { primary: true })
//     @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
//     mote                   : Mote
//     // /// "R" `ApiClient` can only read data from the `Mote`
//     // /// "RW" `ApiClient` can only read and write data from the `Mote`
//     // permission             : string
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Alert`s from the `Mote`
//     @Column()
//     receive_alerts         : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Alert`s from the `Mote`
//     @Column()
//     receive_triggers       : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Action` results from the `Mote`
//     @Column()
//     receive_action_results : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of `Sensor` measurements from the `Mote`
//     @Column()
//     receive_measurements   : boolean
//     /// Indicates if the client[`ApiClient`] should receive
//     /// notifications of errors[`MoteError`] from the `Mote`
//     @Column()
//     receive_errors         : boolean
//
//
//     static readonly table_name: string = 'api_use_mote'
//     hidden_fields : string[] = []
//     related_models: object = {
//         'api_client': ['api_client', ApiClient],
//         'mote'      : ['mote', Mote]
//     }
//
//     read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
//     __sub_read: <Entity>(
  //     query: Query,
  //     query_builder: SelectQueryBuilder<Entity>,
  //     first: boolean,
  //     visited: string[],
        // used_alias: string
  // ) => void
//     write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
//     update: (
//         where: Filter[],
//         new_data: object  ,
//         client_id: string,
//         client_type: ClientType
//     ) => Promise<number>
//     delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<number>
//
// }


/// Data associated with an extension pack.
/// Extension packs are added by clients to perform operations on
/// `Mote`'s datas in the server side.
@Entity("extension_pack")
@DataResource('extension_pack', ExtensionPack)
export class ExtensionPack extends BaseEntity {
    @PrimaryGeneratedColumn()
    id                      : number
    /// The user[`Client`] who owne this `ExtensionPack`
    @ManyToOne(type => Client, client => client.extension_packs)
    @JoinColumn({ name: "client_id", referencedColumnName: "id" })
    owner                   : Client
    @Column()
    client_id               : string // owner's id
    /// Programming Language used by the `ExtensionPack`
    @Column()
    prog_language           : string
    /// Used to update the pack direcly if necessary
    @Column()
    code_repo_url           : string
    /// Address used by components of the system to communicate with
    /// the `ExtensionPack`
    @Column()
    container_address       : string
    /// Indicates if the `ExtensionPack` should receive
    /// notifications of `Alert`s from the `Mote`s its owner owns
    @Column()
    receive_alerts         : boolean
    /// Indicates if the `ExtensionPack` should receive
    /// notifications of `Alert`s from the `Mote`s its owner owns
    @Column()
    receive_triggers       : boolean
    /// Indicates if the `ExtensionPack` should receive
    /// notifications of `Action` results from the `Mote`s its owner owns
    @Column()
    receive_action_results : boolean
    /// Indicates if the `ExtensionPack` should receive
    /// notifications of `Sensor` measurements from the `Mote`s its owner owns
    @Column()
    receive_measurements   : boolean
    /// Indicates if the `ExtensionPack` should receive
    /// notifications of errors[`MoteError`] from the `Mote`s its owner owns
    @Column()
    receive_errors         : boolean
    /// Indicates if the extension pack is currently active
    @Column()
    active                 : boolean
    /// The container where the extension is running
    @Column()
    container_id           : string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number

    @OneToMany(type => ExtensionPackLog, log => log.owner)
    logs: ExtensionPackLog[]


    static readonly table_name: string   = 'extension_pack'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// To log operations performed by an `ApiClient`
@Entity("api_operations_log")
@DataResource('api_operations_log', ApiOperationsLog)
export class ApiOperationsLog extends BaseEntity {
    /// References `ApiClient::id`
    @ManyToOne(type => ApiClient, api_client => api_client.logs, { primary: true })
    @JoinColumn({ name: "api_client_id", referencedColumnName: "id" })
    owner         : ApiClient
    @Column()
    api_client_id : string // owner's id
    /// Date and time when the operation was perfomed
    @PrimaryColumn("timestamp with time zone")
    date_time     : Date
    /// The HTTP query used to perform the operation
    /// with all the body data
    @Column()
    query         : string
    /// Indicates the severity level of the log
    /// Defined levels are info, warning, error, unauthorized_behaviour
    @Column()
    level         : string
    /// Name of the operation executed
    @Column()
    operation     : string
    /// Log message
    @Column()
    message       : string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'api_operations_log'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>

}


/// To log operations performed by an `Client`
@Entity("user_operations_log")
@DataResource('user_operations_log', UserOperationsLog)
export class UserOperationsLog extends BaseEntity {
    /// References `Client::id`
    @ManyToOne(type => Client, client => client.logs, { primary: true })
    @JoinColumn({ name: "client_id", referencedColumnName: "id" })
    owner     : Client
    @Column()
    client_id : string // owner's id
    /// Date and time when the operation was perfomed
    @PrimaryColumn("timestamp with time zone")
    date_time : Date
    /// The HTTP query used to perform the operation
    /// with all the body data
    @Column()
    query     : string
    /// Indicates the severity level of the log
    /// Defined levels are info, warning, error
    @Column()
    level     : string
    /// Name of the operation executed
    @Column()
    operation : string
    /// Log message
    @Column()
    message   : string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number
    


    static readonly table_name: string = 'user_operations_log'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>
}


/// To log operations performed by an `ExtensionPack`
@Entity("extension_pack_log")
@DataResource('extension_pack_log', ExtensionPackLog)
export class ExtensionPackLog extends BaseEntity {
    /// References `ExtensionPack::id`
    @ManyToOne(type => ExtensionPack, ext_pack => ext_pack.logs, { primary: true })
    @JoinColumn({ name: "extension_pack_id", referencedColumnName: "id" })
    owner: ExtensionPack
    @Column("bigint")
    extension_pack_id: number // owner's id
    /// Date and time when the operation was perfomed
    @PrimaryColumn("timestamp with time zone")
    date_time: Date
    /// Indicates the severity level of the log
    /// Defined levels are info, warning, error
    @Column()
    level: string
    /// Log message
    @Column()
    message: string

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id: number


    static readonly table_name: string   = 'extension_pack_log'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read: (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
        query: Query,
        query_builder: SelectQueryBuilder<Entity>,
        first: boolean,
        visited: string[],
        used_alias: string
    ) => void
    write: (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>
}

@Entity("packet")
@DataResource("packet", Packet)
export class Packet extends BaseEntity {
    // date time that the packet was received
    @PrimaryColumn("timestamp")
    date_time  : Date
    // mote that sent the packet
    @ManyToOne(type => Mote, mote => mote.packets, { primary: true })
    @JoinColumn({ name: "mote_id", referencedColumnName: "id" })
    mote       : Mote
    @Column("int")
    packet_type: number
    @Column("int")
    length     : number
    @Column("int")
    packet_nr  : number

    /// the group thath this item belongs to
    @Column({ select: false })
    group_id : number


    static readonly table_name: string = 'packet'
    static hidden_fields      : string[] = []
    static exposed_fields     : string[] = []
    static non_writable_fields: string[] = []
    static related_models     : object   = {}


    read  : (query: Query, client_info: ClientAuthInfo) => Promise<object[]>
    __sub_read: <Entity>(
      query: Query,
      query_builder: SelectQueryBuilder<Entity>,
      first: boolean,
      visited: string[],
      used_alias: string
  ) => void
    write : (data: object[], client_info: ClientAuthInfo) => Promise<any[]>
    update: (
        where: Filter[],
        new_data: object  ,
        client_info: ClientAuthInfo
    ) => Promise<number>
    delete: (where: Filter[], client_info: ClientAuthInfo) => Promise<void>
}



import './models_ops/mote'
import './models_ops/mote_error'
import './models_ops/mote_configuration'
import './models_ops/mote_status_report'
import './models_ops/mote_connection'
import './models_ops/parameter'
import './models_ops/action_result'
import './models_ops/action'
import './models_ops/actuator_use'
import './models_ops/actuator'
import './models_ops/alert_occurrence'
import './models_ops/alert_use'
import './models_ops/alert'
import './models_ops/api_client'
import './models_ops/api_operations_log'
import './models_ops/client_mote'
import './models_ops/client'
import './models_ops/extension_pack'
import './models_ops/extension_pack_log'
import './models_ops/measurement'
import './models_ops/sensor_use'
import './models_ops/sensor'
import './models_ops/trigger_occurrence'
import './models_ops/trigger'
import './models_ops/trigger_use'
import './models_ops/user_operations_log'
import './models_ops/packet'
