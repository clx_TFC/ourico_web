import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.MoteConnection.hidden_fields = [ 'group_id' ]

Models.MoteConnection.related_models = {
    'mote_here' : Models.Mote,
    'mote_there': Models.Mote
}

Models.MoteConnection.exposed_fields = [
    'direction',
    'mote_id',
    'other_mote'
]


Models.MoteConnection.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {
    logger.debug('@mote_connection.__sub_read visited')

    return Common.default__sub_read(
        Models.MoteConnection,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.MoteConnection.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.MoteConnection)
        .createQueryBuilder(Models.MoteConnection.table_name)
        .where(`${Models.MoteConnection.table_name}.group_id = :gid`, { gid: client_info.group_id })


    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.MoteConnection.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.MoteConnection)

    Common.default_write(
        Models.MoteConnection,
        'mote_connection',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.MoteConnection.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.MoteConnection)
        .where(`${Models.MoteConnection.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.MoteConnection,
        'mote_connection',
        'mote_connection',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.MoteConnection.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.MoteConnection)
        .where(`${Models.MoteConnection.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.MoteConnection,
        'mote_connection',
        'mote_connection',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
