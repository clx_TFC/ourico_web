import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Bcrypt      from 'bcrypt'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'

const config = require('../../config.json')


const logger = loggers.db

Models.Client.hidden_fields = [ 'group_id', 'current_session_token' ]

Models.Client.related_models = {
    'motes'          : Models.ClientMote,
    'api_clients'    : Models.ApiClient,
    'logs'           : Models.UserOperationsLog,
    'extension_packs': Models.ExtensionPack,
    'owned_users'    : Models.Client,
    'owner'          : Models.Client
}

Models.Client.exposed_fields = [
    'email',
    'name',
    'id',
    'admin',
    'client_id',
    'username',
    'password',
    'write_capability',
    'compute_capability',
    'rt_events_capability'
]

Models.Client.non_writable_fields = [
    'owner',
    'client_id',
    'id',
    'write_capability',
    'compute_capability',
    'rt_events_capability'
]

Models.Client.updatable_fields = [
    'write_capability',
    'compute_capability',
    'rt_events_capability',
    'password'
]


Models.Client.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {
    return Common.default__sub_read(
        Models.Client,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.Client.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.Client)
        .createQueryBuilder(Models.Client.table_name)
        .where(`${Models.Client.table_name}.group_id = :gid`, { gid: client_info.group_id })

    // check if current user is admin
    let current_user = await getRepository(Models.Client)
        .createQueryBuilder(Models.Client.table_name)
        .where(`${Models.Client.table_name}.id = :id`, { id: client_info.client_id })
        .getOne()

    if (!current_user.admin) {
        // return only this client's data if he isnt an admin
        query_builder
            .where(`${Models.Client.table_name}.id = :xxx`, { xxx: client_info.client_id })
    }

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.Client.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    Common.admin_op_guard(client_info, 'create clients')

    // check that username is unique
    let usernames = await getRepository(Models.Client)
        .find({ username: data['username'] })
    if (usernames.length != 0)
        throw new Common.DbOpError(`username ${data['username']} is not available`)

    // check that email is unique
    let emails = await getRepository(Models.Client)
        .find({ email: data['email'] })
    if (emails.length != 0)
        throw new Common.DbOpError(`email ${data['email']} is already being used`)


    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.Client)

    Common.default_write(
        Models.Client,
        'client',
        data,
        query_builder
    )

    data['id']        = await Auth.rand_string(20)
    data['group_id']  = client_info.group_id
    data['client_id'] = client_info.client_id

    let capabilities: Auth.Capabilities[] = []
    capabilities.push(Auth.Capabilities.Read) // default
    if (data['write_capability'])
        capabilities.push(Auth.Capabilities.Write)
    if (data['rt_events_capability'])
        capabilities.push(Auth.Capabilities.RtEvents)
    if (data['compute_capability'])
        capabilities.push(Auth.Capabilities.Compute)


    let jwt_payload = new Auth.ClientAuthInfo(
        data['id'], Auth.ClientType.User, data['group_id'], null, capabilities
    )

    data['access_token'] = await Auth.generate_token(jwt_payload)

    // validate password
    try {
        Auth.validate_password(data['password'])
    } catch (e) {
        throw new Common.DbOpError(e)
    }
    // save hash password
    data['password'] = await Bcrypt.hash(data['password'], config.bcrypt_salt_rounds)


    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.Client.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let current_user = await getRepository(Models.Client)
        .createQueryBuilder(Models.Client.table_name)
        .where(`${Models.Client.table_name}.id = :id`, { id: client_info.client_id })
        .getOne()

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.Client)
        .where(`${Models.Client.table_name}.group_id = :gid`, { gid: client_info.group_id })

    if (!current_user.admin) {
        // non admin client can change only his data
        query_builder
            .where(`${Models.Client.table_name}.id = :xxx`, { xxx: client_info.client_id })
    }

    // check that username is unique
    let usernames = await getRepository(Models.Client)
        .find({ username: new_data['username'] })
    if (usernames.length != 0)
        throw new Common.DbOpError(`username ${new_data['username']} is not available`)

    // check that email is unique
    let emails = await getRepository(Models.Client)
        .find({ email: new_data['email'] })
    if (emails.length != 0)
        throw new Common.DbOpError(`email ${new_data['email']} is already being used`)


    Common.default_update(
        Models.Client,
        'client',
        'client',
        new_data,
        where,
        query_builder
    )

    if (new_data.hasOwnProperty('password')) {
        // validate password
        try {
            Auth.validate_password(new_data['password'])
        } catch (e) {
            throw new Common.DbOpError(e)
        }
        // save hash password
        new_data['password'] =
            await Bcrypt.hash(new_data['password'], config.bcrypt_salt_rounds)
    }

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.Client.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {
    throw new Common.DbOpError(
        `clients must be deleted from api.control/delete_client endpoint`
    )
}
