import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.Actuator.hidden_fields = [ 'group_id' ]

Models.Actuator.related_models = {
    'actions': Models.Action,
    'uses'   : Models.ActuatorUse
}

Models.Actuator.exposed_fields = [
    'id',
    'description',
    'name'
]

Models.Actuator.non_writable_fields = [ 'id' ]


Models.Actuator.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.Actuator,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.Actuator.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.Actuator)
        .createQueryBuilder(Models.Actuator.table_name)
        .where(`${Models.Actuator.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.Actuator.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.Actuator)

    Common.default_write(
        Models.Actuator,
        'actuator',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.Actuator.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.Actuator)
        .where(`${Models.Actuator.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.Actuator,
        'actuator',
        'actuator',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.Actuator.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.Actuator)
        .where(`${Models.Actuator.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.Actuator,
        'actuator',
        'actuator',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
