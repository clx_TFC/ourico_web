import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.SensorUse.hidden_fields = [ 'group_id' ]

Models.SensorUse.related_models = {
    'measurements': Models.Measurement,
    'sensor'      : Models.Sensor,
    'mote'        : Models.Mote
}

Models.SensorUse.exposed_fields = [
    'mote_id',
    'num',
    'interface_num',
    'interface_name',
    'use_description',
    'active',
    'activated_at',
    'deactivated_at',
    'mote_id',
    'sensor_id'
]

Models.SensorUse.non_writable_fields = [
    'num',
    'active',
    'activated_at',
    'deactivated_at'
]


Models.SensorUse.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.SensorUse,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.SensorUse.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.SensorUse)
        .createQueryBuilder(Models.SensorUse.table_name)
        .where(`${Models.SensorUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.SensorUse.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.SensorUse)

    Common.default_write(
        Models.SensorUse,
        'sensor_use',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.SensorUse.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.SensorUse)
        .where(`${Models.SensorUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.SensorUse,
        'sensor_use',
        'sensor_use',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.SensorUse.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.SensorUse)
        .where(`${Models.SensorUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.SensorUse,
        'sensor_use',
        'sensor_use',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
