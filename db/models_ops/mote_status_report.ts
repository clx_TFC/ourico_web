import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'
// import { connection }         from '../connection'

const logger = loggers.db


/**
 * fields that clients can not access
 */
Models.MoteStatusReport.hidden_fields = [ 'group_id' ]

/**
 * models that can be joined with this
 * name in field => [table name, model]
 * TODO: should get direcly from model metadata
 */
Models.MoteStatusReport.related_models = { 'mote': Models.Mote }

/**
 * fields that clients can access
 */
Models.MoteStatusReport.exposed_fields = [
    'mote_id',
    'start_date_time',
    'end_date_time',
    'nr_tx_packets',
    'nr_lost_packets',
    'nr_tx_bytes',
    'nr_lost_bytes'
]



Models.MoteStatusReport.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {
    logger.debug('@mote_status_report.__sub_read visited')

    return Common.default__sub_read(
        Models.MoteStatusReport,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}



Models.MoteStatusReport.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.MoteStatusReport)
        .createQueryBuilder(Models.MoteStatusReport.table_name)
        .where(`${Models.MoteStatusReport.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.MoteStatusReport.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {
    throw new Common.DbOpError('can not write items in "MoteStatusReport"')
}


Models.MoteStatusReport.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {
    throw new Common.DbOpError('can not update "mote_status_report" items')
}


Models.MoteStatusReport.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.MoteStatusReport)
        .where(`${Models.MoteStatusReport.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.MoteStatusReport,
        'mote_status_report',
        'mote_status_report',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
