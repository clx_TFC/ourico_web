import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.MoteError.hidden_fields  = [ 'group_id' ]

Models.MoteError.related_models = { 'mote': Models.Mote }

Models.MoteError.exposed_fields = [
    'mote_id',
    'date_time',
    'err_code',
    'err_message'
]


Models.MoteError.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.MoteError,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.MoteError.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.MoteError)
        .createQueryBuilder(Models.MoteError.table_name)
        .where(`${Models.MoteError.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}
Models.MoteError.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {
    throw new Common.DbOpError('can not write items in "mote_error"')
}


Models.MoteError.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {
    throw new Common.DbOpError('can not update "mote_error" items')
}


Models.MoteError.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.MoteError)
        .where(`${Models.MoteError.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.MoteError,
        'mote_error',
        'mote_error',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
