import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.ApiOperationsLog.hidden_fields = [ 'group_id' ]

Models.ApiOperationsLog.related_models = {
    'owner' : Models.ApiClient
}

Models.ApiOperationsLog.exposed_fields = [
    'query',
    'date_time',
    'message',
    'level',
    'operation',
    'api_client_id'
]



Models.ApiOperationsLog.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.ApiOperationsLog,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.ApiOperationsLog.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {

    Common.admin_op_guard(client_info, 'read api client logs')

    let query_builder = getRepository(Models.ApiOperationsLog)
        .createQueryBuilder(Models.ApiOperationsLog.table_name)
        .where(`${Models.ApiOperationsLog.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.ApiOperationsLog.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {
    throw new Common.DbOpError(`can not insert items in "api_client_logs"`)
}


Models.ApiOperationsLog.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {
    throw new Common.DbOpError(`can not update "api_client_logs" items`)
}


Models.ApiOperationsLog.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    Common.admin_op_guard(client_info, 'delete api client logs')

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.ApiOperationsLog)
        .where(`${Models.ApiOperationsLog.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.ApiOperationsLog,
        'api_operations_log',
        'api_operations_log',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
