import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.MoteConfiguration.hidden_fields = [ 'group_id' ]

Models.MoteConfiguration.related_models = { 'mote': Models.Mote }

Models.MoteConfiguration.exposed_fields = [
    'mote_id',
    'name',
    'measurement_period',
    'send_data_period',
    'status_report_period',
    'use_crypto',
    'active'
]


Models.MoteConfiguration.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {
    logger.debug('@mote_configuration.__sub_read visited')

    return Common.default__sub_read(
        Models.MoteConfiguration,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.MoteConfiguration.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.MoteConfiguration)
        .createQueryBuilder(Models.MoteConfiguration.table_name)
        .where(`${Models.MoteConfiguration.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.MoteConfiguration.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.MoteConfiguration)

    Common.default_write(
        Models.MoteConfiguration,
        'mote_configuration',
        data,
        query_builder
    )

    let { nr_of_confs } = await getRepository(Models.MoteConfiguration)
        .createQueryBuilder('mote_configuration')
        .select("count(mote_configuration.name)", "nr_of_confs")
        .where("mote_configuration.mote_id = :mid", { mid: data['mote_id'] })
        .getRawOne();
    
    // automatically activate the first configuration
    if (nr_of_confs == 0)
        data['active'] = true;

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.MoteConfiguration.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.MoteConfiguration)
        .where(`${Models.MoteConfiguration.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.MoteConfiguration,
        'mote_configuration',
        'mote_configuration',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.MoteConfiguration.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.MoteConfiguration)
        .where(`${Models.MoteConfiguration.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.MoteConfiguration,
        'mote_configuration',
        'mote_configuration',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
