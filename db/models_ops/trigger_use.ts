import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.TriggerUse.hidden_fields = [ 'group_id' ]

Models.TriggerUse.related_models = {
    'trigger'    : Models.Trigger,
    'mote'       : Models.Mote,
    'occurrences': Models.TriggerOccurrence
}

Models.TriggerUse.exposed_fields = [
    'mote_id',
    'active',
    'activated_at',
    'deactivated_at',
    'trigger_id'
]

Models.TriggerUse.non_writable_fields = [
    'active',
    'activated_at',
    'deactivated_at'
]


Models.TriggerUse.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.TriggerUse,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.TriggerUse.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.TriggerUse)
        .createQueryBuilder(Models.TriggerUse.table_name)
        .where(`${Models.TriggerUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.TriggerUse.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.TriggerUse)

    Common.default_write(
        Models.TriggerUse,
        'trigger_use',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.TriggerUse.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.TriggerUse)
        .where(`${Models.TriggerUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.TriggerUse,
        'trigger_use',
        'trigger_use',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.TriggerUse.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.TriggerUse)
        .where(`${Models.TriggerUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.TriggerUse,
        'trigger_use',
        'trigger_use',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
