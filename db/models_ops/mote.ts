import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'

const logger = loggers.db



Models.Mote.hidden_fields = [
    'crypto_last_nonce_in',
    'crypto_last_nonce_out',
    'group_id'
]


Models.Mote.related_models = {
    'errors'         : Models.MoteError,
    'configurations' : Models.MoteConfiguration,
    'status_reports' : Models.MoteStatusReport,
    'conns_from_here': Models.MoteConnection,
    'conns_to_here'  : Models.MoteConnection,
    'sensors'        : Models.SensorUse,
    'actuators'      : Models.ActuatorUse,
    'alerts'         : Models.AlertUse,
    'triggers'       : Models.TriggerUse,
    'clients'        : Models.Client,
    'packets'        : Models.Packet
}

/**
 * fields that clients can access
 */
Models.Mote.exposed_fields = [
    'id',
    'name',
    'location_lat',
    'location_lng',
    'active',
    'current_ip',
    'last_status_report',
    'max_time_with_no_report',
    'crypto_alg',
    'crypto_key',
    'purpose_description',
    'other_data'
]


Models.Mote.non_writable_fields = [
    'mote_id',
    'current_ip',
    'last_status_report',
    'crypto_last_nonce_in',
    'crypto_last_nonce_out'
]


Models.Mote.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.Mote,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}



Models.Mote.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.Mote)
        .createQueryBuilder(Models.Mote.table_name)
        .where(`${Models.Mote.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.Mote.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.Mote)

    Common.default_write(
        Models.Mote,
        'mote',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.Mote.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.Mote)
        .where(`${Models.Mote.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.Mote,
        'mote',
        'mote',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.Mote.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.Mote)
        .where(`${Models.Mote.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.Mote,
        'mote',
        'mote',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
