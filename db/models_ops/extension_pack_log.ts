import { DataResource } from '../../web/api/data_resources'
import * as Models from '../models'
import * as Common from '../common'
import * as Auth from '../../web/auth'
import { loggers } from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.ExtensionPackLog.hidden_fields = ['group_id']

Models.ExtensionPackLog.related_models = {
    'owner': Models.ExtensionPackLog
}

Models.ExtensionPackLog.exposed_fields = [
    'extension_pack_id',
    'date_time',
    'message',
    'level'
]


Models.ExtensionPackLog.prototype.__sub_read = function <Entity>(
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.ExtensionPackLog,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.ExtensionPackLog.prototype.read =
    async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {

        Common.admin_op_guard(client_info, 'read api client logs')

        let query_builder = getRepository(Models.ExtensionPackLog)
            .createQueryBuilder(Models.ExtensionPackLog.table_name)
            .where(`${Models.ExtensionPackLog.table_name}.group_id = :gid`, { gid: client_info.group_id })

        this.__sub_read(query, query_builder, true)
        let results = await query_builder.cache(true).getMany()

        return results
    }


Models.ExtensionPackLog.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {
    throw new Common.DbOpError(`can not insert items in "extension_pack_logs"`)
}


Models.ExtensionPackLog.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {
    throw new Common.DbOpError(`can not update "extension_pack_logs" items`)
}


Models.ExtensionPackLog.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    Common.admin_op_guard(client_info, 'delete api client logs')

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.ExtensionPackLog)
        .where(`${Models.ExtensionPackLog.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.ExtensionPackLog,
        'extension_pack_log',
        'extension_pack_log',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}