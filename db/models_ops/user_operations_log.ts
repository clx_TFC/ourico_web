import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.UserOperationsLog.hidden_fields = [ 'group_id' ]

Models.UserOperationsLog.related_models = {
    'owner' : Models.Client
}

Models.UserOperationsLog.exposed_fields = [
    'query',
    'date_time',
    'message',
    'level',
    'operation',
    'client_id'
]


Models.UserOperationsLog.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.UserOperationsLog,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.UserOperationsLog.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {

    Common.admin_op_guard(client_info, 'read client logs')

    let query_builder = getRepository(Models.UserOperationsLog)
        .createQueryBuilder(Models.UserOperationsLog.table_name)
        .where(`${Models.UserOperationsLog.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.UserOperationsLog.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {
    throw new Common.DbOpError('can not write items in "client_logs"')
}


Models.UserOperationsLog.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {
    throw new Common.DbOpError('can not update "client_logs" items')
}


Models.UserOperationsLog.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    Common.admin_op_guard(client_info, 'delete client logs')

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.UserOperationsLog)
        .where(`${Models.UserOperationsLog.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.UserOperationsLog,
        'user_operations_log',
        'user_operations_log',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
