import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.ExtensionPack.hidden_fields = [ 'group_id' ]

Models.ExtensionPack.related_models = {
    'owner' : Models.Client,
    'logs'  : Models.ExtensionPackLog
}

Models.ExtensionPack.exposed_fields = [
    'id',
    'prog_language',
    'code_repo_url',
    'active'
]

Models.ExtensionPack.non_writable_fields = [ 'id', 'active' ]


Models.ExtensionPack.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.ExtensionPack,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.ExtensionPack.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.ExtensionPack)
        .createQueryBuilder(Models.ExtensionPack.table_name)
        .where(`${Models.ExtensionPack.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.ExtensionPack.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.ExtensionPack)

    Common.default_write(
        Models.ExtensionPack,
        'extension_pack',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.ExtensionPack.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.ExtensionPack)
        .where(`${Models.ExtensionPack.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.ExtensionPack,
        'extension_pack',
        'extension_pack',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.ExtensionPack.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.ExtensionPack)
        .where(`${Models.ExtensionPack.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.ExtensionPack,
        'extension_pack',
        'extension_pack',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
