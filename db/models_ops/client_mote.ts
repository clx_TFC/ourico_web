import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.ClientMote.hidden_fields = [ 'group_id' ]

Models.ClientMote.related_models = {
    'client': Models.Client,
    'mote'  : Models.Mote
}

Models.ClientMote.exposed_fields = [
    'receive_alerts',
    'receive_triggers',
    'receive_action_results',
    'receive_measurements',
    'receive_errors'
]


Models.ClientMote.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.ClientMote,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.ClientMote.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.ClientMote)
        .createQueryBuilder(Models.ClientMote.table_name)
        .where(`${Models.ClientMote.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.ClientMote.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.ClientMote)

    Common.default_write(
        Models.ClientMote,
        'client_mote',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.ClientMote.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.ClientMote)
        .where(`${Models.ClientMote.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.ClientMote,
        'client_mote',
        'user_mote',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.ClientMote.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.ClientMote)
        .where(`${Models.ClientMote.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.ClientMote,
        'client_mote',
        'user_mote',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
