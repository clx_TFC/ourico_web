import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.ApiClient.hidden_fields = [ 'group_id' ]

Models.ApiClient.related_models = {
    'logs'  : Models.ApiOperationsLog,
    'owner' : Models.Client
}

Models.ApiClient.exposed_fields = [
    'id',
    'access_token',
    'token_expire_date',
    'token_update_token',
    'write_capability',
    'compute_capability',
    'rt_events_capability',
    'allowed_origin_address',
    'name',
    'client_id'
]


Models.ApiClient.non_writable_fields = [
    'id',
    'access_token',
    'token_expire_date',
    'token_update_token',
    'write_capability',
    'compute_capability',
    'rt_events_capability',
    'allowed_origin_address'
]


Models.ApiClient.updatable_fields = [
    'write_capability',
    'compute_capability',
    'rt_events_capability',
    'allowed_origin_address'
]


Models.ApiClient.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.ApiClient,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.ApiClient.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.ApiClient)
        .createQueryBuilder(Models.ApiClient.table_name)
        .where(`${Models.ApiClient.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.ApiClient.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.ApiClient)

    Common.default_write(
        Models.ApiClient,
        'api_client',
        data,
        query_builder
    )
    
    data['id']          = await Auth.rand_string(20)
    data['group_id']    = client_info.group_id
    data['client_id']   = client_info.client_id


    let capabilities: Auth.Capabilities[] = []
    capabilities.push(Auth.Capabilities.Read) // default
    if (data['write_capability'])
        capabilities.push(Auth.Capabilities.Write)
    if (data['rt_events_capability'])
        capabilities.push(Auth.Capabilities.RtEvents)
    if (data['compute_capability'])
        capabilities.push(Auth.Capabilities.Compute)


    let jwt_payload =  new Auth.ClientAuthInfo(
        data['id'], Auth.ClientType.ApiClient, data['group_id'], null, capabilities
    )

    data['access_token'] = await Auth.generate_token(jwt_payload)

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.ApiClient.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.ApiClient)
        .where(`${Models.ApiClient.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.ApiClient,
        'api_client',
        'api_client',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.ApiClient.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {
    throw new Common.DbOpError(
        `clients must be deleted from api.control/delete_client endpoint`
    )
}
