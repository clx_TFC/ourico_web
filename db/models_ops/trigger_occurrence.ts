import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.TriggerOccurrence.hidden_fields = [ 'group_id' ]

Models.TriggerOccurrence.related_models = {
    'trigger_use': Models.TriggerUse
}

Models.TriggerOccurrence.exposed_fields = [
    'date_time'
]


Models.TriggerOccurrence.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.TriggerOccurrence,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.TriggerOccurrence.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.TriggerOccurrence)
        .createQueryBuilder(Models.TriggerOccurrence.table_name)
        .where(`${Models.TriggerOccurrence.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.TriggerOccurrence.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {
    throw new Common.DbOpError('can not write items in "trigger_occurrence"')
}


Models.TriggerOccurrence.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {
    throw new Common.DbOpError('can not update "trigger_occurrence" items')
}


Models.TriggerOccurrence.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.TriggerOccurrence)
        .where(`${Models.TriggerOccurrence.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.TriggerOccurrence,
        'trigger_occurrence',
        'trigger_occurrence',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
