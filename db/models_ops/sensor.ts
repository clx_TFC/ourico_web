import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.Sensor.hidden_fields = [ 'group_id' ]

Models.Sensor.related_models = {
    'parameters': Models.Parameter,
    'uses'      : Models.SensorUse
}

Models.Sensor.exposed_fields = [
    'id',
    'output_size',
    'description',
    'async',
    'name'
]

Models.Sensor.non_writable_fields = [ 'id' ]


Models.Sensor.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.Sensor,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.Sensor.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.Sensor)
        .createQueryBuilder(Models.Sensor.table_name)
        .where(`${Models.Sensor.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.Sensor.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.Sensor)

    Common.default_write(
        Models.Sensor,
        'sensor',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()
    console.log('the insert result of the sensor', insert_result)

    return insert_result.identifiers
}


Models.Sensor.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.Sensor)
        .where(`${Models.Sensor.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.Sensor,
        'sensor',
        'sensor',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.Sensor.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.Sensor)
        .where(`${Models.Sensor.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.Sensor,
        'sensor',
        'sensor',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
