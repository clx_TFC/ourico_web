import { DataResource } from '../../web/api/data_resources'
import * as Models      from '../models'
import * as Common      from '../common'
import * as Auth        from '../../web/auth'
import { loggers }      from '../../logger'
import {
    Comparison, Filter, Option, Query
} from '../../web/api/query'


import { getConnection, getRepository, SelectQueryBuilder } from 'typeorm'


const logger = loggers.db

Models.AlertUse.hidden_fields = [ 'group_id' ]

Models.AlertUse.related_models = {
    'mote'       : Models.Mote,
    'alert'      : Models.Alert,
    'occurrences': Models.AlertOccurrence
}

Models.AlertUse.exposed_fields = [
    'mote_id',
    'active',
    'activated_at',
    'deactivated_at',
    'alert_id'
]

Models.AlertUse.non_writable_fields = [
    'active',
    'activated_at',
    'deactivated_at'
]

Models.AlertUse.prototype.__sub_read = function <Entity> (
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[] = [],
    used_alias: string = undefined
) {

    return Common.default__sub_read(
        Models.AlertUse,
        used_alias,
        query,
        query_builder,
        first,
        visited
    )
}


Models.AlertUse.prototype.read =
async function (query: Query, client_info: Auth.ClientAuthInfo): Promise<object[]> {
    let query_builder = getRepository(Models.AlertUse)
        .createQueryBuilder(Models.AlertUse.table_name)
        .where(`${Models.AlertUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    this.__sub_read(query, query_builder, true)
    let results = await query_builder.cache(true).getMany()

    return results
}


Models.AlertUse.prototype.write =
async function (data: object[], client_info: Auth.ClientAuthInfo): Promise<any[]> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .insert()
        .into(Models.AlertUse)

    Common.default_write(
        Models.AlertUse,
        'alert_use',
        data,
        query_builder
    )

    data['group_id'] = client_info.group_id

    let insert_result = await query_builder.execute()

    return insert_result.identifiers
}


Models.AlertUse.prototype.update = async function (
    where: Filter[],
    new_data: object[],
    client_info: Auth.ClientAuthInfo
): Promise<number> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .update(Models.AlertUse)
        .where(`${Models.AlertUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_update(
        Models.AlertUse,
        'alert_use',
        'alert_use',
        new_data,
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return update_result.generatedMaps.length
}


Models.AlertUse.prototype.delete =
async function (where: Filter[], client_info: Auth.ClientAuthInfo): Promise<void> {

    let query_builder = getConnection()
        .createQueryBuilder()
        .delete()
        .from(Models.AlertUse)
        .where(`${Models.AlertUse.table_name}.group_id = :gid`, { gid: client_info.group_id })

    Common.default_delete(
        Models.AlertUse,
        'alert_use',
        'alert_use',
        where,
        query_builder
    )

    let update_result = await query_builder.execute()

    return
}
