import { Connection, createConnection } from 'typeorm'
import { loggers } from '../logger.js'

const logger = loggers.db

let connection: Connection = undefined
export { connection }


/**
 * Connect to database
 */
export async function setup () {
    try {
        connection = await createConnection()
        logger.info(`@db.connection established connection to database`)
        return connection
    } catch (exception) {
        logger.error(`failed to create database connection: ${exception}`)
    }
}
