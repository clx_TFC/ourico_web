/**
 * Common functionality applied to every model
 */

import { Comparison, Filter, Option, Query } from '../web/api/query'

import {

    Entity,
    QueryBuilder,
    SelectQueryBuilder,
    InsertQueryBuilder,
    UpdateQueryBuilder,
    DeleteQueryBuilder,

    getRepository

} from 'typeorm'

import { loggers } from '../logger'
import { Model }   from './models'
import * as Models from './models'
import * as Auth   from '../web/auth'

const logger = loggers.web


export class DbOpError {
    constructor (public message: string) { }
}


/**
 * Available options are:
 * groupby   - equivalent to sql's group by
 * orddesc   - sort in descending order
 * ordasc    - sort in ascending order
 * returning - return the results of an update
 * skip      - skip some initial results
 * take      - limit the number of results
 *
 * @returns void | error message
 */
export function apply_options <Entity> (

    resource_name: string,
    alias: string,
    options: Option[],
    query_builder: SelectQueryBuilder<Entity>,
    hidden_fields: string[],
    exposed_fields: string[]

) {

    for (var option of options) {

        switch (option.name) {

            case 'orddesc': {

                if (option.target !== undefined && hidden_fields.find(h => h == option.target)) {
                    logger.warn(
                        `client tried to access hidden field ${option.target}`
                    )
                    throw new DbOpError(
                        `resource "${resource_name}" does not have field ${option.target}`
                    )
                } else if (option.target !== undefined && !exposed_fields.find(e => e == option.target)) {
                    throw new DbOpError(
                        `resource "${resource_name}" does not have field ${option.target}`
                    )
                }

                if (option.target === undefined)
                    throw new DbOpError(`orddesc must have a target`)
                query_builder.orderBy(`${alias}.${option.target}`, 'DESC')
                break
            }

            case 'ordasc': {

                if (option.target !== undefined && hidden_fields.find(h => h == option.target)) {
                    logger.warn(
                        `client tried to access hidden field ${option.target}`
                    )
                    throw new DbOpError(
                        `resource "${resource_name}" does not have field ${option.target}`
                    )
                } else if (option.target !== undefined && !exposed_fields.find(e => e == option.target)) {
                    throw new DbOpError(
                        `resource "${resource_name}" does not have field ${option.target}`
                    )
                }

                if (option.target === undefined)
                    throw new DbOpError(`ordasc must have a target`)
                query_builder.orderBy(`${alias}.${option.target}`, 'ASC')
                break
            }

            case 'groupby': {

                // if (option.target !== undefined && hidden_fields.find(h => h == option.target)) {
                //     logger.warn(
                //         `client tried to access hidden field ${option.target}`
                //     )
                //     throw new DbOpError(
                //         `resource "${resource_name}" does not have field ${option.target}`
                //     )
                // } else if (option.target !== undefined && !exposed_fields.find(e => e == option.target)) {
                //     throw new DbOpError(
                //         `resource "${resource_name}" does not have field ${option.target}`
                //     )
                // }
                //
                // if (option.target === undefined)
                //     throw new DbOpError(`groupby must have a target`)
                // query_builder.groupBy(`${alias}.${option.target}`)
                // break
                throw new DbOpError(`option 'groupby' is not yet implemented`)
            }

            case 'skip': {
                if (option.target === undefined)
                    throw new DbOpError(`'skip' must tell how many results to skip`)
                else if (isNaN(+option.target))
                    throw new DbOpError(`target of 'skip' must be an integer`)
                query_builder.skip(+option.target)
                break
            }

            case 'take': {
                if (option.target === undefined)
                    throw new DbOpError(`'take' must tell how many results to limit`)
                else if (isNaN(+option.target))
                    throw new DbOpError(`target of 'take' must be an integer`)
                query_builder.take(+option.target)
                break
            }

            case 'returning': {
                throw new DbOpError(`option 'returning' is not yet implemented`)
            }

            default:
                throw new DbOpError(`unknown option ${option.name}`)
        }
    }
}


/**
 * renames the field to have aggregator if it has one
 * aggregator syntax is: <field_name>.<aggregator> (e.g. price.avg)
 * it will be renamed as following <aggregator>(<field_name>) (e.g avg(prive))
 * or kept the same if there is no aggregator
 * Available aggretaros are:
 * count
 * avg
 * min
 * max
 * sum
 *
 * @returns void | error message
 */
export function select_fields <Entity> (

    resource_name: string,
    alias: string,
    fields: string[],
    hidden_fields: string[],
    exposed_fields: string[]

) {

    let selected: string[] = []

    for (var field of fields) {

        let parts = field.split('.')

        if (parts.length == 1) {
            if (hidden_fields.find(h => h == field)) {
                logger.warn(
                    `client tried to access hidden field ${field}`
                )
                throw new DbOpError(`resource "${resource_name}" does not have field ${field}`)
            } else if (!exposed_fields.find(e => e == field)) {
                throw new DbOpError(`resource "${resource_name}" does not have field ${field}`)
            }

            selected.push(`${alias}.${field}`)
        }

        else if (parts.length == 2) {
            if (hidden_fields.find(h => h == parts[0])) {
                logger.warn(
                    `client tried to access hidden field ${parts[0]}`
                )
                throw new DbOpError(`resource "${resource_name}" does not have field ${parts[0]}`)
            } else if (!exposed_fields.find(e => e == parts[0])) {
                throw new DbOpError(`resource "${resource_name}" does not have field ${parts[0]}`)
            }

            switch (parts[1].trim()) {
                case "count": selected.push(`COUNT(${alias}.${parts[0]})`); break
                case "avg"  : selected.push(`AVG(${alias}.${parts[0]})`)  ; break
                case "min"  : selected.push(`MIN(${alias}.${parts[0]})`)  ; break
                case "max"  : selected.push(`MAX(${alias}.${parts[0]})`)  ; break
                case "sum"  : selected.push(`SUM(${alias}.${parts[0]})`)  ; break

                default:
                    throw new DbOpError(`unknown aggregator ${parts[1]}`)
            }
        }

        else {
            throw new DbOpError(`field ${field} should have only one aggregator`)
        }
    }

    return selected
}


/**
 * Apply filter to attribute
 * Available filters are
 * eq  or  =
 * neq or != or <>
 * lt or <
 * lte or <=
 * gt or >
 * gte or >=
 * in
 * notin
 * null
 * notnull
 *
 * any of the comparisons may be preceeded by or_, in which case an or will
 * be apply instead of and (e.g. or_=, or_notin)
 *
 * @returns void | error message
 */
export function apply_filters <Entity> (

    resource_name: string,
    alias: string,
    filters: Filter[],
    // TODO: should be of type UpdateQueryBuilder or SelectQueryBuilder or DeleteQueryBuilder
    query_builder: any,
    hidden_fields: string[],
    exposed_fields: string[],
    return_filter: boolean // return the filter instead of adding to the queryBuilder

) {

    let x = 0
    let filter_text = ""
    let filter_targets = {}

    for (var filter of filters) {

        // guard hidden fields from appearing in queries
        if (hidden_fields.find(h => h == filter.field)) {
            logger.warn(
                `client tried to access hidden field ${filter.field}`
            )
            throw new DbOpError(`resource "${resource_name}" does not have field ${filter.field}`)
        // verify that fields appearing are known
        } else if (!exposed_fields.find(e => e == filter.field)) {
            throw new DbOpError(`resource "${resource_name}" does not have field ${filter.field}`)
        }

        // TODO: check types
        let cmp
        let with_or = false

        let comp_name
        if (filter.comparison.name.startsWith('or_')) {
            comp_name = filter.comparison.name.substr(3)
            with_or = true
        } else {
            comp_name = filter.comparison.name
        }

        switch (comp_name) {

            case 'eq'   : case '=' :            cmp = '=';      break;
            case 'neq'  : case '!=': case '<>': cmp = '<>';     break;
            case 'lt'   : case '<' :            cmp = '<';     break;
            case 'lte'  : case '<=': case '=<': cmp = '<=';     break;
            case 'gt'   : case '>' :            cmp = '>';      break;
            case 'gte'  : case '>=': case '=>': cmp = '>=';      break;
            case 'in'   :                       cmp = 'in';     break;
            case 'notin':                       cmp = 'not in'; break;

            default:
                throw new DbOpError(`unknown filter ${filter.comparison.name}`)

        }

        let key = `${alias}${filter.field}${x++}`
        let comparison_txt = comp_name == 'in' || comp_name == 'notin' ?
            `${alias}.${filter.field} ${cmp} (:...${key})` : // compare against a list
            `${alias}.${filter.field} ${cmp} :${key}` // comparte against an 'atom'
        

        // add to values to be returned instead of adding to query builder
        if (return_filter) {
            let c = with_or ? 'or' : 'and'
            if (x === 1)
                filter_text += comparison_txt
            else filter_text += ` ${c} ${comparison_txt} `
            filter_targets[key] = filter.comparison.target
            continue;
        }

        // add to query builder
        let target = {}
        target[key] = filter.comparison.target
        if (!with_or) {
            if (comp_name == 'in' || comp_name == 'notin')
                query_builder.andWhere(`${alias}.${filter.field} ${cmp} (:...${key})`, target)
            else
                query_builder.andWhere(`${alias}.${filter.field} ${cmp} :${key}`, target)
        } else {
            if (comp_name == 'in' || comp_name == 'notin')
                query_builder.orWhere(`${alias}.${filter.field} ${cmp} (:...${key})`, target)
            else
                query_builder.orWhere(`${alias}.${filter.field} ${cmp} :${key}`, target)
        }

    }

    if (return_filter) return { filter: filter_text, targets: filter_targets }

}


/**
 * Execute a query recursively
 *
 * @param this_model     model where this query is being executed
 * @param query
 * @param query_builder
 * @param first          indicates if this is the root of the query
 * @param visited        indicates if the current table has been visited in this query
 *
 * @throws DbOpError
 */
export function default__sub_read <Entity> (

    this_model: any,
    used_alias: string,
    query: Query,
    query_builder: SelectQueryBuilder<Entity>,
    first: boolean = false,
    visited: string[]

) {

    logger.info(`@db.common sub reading ${this_model.table_name}#${used_alias}`)

    let hidden_fields  = this_model['hidden_fields']
    let exposed_fields = this_model['exposed_fields']
    let related_models = this_model['related_models']
    let alias
    if (used_alias == undefined) alias = this_model['table_name']
    else alias = used_alias

    let selected = (query.fields !== undefined && query.fields.length != 0)   ?
            select_fields(query.resource, alias, query.fields, hidden_fields, exposed_fields) :
            exposed_fields.map(f => `${alias}.${f}`)

    if (first) query_builder.select(selected)
    else       query_builder.addSelect(selected)


    if (query.filters !== undefined && query.filters.length != 0)
        apply_filters(
            query.resource, 
            alias, 
            query.filters, 
            query_builder, 
            hidden_fields, 
            exposed_fields,
            false
        )


    if (query.options !== undefined && query.options.length != 0)
        apply_options(query.resource, alias, query.options, query_builder, hidden_fields, exposed_fields)


    if (query.includes !== undefined) {
        for (var sub_query of query.includes) {
            if (!related_models.hasOwnProperty(sub_query.resource)) {
                throw new DbOpError(`"${sub_query.resource}" is not a sub resource of "${query.resource}"`)
            }

            logger.debug(`@db.common sub querying resource ${sub_query.resource}`)

            if (related_models[sub_query.resource] instanceof Array) {
                // must do a series of joins to reach the resource

                let relation_chain = related_models[sub_query.resource]
                let target_resource = relation_chain[relation_chain.length - 1]
                let curr_alias = alias
                for (let i = 0; i < relation_chain.length - 1; i++) {
                    // intermediate[0] is the name of the intermediate resource
                    // intermediate[1] is the model corresponding to the resource
                    let intermediate = relation_chain[i]
                    let sub_alias = intermediate[1].table_name + visited.length
                    query_builder.leftJoinAndSelect(
                        `${curr_alias}.${intermediate[0]}`, `${sub_alias}`
                    )
                    visited.push(sub_alias)
                    curr_alias = sub_alias
                }

                let sub_alias = target_resource[1].table_name + visited.length
                if (sub_query.filters !== undefined && sub_query.filters.length !== 0) {
                    // take all the filters from sub query to apply them in join
                    let sub_query_filter = sub_query.filters.splice(0, sub_query.filters.length)
                    let join_filter = apply_filters(
                        sub_query.resource, 
                        sub_alias, 
                        sub_query_filter, 
                        null,
                        target_resource[1].hidden_fields,
                        target_resource[1].exposed_fields,
                        true
                    )

                    // join with target sub_resource
                    query_builder.leftJoin(
                        `${curr_alias}.${target_resource[0]}`, `${sub_alias}`,
                        join_filter.filter, join_filter.targets
                    )
                } else {
                    // join with target sub_resource
                    query_builder.leftJoin(`${curr_alias}.${target_resource[0]}`, `${sub_alias}`)
                }
                let sub_resource = new (target_resource[1])()
                visited.push(sub_alias)
                sub_resource.__sub_read(sub_query, query_builder, false, visited, sub_alias)

            } else {

                let sub_alias = related_models[sub_query.resource].table_name + visited.length
                if (sub_query.filters !== undefined && sub_query.filters.length !== 0) {
                    // take all the filters from sub query to apply them in join
                    let sub_query_filter = sub_query.filters.splice(0, sub_query.filters.length)
                    let join_filter = apply_filters(
                        sub_query.resource, 
                        sub_alias, 
                        sub_query_filter, 
                        null,
                        related_models[sub_query.resource].hidden_fields,
                        related_models[sub_query.resource].exposed_fields,
                        true
                    )

                    // join with sub_resource
                    query_builder.leftJoin(
                        `${alias}.${sub_query.resource}`, `${sub_alias}`,
                        join_filter.filter, join_filter.targets
                    )
                } else {
                    // join with sub_resource
                    query_builder.leftJoin(`${alias}.${sub_query.resource}`, `${sub_alias}`)
                }

                let sub_resource = new (related_models[sub_query.resource])()
                visited.push(sub_alias)
                sub_resource.__sub_read(sub_query, query_builder, false, visited, sub_alias)

            }

        }
    }
}



/**
 * @param target_model  model where to apply updates
 * @param alias
 * @param resource
 * @param where
 * @param new_data
 * @param query_builder
 *
 * @throws DbOpError
 */
export function default_update <Entity> (

    target_model: any,
    alias: string,
    resource: string,
    new_data: object,
    wheres: Filter[],
    query_builder: UpdateQueryBuilder<Entity>

) {

    let hidden_fields       = target_model['hidden_fields']
    let exposed_fields      = target_model['exposed_fields']
    let non_writable_fields = target_model['non_writable_fields']

    apply_filters(resource, alias, wheres, query_builder, hidden_fields, exposed_fields, false)

    // check if new fields exist and can be changed
    for (var attr in new_data) {
        if (hidden_fields.find(h => h == attr)) {
            logger.warn(`client tried to access hidden field ${attr}`)
            throw new DbOpError(`"${resource}" does not have field ${attr}`)
        } else if (non_writable_fields.find(nw => nw == attr)) {
            throw new DbOpError(`can't replace field "${attr}" of resource "${resource}"`)
        } else if (!exposed_fields.find(e => e == attr)) {
            throw new DbOpError(`"${resource}" does not have field ${attr}`)
        }
    }

    query_builder.set(new_data)
}


/**
 * @param target_model model where to insert new data
 * @param resource
 * @param query_builder
 * @param data
 *
 * @throws DbOpError
 */
export function default_write <Entity> (

    target_model: any,
    resource: string,
    data: object[],
    query_builder: InsertQueryBuilder<Entity>

) {

    let hidden_fields  = target_model['hidden_fields']
    let exposed_fields = target_model['exposed_fields']

    for (var new_data of data) {
        for (var attr in new_data) {
            if (hidden_fields.find(h => h == attr)) {
                logger.warn(`client tried to access hidden field ${attr}`)
                throw new DbOpError(`"${resource}" does not have field ${attr}`)
            } else if (!exposed_fields.find(e => e == attr)) {
                throw new DbOpError(`"${resource}" does not have field ${attr}`)
            }
        }
    }

    query_builder.values(data)
}



/**
 * @param target_model model where to delete
 * @param resource
 * @param wheres
 * @param query_builder
 */
export function default_delete <Entity> (

    target_model: any,
    alias: string,
    resource: string,
    wheres: Filter[],
    query_builder: DeleteQueryBuilder<Entity>

) {

    let hidden_fields  = target_model['hidden_fields']
    let exposed_fields = target_model['exposed_fields']

    apply_filters(resource, alias, wheres, query_builder, hidden_fields, exposed_fields, false)

}


/**
 * To implement fine grained guard on specific operations on models
 * Checks if the client making the request is an admin. If not an exception is thrown
 *
 * @param client_info information about the client executing the operation
 * @param operation   name of the operation being called
 *
 * @throws DbOpError
 */
export async function admin_op_guard
(client_info: Auth.ClientAuthInfo, operation: string) {

    let current_user = await getRepository(Models.Client)
        .createQueryBuilder(Models.Client.table_name)
        .where(`${Models.Client.table_name}.id = :id`, { id: client_info.client_id })
        .getOne()

    if (!current_user.admin) {
        throw new DbOpError(`only admins can ${operation}`)
    }

}
